$( document ).ready(function() {

    $('.roles').on('change',function(e){
        var role_id =  $('.roles').val();
        redirect('cms/role/rights/'+role_id);
    });
    
    $('.users').on('change',function(e){
        var user_id =  $('.users').val();
        redirect('cms/user/rights/'+user_id);
    });
    
    $('#all_check').on('click',function(e){
        
        if($(this).is(':checked')){
            $('.all').prop('checked', true);
        }else{
            $('.all').prop('checked', false);
        }
        
    });
    
    
    
    $(".horizontal").on('click',function(e){
        var module_id = $(this).attr('data-id');
        if($(this).is(':checked')){
           $('.horizontal-'+module_id).prop('checked', true);
        }else{
            $('.horizontal-'+module_id).prop('checked', false);
        } 
    });
	
});


