
            var sfw;
            var valid = false;
            $(document).ready(function () {
                sfw = $("#wizard_example").stepFormWizard({
                    height: 'auto',
                    theme: 'circle',
                    transition: 'fade',
                    onNext: function (i) {
                        
                        
                        
                       valid = submit_form_code(i); 
                       
                        sfw.refresh();
                        return valid;
                    },
                    onPrev: function (i) {
                        
                        sfw.refresh();
                        return true;
                    },
                    onFinish: function (i) {
                        //var valid = $("#form-0").parsley().validate();
                        // if use height: 'auto' call refresh metod after validation, because parsley can change content
                        
                       
                       valid = submit_form_code(i); 
                       
                        sfw.refresh();
                        return valid;
                    }
                });
                
                
                
                
                
                
                
            });

            $(document).ready(function () {
                //$('#PreviousHIVTestWrap').hide();
                $('#PreviousHIVTest-1').change(function () {
                    $('#PreviousHIVTestWrap').fadeToggle();
                });
                $('#PreviousHIVTest-2').change(function () {
                    $('#PreviousHIVTestWrap').fadeToggle();
                });


                //$('#DrugUserWrap').hide();
                $('#DrugUser').change(function () {
                    $('#DrugUserWrap').fadeToggle();
                });
                $('#DrugUserNo').change(function () {
                    $('#DrugUserWrap').hide();
                });
            });

            /**/
            var FormStuff = {

                init: function () {
                    this.applyConditionalRequired();
                    this.bindUIActions();
                },

                bindUIActions: function () {
                    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
                },

                applyConditionalRequired: function () {

                    $(".require-if-active").each(function () {
                        var el = $(this);
                        if ($(el.data("require-pair")).is(":checked")) {
                            el.prop("required", true);
                        } else {
                            el.prop("required", false);
                        }
                    });

                }

            };

            FormStuff.init();

            /**/


     



function submit_form_code(step){
                
    var has_validate_error = false;
    $('#form-'+step+' .validateInput').each(function(a, e){

    //$(fieldset_inputs).each(function (i, e) {
       if(this.value == '') {
               //alert(this.value);
               has_validate_error = true;
               $(e).addClass('validate_error');
       }else {
               //has_validate_error = false;
               $(e).removeClass('validate_error');
       }
    });

    if(has_validate_error){

            return false;
    }

    
    var valid = false;
    $.blockUI({
                            css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
 
                       
                        //$("#form-"+step).parsley().validate('block' + i)
                        var url = $("#form-"+step).attr('action');
                        var postData = $("#form-"+step).serialize();
                       // alert("#form-"+step);
                        $.ajax(
                        {
                            url : url,
                            dataType: "json",
                            type: "POST",
                            data : postData,
                            async:false,
                            success:function(result) 
                            {
                                $.unblockUI;
                                if(result.error == false){
                                    
                                    if(result.show_message){
                                        $('#validation-msg').addClass('alert-success show');
                                        $("#validation-msg").html(result.success);
                                     }
                                    
                                    /* setTimeout(function() {
                                        window.location.reload();
                                    }, 1000);*/
                       
            
                                    valid = true;
                                }else{
                                    if(result.show_message){
                                        $('#validation-msg').addClass('alert-danger show');
                                        $("#validation-msg").html(result.error);
                                     }
                                    valid = false;
                                }
                                
                                
                                
                               
                                
                                setTimeout(function() {
                                        $('#validation-msg').removeClass('alert-danger show alert-success');
                                        $('#validation-msg').html('');
                                }, 5000);
                                if (result.redirect) {
                                        setTimeout(function() {
                                            window.location.href = base_url + result.url;
                                        }, 1000);

                                    }
                                if (result.reset){
                                    $("#form-"+step)[0].reset();
                                }
                            },
                            complete: function() {
                                $.unblockUI();

                            }
                        });
                        
                      
                        return valid;
}