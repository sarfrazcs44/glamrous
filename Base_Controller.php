<?php

class Base_Controller extends CI_Controller {

    protected $language;

    public function __construct() {

        parent::__construct();
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }
        $this->data['site_setting'] = $this->getSiteSetting();
    }

    public function changeLanguage($language) {
        $this->load->Model('Model_system_language');
        $fetch_by['GlobalCode'] = $language;
        $result = $this->Model_system_language->getWithMultipleFields($fetch_by);
        $languageID = $result->SysLID;
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
            $languageID = $default_lang->SystemLanguageID;
        }
        $this->session->set_userdata('lang', $language);
        //$this->session->set_userdata('languageID',$languageID);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting() {

        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImageAdd($file_key, $path, $id = false, $type = false, $multiple = false) {
        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                } else {
                    $this->load->model('Image_model');
                    $data['OtherTableID'] = $id;
                    $data['OtherTableName'] = $type;
                    $data['FileName'] = $path . $file_name;
                    $this->Image_model->save($data);
                }
                /* $data['DestinationID'] = $id; 
                  $data['ImagePath'] = $path.$file_name;
                  $this->Site_images_model->save($data); */
            }
        }
        return true;
    }
    
    public function DeleteImage() {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }
    
    
    
    public function UploadImages($id,$key) {
        
         $this->load->model('Image_model');
       
        if (!empty($_FILES['image']['name'])) {
             
            // next we pass the upload path for the images
            $config['upload_path'] = './uploads/images/';
            // also, we make sure we allow only certain type of images
            $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|txt|doc|docx';
            $responseUpload = $this->uploadMultipleFiles('image', $config);
            
            if ($responseUpload != false) {
               
                $path = 'uploads/images/';
                $file_name = $responseUpload['uploads'][0]['file_name'];
                $image_data     = array();
                $image_data['OtherTableID'] = $id;
                 $image_data['OtherTableName'] = $key;
                $image_data['FileName'] = $path.$file_name;
                
                $insert_id = $this->Image_model->save($image_data);
                $data['insert_id'] = $insert_id;

                header('Content-type: application/json');
                echo json_encode($data);
            } else
                echo 'error uploading';
        }
    }
    
    public function uploadMultipleFiles($fileName='', $config=null)
	{
       
		// retrieve the number of images uploaded;
		$number_of_files = sizeof($_FILES[$fileName]['tmp_name']);
		// considering that do_upload() accepts single files, we will have to do a small hack so that we can upload multiple files. For this we will have to keep the data of uploaded files in a variable, and redo the $_FILE.
		$files = $_FILES[$fileName];
		$errors = array();
	
		// first make sure that there is no error in uploading the files
		for($i=0;$i<$number_of_files;$i++)
		{
		  if($_FILES[$fileName]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES[$fileName]['name'][$i];
		}
		if(sizeof($errors)==0)
		{
		  // now, taking into account that there can be more than one file, for each file we will have to do the upload
		  // we first load the upload library
		  $this->load->library('upload');

		  for ($i = 0; $i < $number_of_files; $i++) {
			$_FILES['uploadFile']['name'] = $files['name'][$i];
			$_FILES['uploadFile']['type'] = $files['type'][$i];
			$_FILES['uploadFile']['tmp_name'] = $files['tmp_name'][$i];
			$_FILES['uploadFile']['error'] = $files['error'][$i];
			$_FILES['uploadFile']['size'] = $files['size'][$i];
			//now we initialize the upload library
			$this->upload->initialize($config);
			// we retrieve the number of files that were uploaded
			if ($this->upload->do_upload('uploadFile'))
			{
			  $data['uploads'][$i] = $this->upload->data();
//                            $path = 'uploads/adult_reports/';
//                            $file_name = $data['uploads'][$i]['file_name'];
//                            $db_file_name = mt_rand(100000, 999999).$file_name;
//                            $image_data     = array();
//                            $image_data['PatientID'] = $PatientID;
//                            $image_data['FileName'] = $path.$db_file_name;
//
//                            $insert_id = $this->Action_plan_file_model->save($image_data);
			}
			else
			{
			  $data['upload_errors'][$i] = $this->upload->display_errors();
			}
		  }
		}
		else
		{
		  return false;
		}
		
		return $data;

	}
    
        public function RemoveImages() {
            $this->delete_file('uploads/images/' . $this->input->post('file'));
            $this->Action_plan_file_model->delete(['PlanActionFileID' => $this->input->post('id')]);
        }
    
    
    public function delete_file($files) {
		if(is_array($files)) {
			foreach($files as $file) {
				@unlink($file);
			}
		} else {
			@unlink($files);
		}
	}
    
}
