-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 24, 2019 at 01:04 PM
-- Server version: 5.7.28
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salon_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `AddressID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Zip` varchar(255) NOT NULL,
  `PoBox` varchar(255) NOT NULL,
  `BlockNo` varchar(255) NOT NULL,
  `FlatNo` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `District` varchar(255) NOT NULL,
  `Street` varchar(255) NOT NULL,
  `CityID` int(11) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `Lat` varchar(255) NOT NULL,
  `Lng` varchar(255) NOT NULL,
  `IsDefault` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`AddressID`, `UserID`, `Zip`, `PoBox`, `BlockNo`, `FlatNo`, `Phone`, `Location`, `District`, `Street`, `CityID`, `Country`, `Lat`, `Lng`, `IsDefault`, `CreatedAt`, `UpdatedAt`) VALUES
(2, 29, '2', '1', '3', '5', '677', 'Saudi Arabia', 'Lahore', 'Lahore', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '2018-09-14 11:56:45'),
(4, 31, '128ue', '812ue8122', 'Kansa', 'klnascx', '0512345678', 'Al Masarah, Jeddah Saudi Arabia', 'acmklsadc', '12uer', 4, 'Saudi Arabia', '21.2854067', '39.2375507', 0, '0000-00-00 00:00:00', '2018-09-10 13:51:07'),
(5, 1536818051, '5490', '54', '45', '56', '134', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1536818695, '5400', '54', '56', '56', '12345678', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1536819428, '5400', '5400', '45', '5', '1234', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1536926126, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1536936027, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 33, '64350', 'jn', 'hbs', 'jbss', '0512345678', 'Al Masarah, Jeddah Saudi Arabia', 'kabba', 'jbs', 3, 'Saudi Arabia', '21.2854067', '39.2375507', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1549279932, '123', '123', '4', '23', '03312342966', 'Saudi Arabia', 'Lahore', 'Xyz', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 50, 'yeue', '6263', 'ytt', '56', '0512345678', 'Unnamed Road, Al Masarah, Jeddah Saudi Arabia', 'lahore', '2', 3, 'Saudi Arabia', '21.28539750000001', '39.23759765624997', 0, '0000-00-00 00:00:00', '2019-02-26 11:17:08'),
(17, 51, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 51, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 45, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 45, '', '', '', '', '', 'Saudi Arabia', '', '', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 43, '123', '234', '23', '22', '124214234', 'Saudi Arabia', 'Lahore', '123', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '2019-02-27 11:07:36'),
(23, 57, '123', '123', '123', '123', '2838488448', 'Saudi Arabia', 'Lahore', '2', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 41, 'hgdv', 'hh', 'vv', 'vv', 'bb', 'Saudi Arabia', 'vg', 'vh', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 41, 'ruturur', 'hhhg', 'aaa', 'bbbb', '03312345678', 'Saudi Arabia', 'tgggh', 'djdhdxbx', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 41, '75000', '123', '2', '22', '033123456789', 'Saudi Arabia', 'Lahore', '45', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 41, '67788', '3', '3', '34', '00000444', 'Saudi Arabia', '23', '557', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 41, '7899', '56', '45', '566', '455555566', 'Saudi Arabia', '56', '67', 4, '', ' ', ' ', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 41, '3747', '4747', '475', '47', '03347484848', 'Saudi Arabia', '4858', '475', 4, '', '31.520369600000006', '74.35874729999999', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 0, '4747', '374', '47', '374', '3939495959', 'Saudi Arabia', '4774', '4747', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 41, '75000', 'diddej', '47485', '37474', '01234556', 'Saudi Arabia', 'dnddnn', '78', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 41, '75000', '38383', '47', '47', '033283848585', 'Saudi Arabia', '4848', '48', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 41, '4747', '36464', '47585858', '484', '47758585', 'Saudi Arabia', '5858', '5858', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 41, '7', '686', '6', '6', '777777', 'Saudi Arabia', '6', '7', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 41, 'f5cycyv', 'v8gy', 'uvuu', 'ycyvyv', 'yvuvuvy', 'Saudi Arabia', '5cycyg', 'yvyvyvu', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 41, 'num7n5', 'ynun5v', 'nynumun', 'j6nunun', 'ununuk7', 'Saudi Arabia', 'ynyn6n', '6j7k7', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 60, '57000', '54000', '34', '266', '090078601', 'Saudi Arabia', 'lahore', '24', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 61, '24', '123', '14', 'w3', '934847755', 'Saudi Arabia', '12', '1334', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 61, '12', '12', '12', '12', '12412412412', 'lqhor3', 'asdas', 'we', 4, 'Saudi Arabia', '31.520369600000006', '74.35874729999999', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `AdID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`AdID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/2846937615420191011053317download-5.jpg', 0, 0, 1, '2018-09-10 12:21:08', '2019-10-11 17:17:33', 1, 1),
(2, 'uploads/images/2394077304320191011054717download.jpg', 1, 0, 1, '2018-09-11 05:57:29', '2019-10-11 17:17:47', 1, 1),
(3, 'uploads/images/7429889576420191011055717getlstd-property-photo-1.jpg', 2, 0, 1, '2018-09-11 05:59:09', '2019-10-11 17:17:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ads_text`
--

CREATE TABLE `ads_text` (
  `AdTextID` int(11) NOT NULL,
  `AdID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads_text`
--

INSERT INTO `ads_text` (`AdTextID`, `AdID`, `Title`, `ProductID`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Ad 1', 2, 1, '2018-09-10 12:21:08', '2019-10-11 17:17:33', 1, 1),
(2, 2, 'Ad 2', 4, 1, '2018-09-11 05:57:29', '2019-10-11 17:17:47', 1, 1),
(3, 3, 'Ad 3', 5, 1, '2018-09-11 05:59:09', '2019-10-11 17:17:57', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `BookingID` int(11) NOT NULL,
  `OrderTrackID` varchar(255) NOT NULL,
  `TimeStart` varchar(255) NOT NULL,
  `TimeEnd` varchar(255) NOT NULL,
  `Status` enum('Pending','Approved') NOT NULL DEFAULT 'Pending',
  `UserID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`BookingID`, `OrderTrackID`, `TimeStart`, `TimeEnd`, `Status`, `UserID`, `CreatedAt`, `UpdatedAt`) VALUES
(1, '39931', '1571843700', '1571847300', 'Pending', 41, '2019-10-23 09:43:52', '2019-10-23 09:43:52'),
(2, '73472', '1571850900', '1571854500', 'Pending', 41, '2019-10-23 09:44:20', '2019-10-23 09:44:20'),
(3, '56033', '0', '3600', 'Pending', 41, '2019-10-23 10:37:53', '2019-10-23 10:37:53');

-- --------------------------------------------------------

--
-- Table structure for table `booking_services`
--

CREATE TABLE `booking_services` (
  `BookingServiceID` int(11) NOT NULL,
  `BookingID` int(11) NOT NULL,
  `ServiceID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_services`
--

INSERT INTO `booking_services` (`BookingServiceID`, `BookingID`, `ServiceID`) VALUES
(1, 1, 4),
(2, 2, 4),
(3, 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `branchs`
--

CREATE TABLE `branchs` (
  `BranchID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL COMMENT 'store user id',
  `CityID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branchs`
--

INSERT INTO `branchs` (`BranchID`, `UserID`, `CityID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 12, 11, 2, 0, 1, '2018-07-12 08:44:57', '2018-07-12 08:50:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branchs_text`
--

CREATE TABLE `branchs_text` (
  `BranchTextID` int(11) NOT NULL,
  `BranchID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branchs_text`
--

INSERT INTO `branchs_text` (`BranchTextID`, `BranchID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(3, 3, 'New Branch Update', 1, '2018-07-12 08:44:57', '2018-07-12 08:50:08', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/images/1465060870420191011012045Current-Specials.jpg', 0, 0, 1, '2018-07-04 08:40:23', '2019-10-11 13:45:20', 1, 1),
(2, 'uploads/images/9810651890320191011010255getlstd-property-photo.jpg', 1, 0, 1, '2018-07-04 08:40:32', '2019-10-11 13:55:02', 1, 1),
(4, 'uploads/images/3509260473620191011010349download-3.jpg', 3, 0, 1, '2018-09-13 07:35:57', '2019-10-11 13:49:03', 1, 1),
(5, 'uploads/images/4686265476520191011010447Screen_Shot_2019-10-11_at_4.46.22_PM.png', 4, 0, 1, '2018-09-13 07:35:57', '2019-10-11 13:47:04', 1, 1),
(7, 'uploads/images/3141267333520191011011650download-4.jpg', 5, 0, 1, '2019-10-11 13:50:16', '2019-10-11 13:50:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories_text`
--

CREATE TABLE `categories_text` (
  `CategoryTextID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories_text`
--

INSERT INTO `categories_text` (`CategoryTextID`, `CategoryID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Derma Clinic', 1, '2018-07-04 08:40:23', '2019-10-11 13:45:20', 1, 1),
(2, 2, 'Massage Centers', 1, '2018-07-04 08:40:32', '2019-10-11 13:55:02', 1, 1),
(4, 4, 'Hair Salon', 1, '2018-09-13 07:35:57', '2019-10-11 13:49:03', 1, 1),
(5, 5, 'Beauty Salon', 1, '2018-09-13 07:35:57', '2019-10-11 13:47:04', 1, 1),
(7, 7, 'Fitness Center', 1, '2019-10-11 13:50:16', '2019-10-11 13:50:16', 1, 1),
(8, 7, 'gymnasium', 2, '2019-10-11 13:50:27', '2019-10-11 13:50:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `CityID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `DistrictID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(147, 37, 73, 0, 1, '2019-10-11 16:56:58', '2019-10-11 16:56:58', 1, 1),
(148, 37, 74, 0, 1, '2019-10-11 16:57:28', '2019-10-11 16:57:28', 1, 1),
(149, 37, 75, 0, 1, '2019-10-11 16:57:36', '2019-10-11 16:57:36', 1, 1),
(150, 37, 76, 0, 1, '2019-10-11 16:57:45', '2019-10-11 16:57:45', 1, 1),
(151, 37, 77, 0, 1, '2019-10-11 16:57:55', '2019-10-11 16:57:55', 1, 1),
(152, 38, 78, 0, 1, '2019-10-11 16:58:15', '2019-10-11 16:58:15', 1, 1),
(153, 38, 79, 0, 1, '2019-10-11 16:58:22', '2019-10-11 16:58:22', 1, 1),
(154, 38, 80, 0, 1, '2019-10-11 16:58:43', '2019-10-11 16:58:43', 1, 1),
(155, 38, 81, 0, 1, '2019-10-11 16:58:52', '2019-10-11 16:58:52', 1, 1),
(156, 38, 82, 0, 1, '2019-10-11 16:59:02', '2019-10-11 16:59:02', 1, 1),
(157, 37, 83, 0, 1, '2019-10-11 16:59:24', '2019-10-11 16:59:24', 1, 1),
(158, 37, 84, 0, 1, '2019-10-11 16:59:34', '2019-10-11 16:59:34', 1, 1),
(159, 41, 85, 0, 1, '2019-10-11 17:00:01', '2019-10-11 17:00:43', 1, 1),
(160, 41, 86, 0, 1, '2019-10-11 17:00:52', '2019-10-11 17:00:52', 1, 1),
(161, 49, 87, 0, 1, '2019-10-11 17:01:36', '2019-10-11 17:05:05', 1, 1),
(162, 49, 88, 0, 1, '2019-10-11 17:01:43', '2019-10-11 17:05:09', 1, 1),
(163, 49, 89, 0, 1, '2019-10-11 17:01:49', '2019-10-11 17:03:23', 1, 1),
(164, 49, 90, 0, 1, '2019-10-11 17:01:53', '2019-10-11 17:05:13', 1, 1),
(165, 49, 91, 0, 1, '2019-10-11 17:02:11', '2019-10-11 17:05:00', 1, 1),
(166, 49, 92, 0, 1, '2019-10-11 17:02:21', '2019-10-11 17:03:20', 1, 1),
(167, 49, 93, 0, 1, '2019-10-11 17:02:28', '2019-10-11 17:05:22', 1, 1),
(168, 49, 94, 0, 1, '2019-10-11 17:02:37', '2019-10-11 17:05:17', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities_text`
--

CREATE TABLE `cities_text` (
  `CityTextID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities_text`
--

INSERT INTO `cities_text` (`CityTextID`, `CityID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(147, 147, 'Mecca', 1, '2019-10-11 16:56:58', '2019-10-11 16:56:58', 1, 1),
(148, 148, 'Taif', 1, '2019-10-11 16:57:28', '2019-10-11 16:57:28', 1, 1),
(149, 149, 'Bahrah', 1, '2019-10-11 16:57:36', '2019-10-11 16:57:36', 1, 1),
(150, 150, 'Rabigh', 1, '2019-10-11 16:57:45', '2019-10-11 16:57:45', 1, 1),
(151, 151, 'Jiddah', 1, '2019-10-11 16:57:55', '2019-10-11 16:57:55', 1, 1),
(152, 152, 'Medina', 1, '2019-10-11 16:58:15', '2019-10-11 16:58:15', 1, 1),
(153, 153, 'Yanbu', 1, '2019-10-11 16:58:22', '2019-10-11 16:58:22', 1, 1),
(154, 154, 'Al Ula', 1, '2019-10-11 16:58:43', '2019-10-11 16:58:43', 1, 1),
(155, 155, 'Badr', 1, '2019-10-11 16:58:52', '2019-10-11 16:58:52', 1, 1),
(156, 156, 'Khaybar', 1, '2019-10-11 16:59:02', '2019-10-11 16:59:02', 1, 1),
(157, 157, 'Al Lith', 1, '2019-10-11 16:59:24', '2019-10-11 16:59:24', 1, 1),
(158, 158, 'Al Qunfudhah', 1, '2019-10-11 16:59:34', '2019-10-11 16:59:34', 1, 1),
(159, 159, 'Riyadh', 1, '2019-10-11 17:00:01', '2019-10-11 17:00:43', 1, 1),
(160, 160, 'Wadi Ad Dawasir', 1, '2019-10-11 17:00:52', '2019-10-11 17:00:52', 1, 1),
(161, 161, 'Dhahran', 1, '2019-10-11 17:01:36', '2019-10-11 17:05:05', 1, 1),
(162, 162, 'Hafar Al Batin', 1, '2019-10-11 17:01:43', '2019-10-11 17:05:09', 1, 1),
(163, 163, 'Jubail', 1, '2019-10-11 17:01:49', '2019-10-11 17:03:23', 1, 1),
(164, 164, 'Khafji', 1, '2019-10-11 17:01:53', '2019-10-11 17:05:13', 1, 1),
(165, 165, 'Dammam', 1, '2019-10-11 17:02:11', '2019-10-11 17:05:00', 1, 1),
(166, 166, 'Khobar', 1, '2019-10-11 17:02:21', '2019-10-11 17:03:20', 1, 1),
(167, 167, 'Ras Tanura', 1, '2019-10-11 17:02:28', '2019-10-11 17:05:22', 1, 1),
(168, 168, 'Qatif', 1, '2019-10-11 17:02:37', '2019-10-11 17:05:17', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `DistrictID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`DistrictID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(37, 1, 0, 1, '2019-10-11 16:52:12', '2019-10-11 16:52:12', 1, 1),
(38, 2, 0, 1, '2019-10-11 16:52:37', '2019-10-11 16:52:37', 1, 1),
(39, 3, 0, 1, '2019-10-11 16:52:53', '2019-10-11 16:52:53', 1, 1),
(40, 4, 0, 1, '2019-10-11 16:53:06', '2019-10-11 16:53:06', 1, 1),
(41, 5, 0, 1, '2019-10-11 16:53:17', '2019-10-11 16:53:17', 1, 1),
(42, 6, 0, 1, '2019-10-11 16:53:36', '2019-10-11 16:53:36', 1, 1),
(43, 7, 0, 1, '2019-10-11 16:53:48', '2019-10-11 16:53:48', 1, 1),
(44, 8, 0, 1, '2019-10-11 16:54:39', '2019-10-11 16:54:39', 1, 1),
(45, 9, 0, 1, '2019-10-11 16:54:55', '2019-10-11 16:54:55', 1, 1),
(46, 10, 0, 1, '2019-10-11 16:55:12', '2019-10-11 16:55:12', 1, 1),
(47, 11, 0, 1, '2019-10-11 16:55:22', '2019-10-11 16:55:22', 1, 1),
(48, 12, 0, 1, '2019-10-11 16:55:31', '2019-10-11 16:55:31', 1, 1),
(49, 13, 0, 1, '2019-10-11 16:55:47', '2019-10-11 16:55:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts_text`
--

CREATE TABLE `districts_text` (
  `DistrictTextID` int(11) NOT NULL,
  `DistrictID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts_text`
--

INSERT INTO `districts_text` (`DistrictTextID`, `DistrictID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(37, 37, 'Makkah Region', 1, '2019-10-11 16:52:12', '2019-10-11 16:52:12', 1, 1),
(38, 38, 'Madinah', 1, '2019-10-11 16:52:37', '2019-10-11 16:52:37', 1, 1),
(39, 39, 'Tabuk Region', 1, '2019-10-11 16:52:53', '2019-10-11 16:52:53', 1, 1),
(40, 40, 'Qassim Region', 1, '2019-10-11 16:53:06', '2019-10-11 16:53:06', 1, 1),
(41, 41, 'Riyadh Region', 1, '2019-10-11 16:53:17', '2019-10-11 16:53:17', 1, 1),
(42, 42, 'Northern Borders', 1, '2019-10-11 16:53:36', '2019-10-11 16:53:36', 1, 1),
(43, 43, 'Jawf Region', 1, '2019-10-11 16:53:48', '2019-10-11 16:53:48', 1, 1),
(44, 44, 'Hail Region', 1, '2019-10-11 16:54:39', '2019-10-11 16:54:39', 1, 1),
(45, 45, 'Bahah Region', 1, '2019-10-11 16:54:55', '2019-10-11 16:54:55', 1, 1),
(46, 46, 'Jizan Region', 1, '2019-10-11 16:55:12', '2019-10-11 16:55:12', 1, 1),
(47, 47, 'Asir Region', 1, '2019-10-11 16:55:22', '2019-10-11 16:55:22', 1, 1),
(48, 48, 'Najran Region', 1, '2019-10-11 16:55:31', '2019-10-11 16:55:31', 1, 1),
(49, 49, 'Eastern Province', 1, '2019-10-11 16:55:47', '2019-10-11 16:55:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `Email_templateID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`Email_templateID`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 'uploads/email_templates/1210446676201807020635331.jpg', 0, 0, 1, '2018-07-02 17:08:45', '2018-07-02 18:33:35', 1, 1),
(4, 'uploads/email_templates/379150302201807020631102.jpg', 1, 0, 1, '2018-07-02 18:10:31', '2018-07-02 18:32:06', 1, 1),
(5, 'uploads/email_templates/414693546201807020612353.jpg', 2, 0, 1, '2018-07-02 18:35:12', '2018-07-02 18:35:12', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `ImageID` int(11) NOT NULL,
  `OtherTableID` int(11) NOT NULL,
  `OtherTableName` varchar(255) NOT NULL,
  `FileName` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`ImageID`, `OtherTableID`, `OtherTableName`, `FileName`, `CreatedAt`, `CreatedBy`, `UpdatedAt`, `UpdatedBy`) VALUES
(1, 4, 'products', 'upload/images/6955902874201807260906234d38701d-018d-4e3b-a32e-0630e74b4233.jpeg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 4, 'products', 'upload/images/221082877952018072609062319905336_1664432806903096_8738396590951730304_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 4, 'products', 'upload/images/423147079002018072609062327459217_1214874058642906_4522705625648580089_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 5, 'products', 'uploads/images/20967967328201807260927244d38701d-018d-4e3b-a32e-0630e74b4233.jpeg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 5, 'products', 'uploads/images/882076358102018072609272419905336_1664432806903096_8738396590951730304_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, 5, 'products', 'uploads/images/232980044412018072609272427459217_1214874058642906_4522705625648580089_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 5, 'products', '1.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, 5, 'products', 'uploads/images/4d38701d-018d-4e3b-a32e-0630e74b4233.jpeg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 5, 'products', 'uploads/images/11.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 5, 'products', 'uploads/images/28167985_1227072597423052_4926496255227966038_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 4, 'products', 'uploads/images/12.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 5, 'products', 'uploads/images/', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(13, 5, 'products', 'uploads/images/4d38701d-018d-4e3b-a32e-0630e74b42331.jpeg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(14, 5, 'products', 'uploads/images/1.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 6, 'products', 'uploads/images/563920853492018072609351132650021_2024332184493751_2767094566631768064_n.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 7, 'products', 'uploads/images/8870089036720191011055521download-7.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 8, 'products', 'uploads/images/637090807020191011051124download-7.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 9, 'products', 'uploads/images/4153407996220191011050925download-6.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 10, 'products', 'uploads/images/2862982481020191011055525900.jpg', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `ModuleID` int(11) NOT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `IconClass` varchar(255) NOT NULL,
  `Slug` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`ModuleID`, `ParentID`, `IconClass`, `Slug`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, ' mdi mdi-settings', '#', 1, 0, 1, '2018-05-09 00:00:00', '2018-05-09 00:00:00', 1, 1),
(18, 1, ' mdi mdi-view-module', 'module', 0, 0, 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(22, 1, 'mdi mdi-yeast', 'role', 3, 0, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(23, 1, 'mdi mdi-account-multiple-outline', 'user', 4, 0, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(32, 1, 'mdi mdi-city', 'city', 7, 0, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(35, 1, 'mdi mdi-city', 'district', 6, 0, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(36, 1, ' mdi mdi-settings-box', 'site_setting', 8, 0, 1, '2018-06-21 20:24:33', '2018-07-03 10:22:27', 1, 1),
(38, 0, 'mdi mdi-format-list-bulleted', 'category', 9, 0, 1, '2018-07-04 08:38:42', '2018-07-04 08:38:42', 1, 1),
(39, 0, 'mdi mdi-shuffle-disabled', 'service', 10, 0, 1, '2018-07-05 08:46:01', '2018-07-05 09:32:41', 1, 1),
(40, 0, 'mdi mdi-shuffle-disabled', 'store_service', 11, 0, 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(41, 0, 'mdi mdi-shuffle-disabled', 'store', 12, 0, 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(42, 0, 'mdi mdi-hospital-building', 'branch', 13, 0, 0, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(43, 0, 'mdi mdi-format-list-bulleted', 'product', 14, 0, 0, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(44, 0, 'mdi mdi-hospital-building', 'order', 15, 0, 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(45, 44, 'mdi mdi-format-list-bulleted', 'order/index/Received', 16, 0, 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(46, 44, 'mdi mdi-format-list-bulleted', 'order/index/Dispatched', 17, 0, 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(47, 44, 'mdi mdi-format-list-bulleted', 'order/index/Delivered', 18, 0, 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(48, 0, 'mdi mdi-hospital-building', '#', 19, 0, 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(49, 48, 'mdi mdi-view-module', 'booking/index/Pending', 20, 0, 1, '2018-09-03 00:00:00', '2018-09-03 00:00:00', 1, 1),
(50, 48, 'mdi mdi-view-module', 'booking/index/Approved', 21, 0, 1, '2018-09-03 00:00:00', '2018-09-03 00:00:00', 1, 1),
(51, 0, 'mdi mdi-hospital-building', 'ad', 22, 0, 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(52, 0, 'mdi mdi-view-module', 'shoppingCard', 23, 0, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_rights`
--

CREATE TABLE `modules_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_rights`
--

INSERT INTO `modules_rights` (`ModuleRightID`, `ModuleID`, `RoleID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(13, 18, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2018-09-13 11:48:20', 1, 1),
(17, 18, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(20, 22, 1, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-09-13 11:48:20', 1, 1),
(21, 22, 2, 1, 1, 1, 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(22, 23, 1, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-09-13 11:48:20', 1, 1),
(23, 23, 2, 1, 1, 1, 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(40, 32, 1, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-09-13 11:48:20', 1, 1),
(41, 32, 2, 1, 1, 1, 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(46, 35, 1, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-09-13 11:48:20', 1, 1),
(47, 35, 2, 1, 1, 1, 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(52, 18, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2018-07-12 09:10:24', 1, 1),
(54, 22, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2018-07-12 09:10:24', 1, 1),
(55, 23, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2018-07-12 09:10:24', 1, 1),
(56, 32, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2018-07-12 09:10:24', 1, 1),
(57, 35, 3, 0, 0, 0, 0, '2018-05-04 17:33:54', '2018-07-12 09:10:24', 1, 1),
(74, 1, 3, 0, 0, 0, 0, '2018-05-09 12:20:50', '2018-07-12 09:10:24', 1, 1),
(75, 1, 2, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-05-09 12:20:50', 1, 1),
(76, 1, 1, 1, 1, 1, 1, '2018-05-09 12:20:50', '2018-09-13 11:48:20', 1, 1),
(77, 36, 1, 1, 1, 1, 1, '2018-02-12 16:32:10', '2018-09-13 11:48:20', 1, 1),
(78, 36, 2, 1, 1, 1, 1, '2018-06-29 13:51:40', '2018-06-29 13:51:40', 1, 1),
(79, 36, 3, 0, 0, 0, 0, '2018-06-29 09:34:16', '2018-07-12 09:10:24', 1, 1),
(83, 38, 1, 1, 1, 1, 1, '2018-07-04 08:38:42', '2018-09-13 11:48:20', 1, 1),
(84, 38, 2, 1, 1, 1, 1, '2018-07-04 08:38:42', '2018-07-04 08:38:42', 1, 1),
(85, 38, 3, 0, 0, 0, 0, '2018-07-04 08:38:42', '2018-07-12 09:10:24', 1, 1),
(86, 39, 1, 1, 1, 1, 1, '2018-07-05 08:46:01', '2018-09-13 11:48:20', 1, 1),
(87, 39, 2, 1, 1, 1, 1, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(88, 39, 3, 0, 0, 0, 0, '2018-07-05 08:46:01', '2018-07-12 09:10:24', 1, 1),
(89, 40, 1, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-09-13 11:48:20', 1, 1),
(90, 40, 2, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(91, 40, 3, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-07-12 09:10:24', 1, 1),
(92, 41, 1, 1, 1, 1, 1, '2018-07-11 07:28:32', '2018-09-13 11:48:20', 1, 1),
(93, 41, 2, 1, 1, 1, 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(94, 41, 3, 1, 1, 1, 0, '2018-07-11 07:28:32', '2018-07-12 09:10:24', 1, 1),
(95, 42, 1, 1, 1, 1, 1, '2018-07-12 07:55:41', '2018-09-13 11:48:20', 1, 1),
(96, 42, 2, 1, 1, 1, 1, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(97, 42, 3, 0, 0, 0, 0, '2018-07-12 07:55:41', '2018-07-12 09:10:24', 1, 1),
(98, 43, 1, 1, 1, 1, 1, '2018-07-24 08:04:44', '2018-09-13 11:48:20', 1, 1),
(99, 43, 2, 1, 1, 1, 1, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(100, 43, 3, 0, 0, 0, 0, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(101, 44, 1, 1, 1, 1, 1, '2018-08-08 08:18:28', '2018-09-13 11:48:20', 1, 1),
(102, 44, 2, 1, 1, 1, 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(103, 44, 3, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(104, 44, 4, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(105, 45, 1, 1, 1, 1, 1, '2018-08-09 08:06:00', '2018-09-13 11:48:20', 1, 1),
(106, 45, 2, 1, 1, 1, 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(107, 45, 3, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(108, 45, 4, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(109, 46, 1, 1, 1, 1, 1, '2018-08-09 08:07:40', '2018-09-13 11:48:20', 1, 1),
(110, 46, 2, 1, 1, 1, 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(111, 46, 3, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(112, 46, 4, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(113, 47, 1, 1, 1, 1, 1, '2018-08-09 08:08:42', '2018-09-13 11:48:20', 1, 1),
(114, 47, 2, 1, 1, 1, 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(115, 47, 3, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(116, 47, 4, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(117, 48, 1, 1, 1, 1, 1, '2018-09-03 07:16:02', '2018-09-13 11:48:20', 1, 1),
(118, 48, 2, 1, 1, 1, 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(119, 48, 3, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(120, 48, 4, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(121, 49, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2018-09-13 11:48:20', 1, 1),
(122, 49, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(123, 50, 2, 1, 1, 1, 1, '2018-05-02 11:37:19', '2018-05-02 11:37:19', 1, 1),
(124, 50, 1, 1, 1, 1, 1, '2018-04-19 15:22:06', '2018-09-13 11:48:20', 1, 1),
(125, 51, 1, 1, 1, 1, 1, '2018-09-10 12:15:58', '2018-09-13 11:48:20', 1, 1),
(126, 51, 2, 1, 1, 1, 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(127, 51, 3, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(128, 51, 4, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(129, 52, 1, 1, 1, 1, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(130, 52, 2, 1, 1, 1, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(131, 52, 3, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(132, 52, 4, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_text`
--

CREATE TABLE `modules_text` (
  `ModuleTextID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_text`
--

INSERT INTO `modules_text` (`ModuleTextID`, `ModuleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 18, 'Module', 1, '2018-04-19 15:22:06', '2018-04-19 15:22:06', 1, 1),
(5, 22, 'Roles', 1, '2018-05-02 12:10:33', '2018-05-02 12:10:33', 1, 1),
(6, 23, 'Users', 1, '2018-05-02 12:44:19', '2018-05-02 12:44:19', 1, 1),
(15, 32, 'City', 1, '2018-05-03 14:01:59', '2018-05-03 14:01:59', 1, 1),
(18, 35, 'District', 1, '2018-05-03 15:49:43', '2018-05-03 15:49:43', 1, 1),
(26, 1, 'Setting', 1, '2018-05-09 00:00:00', '2018-05-09 00:00:00', 1, 1),
(27, 36, 'Site Setting', 1, '2018-06-29 07:34:18', '2018-07-03 10:22:27', 1, 1),
(29, 38, 'Category', 1, '2018-07-04 08:38:42', '2018-07-04 08:38:42', 1, 1),
(30, 39, 'Services', 1, '2018-07-05 08:46:01', '2018-07-05 09:32:41', 1, 1),
(31, 40, 'Store Services', 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(32, 41, 'Stores', 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(33, 42, 'Branches', 1, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(34, 43, 'Products', 1, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(35, 44, 'Order', 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(36, 45, 'Received Orders', 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(37, 46, 'Dispatched Orders', 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(38, 47, 'Delivered Orders', 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(39, 48, 'Bookings', 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(40, 49, 'Pendings', 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(41, 50, 'Approved', 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(42, 51, 'Ads', 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(43, 52, 'Shopping Cards', 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules_users_rights`
--

CREATE TABLE `modules_users_rights` (
  `ModuleRightID` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `CanView` tinyint(4) NOT NULL,
  `CanAdd` tinyint(4) NOT NULL,
  `CanEdit` tinyint(4) NOT NULL,
  `CanDelete` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_users_rights`
--

INSERT INTO `modules_users_rights` (`ModuleRightID`, `ModuleID`, `UserID`, `CanView`, `CanAdd`, `CanEdit`, `CanDelete`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(10, 1, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(11, 18, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(12, 22, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(13, 23, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(14, 32, 7, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(15, 35, 7, 0, 0, 0, 0, '2018-06-28 10:55:57', '2018-06-28 11:21:04', 1, 1),
(16, 36, 7, 1, 1, 1, 1, '2018-06-29 12:42:13', '2018-06-29 12:42:13', 1, 1),
(17, 1, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(18, 18, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(19, 22, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(20, 23, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(21, 32, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(22, 35, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(23, 36, 1, 1, 1, 1, 1, '2018-06-29 12:42:13', '2018-06-29 12:42:13', 1, 1),
(28, 39, 1, 1, 1, 1, 1, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(29, 39, 7, 0, 0, 0, 0, '2018-07-02 16:26:28', '2018-07-02 16:26:28', 1, 1),
(30, 37, 1, 1, 1, 1, 1, '2018-07-04 08:21:24', '2018-07-04 08:21:24', 1, 1),
(31, 37, 7, 0, 0, 0, 0, '2018-07-04 08:21:24', '2018-07-04 08:21:24', 1, 1),
(32, 38, 1, 1, 1, 1, 1, '2018-07-04 08:38:42', '2018-07-04 08:38:42', 1, 1),
(33, 38, 7, 0, 0, 0, 0, '2018-07-04 08:38:42', '2018-07-04 08:38:42', 1, 1),
(34, 1, 8, 1, 1, 1, 1, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(35, 18, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(36, 22, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(37, 23, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(38, 32, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(39, 35, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(40, 36, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(41, 38, 8, 0, 0, 0, 0, '2018-07-04 09:40:51', '2018-07-04 09:40:51', 1, 1),
(42, 1, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(43, 18, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(44, 22, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(45, 23, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(46, 32, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(47, 35, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(48, 36, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(49, 38, 9, 1, 1, 1, 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(50, 1, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(51, 18, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(52, 22, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(53, 23, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(54, 32, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(55, 35, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(56, 36, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(57, 38, 10, 1, 1, 1, 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(58, 39, 1, 1, 1, 1, 1, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(59, 39, 7, 0, 0, 0, 0, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(60, 39, 8, 0, 0, 0, 0, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(61, 39, 9, 1, 1, 1, 1, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(62, 39, 10, 1, 1, 1, 1, '2018-07-05 08:46:01', '2018-07-05 08:46:01', 1, 1),
(63, 40, 1, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(64, 40, 7, 0, 0, 0, 0, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(65, 40, 8, 0, 0, 0, 0, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(66, 40, 9, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(67, 40, 10, 1, 1, 1, 1, '2018-07-05 09:34:27', '2018-07-05 09:34:27', 1, 1),
(78, 41, 1, 1, 1, 1, 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(79, 41, 9, 1, 1, 1, 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(80, 41, 10, 1, 1, 1, 1, '2018-07-11 07:28:32', '2018-07-11 07:28:32', 1, 1),
(93, 1, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(94, 18, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(95, 22, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(96, 23, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(97, 32, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(98, 35, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(99, 36, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(100, 38, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(101, 39, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(102, 40, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(103, 41, 13, 0, 0, 0, 0, '2018-07-11 07:52:43', '2018-07-11 07:52:43', 1, 1),
(104, 1, 14, 0, 0, 0, 0, '2018-07-11 07:54:03', '2018-07-11 07:54:03', 1, 1),
(105, 18, 14, 0, 0, 0, 0, '2018-07-11 07:54:03', '2018-07-11 07:54:03', 1, 1),
(106, 22, 14, 0, 0, 0, 0, '2018-07-11 07:54:03', '2018-07-11 07:54:03', 1, 1),
(107, 23, 14, 0, 0, 0, 0, '2018-07-11 07:54:03', '2018-07-11 07:54:03', 1, 1),
(108, 32, 14, 0, 0, 0, 0, '2018-07-11 07:54:03', '2018-07-11 07:54:03', 1, 1),
(109, 35, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(110, 36, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(111, 38, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(112, 39, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(113, 40, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(114, 41, 14, 0, 0, 0, 0, '2018-07-11 07:54:04', '2018-07-11 07:54:04', 1, 1),
(126, 42, 1, 1, 1, 1, 1, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(127, 42, 9, 1, 1, 1, 1, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(128, 42, 10, 1, 1, 1, 1, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(131, 42, 13, 0, 0, 0, 0, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(132, 42, 14, 0, 0, 0, 0, '2018-07-12 07:55:41', '2018-07-12 07:55:41', 1, 1),
(134, 1, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(135, 18, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(136, 22, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(137, 23, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(138, 32, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(139, 35, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(140, 36, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(141, 38, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(142, 39, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(143, 40, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(144, 41, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(145, 42, 16, 0, 0, 0, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(146, 43, 1, 1, 1, 1, 1, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(147, 43, 9, 1, 1, 1, 1, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(148, 43, 10, 1, 1, 1, 1, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(151, 43, 13, 0, 0, 0, 0, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(152, 43, 14, 0, 0, 0, 0, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(154, 43, 16, 0, 0, 0, 0, '2018-07-24 08:04:44', '2018-07-24 08:04:44', 1, 1),
(155, 44, 1, 1, 1, 1, 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(156, 44, 9, 1, 1, 1, 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(157, 44, 10, 1, 1, 1, 1, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(160, 44, 13, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(161, 44, 14, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(163, 44, 16, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(164, 44, 18, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(165, 44, 19, 0, 0, 0, 0, '2018-08-08 08:18:28', '2018-08-08 08:18:28', 1, 1),
(166, 45, 1, 1, 1, 1, 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(167, 45, 9, 1, 1, 1, 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(168, 45, 10, 1, 1, 1, 1, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(171, 45, 13, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(172, 45, 14, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(174, 45, 16, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(175, 45, 18, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(176, 45, 19, 0, 0, 0, 0, '2018-08-09 08:06:00', '2018-08-09 08:06:00', 1, 1),
(177, 46, 1, 1, 1, 1, 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(178, 46, 9, 1, 1, 1, 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(179, 46, 10, 1, 1, 1, 1, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(182, 46, 13, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(183, 46, 14, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(185, 46, 16, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(186, 46, 18, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(187, 46, 19, 0, 0, 0, 0, '2018-08-09 08:07:40', '2018-08-09 08:07:40', 1, 1),
(188, 47, 1, 1, 1, 1, 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(189, 47, 9, 1, 1, 1, 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(190, 47, 10, 1, 1, 1, 1, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(193, 47, 13, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(194, 47, 14, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(196, 47, 16, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(197, 47, 18, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(198, 47, 19, 0, 0, 0, 0, '2018-08-09 08:08:42', '2018-08-09 08:08:42', 1, 1),
(199, 48, 1, 1, 1, 1, 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(200, 48, 9, 1, 1, 1, 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(201, 48, 10, 1, 1, 1, 1, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(204, 48, 13, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(205, 48, 14, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(207, 48, 16, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(208, 48, 18, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(209, 48, 19, 0, 0, 0, 0, '2018-09-03 07:16:02', '2018-09-03 07:16:02', 1, 1),
(210, 49, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(211, 50, 1, 1, 1, 1, 1, '2018-06-28 10:55:57', '2018-06-28 10:55:57', 1, 1),
(232, 51, 1, 1, 1, 1, 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(233, 51, 9, 1, 1, 1, 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(234, 51, 10, 1, 1, 1, 1, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(237, 51, 13, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(238, 51, 14, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(240, 51, 16, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(241, 51, 24, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(242, 51, 25, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(243, 51, 26, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(244, 51, 27, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(245, 51, 28, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(246, 51, 29, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(248, 51, 31, 0, 0, 0, 0, '2018-09-10 12:15:58', '2018-09-10 12:15:58', 1, 1),
(249, 52, 1, 1, 1, 1, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(250, 52, 9, 1, 1, 1, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(251, 52, 10, 1, 1, 1, 1, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(254, 52, 13, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(255, 52, 14, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(257, 52, 16, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(258, 52, 24, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(259, 52, 25, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(260, 52, 26, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(261, 52, 27, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(262, 52, 28, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(263, 52, 29, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(265, 52, 31, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(266, 52, 32, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(267, 52, 33, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(268, 52, 34, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(269, 52, 35, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(270, 52, 36, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(271, 52, 37, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(272, 52, 39, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(273, 52, 40, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(274, 52, 41, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(275, 52, 42, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(276, 52, 43, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(277, 52, 44, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(278, 52, 45, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(279, 52, 46, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(280, 52, 47, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(281, 52, 48, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(282, 52, 49, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(283, 52, 50, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(284, 52, 51, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(285, 52, 52, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(286, 52, 53, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(287, 52, 54, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(288, 52, 55, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(289, 52, 56, 0, 0, 0, 0, '2019-03-07 07:29:40', '2019-03-07 07:29:40', 1, 1),
(290, 1, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(291, 18, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(292, 22, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(293, 23, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(294, 32, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(295, 35, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(296, 36, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(297, 38, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(298, 39, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(299, 40, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(300, 41, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(301, 42, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(302, 43, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(303, 44, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(304, 45, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(305, 46, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(306, 47, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(307, 48, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(308, 49, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(309, 50, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(310, 51, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(311, 52, 58, 0, 0, 0, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `OrderTrackID` varchar(255) NOT NULL,
  `UserID` int(11) NOT NULL,
  `AddressID` int(11) NOT NULL,
  `Status` enum('Received','Dispatched','Delivered') NOT NULL DEFAULT 'Received',
  `DriverID` int(11) NOT NULL,
  `DeliveryOTP` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderID`, `OrderTrackID`, `UserID`, `AddressID`, `Status`, `DriverID`, `DeliveryOTP`, `CreatedAt`, `UpdatedAt`) VALUES
(1, '48401', 1536926126, 8, 'Received', 0, '', '2018-09-14 07:57:03', '2018-09-14 07:57:03'),
(2, '96402', 31, 4, 'Received', 0, '', '2018-09-14 13:11:19', '2018-09-14 13:11:19'),
(3, '42363', 31, 4, 'Received', 0, '', '2018-09-14 13:14:56', '2018-09-14 13:14:56'),
(4, '56104', 33, 10, 'Received', 0, '', '2018-09-14 15:45:15', '2018-09-14 15:45:15'),
(5, '58715', 33, 10, 'Received', 0, '', '2018-09-14 20:40:21', '2018-09-14 20:40:21'),
(6, '02746', 50, 13, 'Received', 0, '', '2019-02-26 11:22:49', '2019-02-26 11:22:49'),
(7, '80807', 45, 19, 'Received', 0, '', '2019-02-26 11:26:24', '2019-02-26 11:26:24'),
(8, '98128', 43, 21, 'Received', 0, '', '2019-02-26 12:33:31', '2019-02-26 12:33:31'),
(9, '76269', 57, 23, 'Received', 0, '', '2019-03-11 09:17:55', '2019-03-11 09:17:55'),
(10, '252310', 41, 26, 'Received', 0, '', '2019-10-22 06:25:34', '2019-10-22 06:25:34'),
(11, '110511', 41, 30, 'Received', 0, '', '2019-10-22 07:52:25', '2019-10-22 07:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `OrderItemID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`OrderItemID`, `OrderID`, `ProductID`, `Quantity`, `Price`) VALUES
(1, 1, 1, 12, 120),
(2, 2, 5, 2, 100),
(3, 3, 6, 4, 100),
(4, 4, 6, 2, 100),
(5, 5, 6, 45, 100),
(6, 6, 6, 2, 100),
(7, 7, 6, 2, 100),
(8, 8, 1, 1, 120),
(9, 9, 1, 1, 120),
(10, 10, 8, 1, 65.02),
(11, 10, 9, 1, 65),
(12, 11, 8, 1, 65.02);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL COMMENT 'User ID of role Store',
  `Price` float NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductID`, `UserID`, `Price`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 14, 120, 0, 0, 1, '2018-07-24 08:48:50', '2018-07-24 09:14:40', 1, 1),
(2, 14, 100, 1, 0, 1, '2018-07-26 09:21:09', '2018-07-26 09:21:09', 1, 1),
(3, 14, 100, 2, 0, 1, '2018-07-26 09:22:30', '2018-07-26 09:22:30', 1, 1),
(4, 14, 100, 3, 0, 1, '2018-07-26 09:23:06', '2018-07-26 09:23:06', 1, 1),
(5, 14, 100, 4, 0, 1, '2018-07-26 09:24:27', '2018-07-26 09:24:27', 1, 1),
(8, 13, 65.02, 5, 0, 1, '2019-10-11 17:24:11', '2019-10-11 17:24:11', 1, 1),
(9, 13, 65, 6, 0, 1, '2019-10-11 17:25:09', '2019-10-11 17:25:09', 1, 1),
(10, 13, 12, 7, 0, 1, '2019-10-11 17:25:55', '2019-10-11 17:25:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_text`
--

CREATE TABLE `products_text` (
  `ProductTextID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(5000) NOT NULL,
  `Specifications` varchar(500) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_text`
--

INSERT INTO `products_text` (`ProductTextID`, `ProductID`, `Title`, `Description`, `Specifications`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'First Product', 'This is description of first product updae', 'Specifications1,Specifications2,Specifications3', 1, '2018-07-24 08:48:50', '2018-07-24 09:14:40', 1, 1),
(2, 2, 'First', 'this is testing', 'Specifications1,Specifications2,Specifications3', 1, '2018-07-26 09:21:09', '2018-07-26 09:21:09', 1, 1),
(3, 3, 'First as', 'this is testing', 'Specifications1,Specifications2,Specifications3', 1, '2018-07-26 09:22:30', '2018-07-26 09:22:30', 1, 1),
(4, 4, 'As', 'this is testing', 'Specifications1,Specifications2,Specifications3', 1, '2018-07-26 09:23:06', '2018-07-26 09:23:06', 1, 1),
(5, 5, 'Asf', 'this is testing', 'Specifications1,Specifications2,Specifications3', 1, '2018-07-26 09:24:27', '2018-07-26 09:24:27', 1, 1),
(8, 8, 'Hairburst Longer Stronger Hair Shampoo & Conditioner 350ml', 'Our shampoo and conditioner, when used together, will help you on your way to longer, stronger, more radiant hair. Whether you have fine, thin, ...', 'Fit Blast,Dark and Tan,350ml', 1, '2019-10-11 17:24:11', '2019-10-11 17:24:11', 1, 1),
(9, 9, 'Rehab', 'Just can\'t get enough? Now available in 1L online and in select stores. , Bleach, dye, styling and heat can really take a toll on your hair\'s health.', '350ml', 1, '2019-10-11 17:25:09', '2019-10-11 17:25:09', 1, 1),
(10, 10, 'Pantene Pro-V Curly Hair Series Shampoo', 'Helps to smooth & seal hair edges Helps keep curls defined Frizz calming & damage-defying complex formula', '750ml', 1, '2019-10-11 17:25:55', '2019-10-11 17:25:55', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `RoleID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `IsActive` tinyint(4) NOT NULL,
  `CreatedBy` datetime NOT NULL,
  `UpdatedBy` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`RoleID`, `SortOrder`, `Hide`, `IsActive`, `CreatedBy`, `UpdatedBy`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 0, 0, 1, '0000-00-00 00:00:00', 0, '2018-04-03 00:00:00', 2147483647),
(2, 1, 0, 1, '0000-00-00 00:00:00', 0, '2018-05-02 11:37:19', 2147483647),
(3, 2, 0, 1, '0000-00-00 00:00:00', 0, '2018-05-04 17:33:54', 2147483647),
(4, 4, 0, 1, '2018-08-06 00:00:00', 1, '2018-08-06 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles_text`
--

CREATE TABLE `roles_text` (
  `RoleTextID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_text`
--

INSERT INTO `roles_text` (`RoleTextID`, `RoleID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Admin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, 2, 'Super Admin', 1, '2018-05-02 11:37:19', '2018-05-02 12:03:34', 1, 1),
(3, 2, 'Super Admin', 2, '2018-05-02 11:37:28', '2018-05-02 11:37:28', 1, 1),
(4, 3, 'Store', 1, '2018-05-04 17:33:54', '2018-06-27 13:41:29', 1, 1),
(5, 4, 'Customer', 1, '2018-08-06 00:00:00', '2018-08-06 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `screening_camps`
--

CREATE TABLE `screening_camps` (
  `ScreeningCampID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `screening_camps`
--

INSERT INTO `screening_camps` (`ScreeningCampID`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 0, 1, '2018-05-07 16:37:40', '2018-05-07 16:37:40', 1, 1),
(2, 1, 0, 1, '2018-05-07 16:37:53', '2018-05-07 16:37:53', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `ServiceID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`ServiceID`, `SortOrder`, `Image`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 0, 'uploads/services/9296462401320191011010526download.jpg', 0, 1, '2018-07-05 09:09:58', '2019-10-11 13:26:05', 1, 1),
(4, 2, 'uploads/services/9281936615720191008010451shutterstock_136867901-1.jpg', 0, 1, '2019-10-08 13:51:04', '2019-10-08 13:51:10', 1, 1),
(5, 3, 'uploads/services/5095934338420191011011032download-1.jpg', 0, 1, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(6, 4, 'uploads/services/238780871020191011014334what-is-a-pedicure-what-does-a-pedicure-feel-like.png', 0, 1, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(7, 5, 'uploads/services/37226905384201910110136360bbc842579c5f49386d997a5041cbe05-prom-hair-flowers-updo-rose-hair-updo-e1508184802212.jpg', 0, 1, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(8, 6, 'uploads/services/375498920201910110134372018_11_15_58744_1542277514._large.jpg', 0, 1, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_text`
--

CREATE TABLE `services_text` (
  `ServiceTextID` int(11) NOT NULL,
  `ServiceID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_text`
--

INSERT INTO `services_text` (`ServiceTextID`, `ServiceID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Spa', 1, '2018-07-05 09:09:58', '2019-10-11 13:26:05', 1, 1),
(4, 4, 'Manicure', 1, '2019-10-08 13:51:04', '2019-10-08 13:51:10', 1, 1),
(5, 1, 'Spa', 2, '2019-10-11 13:26:02', '2019-10-11 13:26:02', 1, 1),
(6, 5, 'Tanning', 1, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(7, 5, 'Tanning', 2, '2019-10-11 13:32:18', '2019-10-11 13:33:15', 1, 1),
(8, 6, 'Pedicure', 1, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(9, 6, 'Pedicure', 2, '2019-10-11 13:35:39', '2019-10-11 13:35:51', 1, 1),
(10, 7, 'Hair Styling', 1, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(11, 7, 'hairstyling', 2, '2019-10-11 13:36:43', '2019-10-11 13:36:43', 1, 1),
(12, 8, 'Waxing', 1, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(13, 8, 'Waxing', 2, '2019-10-11 13:37:41', '2019-10-11 13:37:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cards`
--

CREATE TABLE `shopping_cards` (
  `ShoppingCardID` int(11) NOT NULL,
  `CardPrice` decimal(10,2) NOT NULL,
  `CardValidityDate` date NOT NULL,
  `CardCount` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shopping_cards`
--

INSERT INTO `shopping_cards` (`ShoppingCardID`, `CardPrice`, `CardValidityDate`, `CardCount`, `Image`, `SortOrder`, `Hide`, `IsActive`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 100.00, '2019-03-28', 149, 'uploads/images/564992201903070916136212172317267285393.jpg', 0, 0, 1, '2019-03-07 07:53:10', '2019-03-07 09:40:48', 1, 1),
(2, 200.00, '2019-03-22', 47, 'uploads/images/141387201903070959571410755526420190225094518security-camera-roundup-pic-1.jpg', 1, 0, 1, '2019-03-07 09:57:59', '2019-03-07 09:57:59', 1, 1),
(3, 500.00, '2019-03-15', 16, 'uploads/images/835162201903071016166212172317267285393.jpg', 2, 0, 1, '2019-03-07 10:16:16', '2019-03-07 10:16:16', 1, 1),
(4, 100.00, '2020-06-11', 59, 'uploads/images/336190201903071050241410755526420190225094518security-camera-roundup-pic-1.jpg', 3, 0, 1, '2019-03-07 10:24:50', '2019-03-14 09:46:26', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cards_text`
--

CREATE TABLE `shopping_cards_text` (
  `ShoppingCardTextID` int(11) NOT NULL,
  `ShoppingCardID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shopping_cards_text`
--

INSERT INTO `shopping_cards_text` (`ShoppingCardTextID`, `ShoppingCardID`, `Title`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 1, 'Card 1', 1, '2019-03-07 07:53:10', '2019-03-07 09:40:48', 1, 1),
(2, 1, 'Card 1', 2, '2019-03-07 07:53:10', '2019-03-07 07:53:10', 1, 1),
(3, 2, 'Card 2', 1, '2019-03-07 09:57:59', '2019-03-07 09:57:59', 1, 1),
(4, 2, 'Card 2', 2, '2019-03-07 09:57:59', '2019-03-07 09:57:59', 1, 1),
(5, 3, 'Card 3', 1, '2019-03-07 10:16:16', '2019-03-07 10:16:16', 1, 1),
(6, 3, 'Card 3', 2, '2019-03-07 10:16:16', '2019-03-07 10:16:16', 1, 1),
(7, 4, 'Card 5', 1, '2019-03-07 10:24:50', '2019-03-14 09:46:26', 1, 1),
(8, 4, 'Card 5', 2, '2019-03-07 10:24:50', '2019-03-07 10:24:50', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_card_numbers`
--

CREATE TABLE `shopping_card_numbers` (
  `ShoppingCardNumberID` int(11) NOT NULL,
  `ShoppingCardID` int(11) NOT NULL,
  `CardNumber` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopping_card_numbers`
--

INSERT INTO `shopping_card_numbers` (`ShoppingCardNumberID`, `ShoppingCardID`, `CardNumber`) VALUES
(1, 3, 'UQyIs45M'),
(2, 2, 'RsJP57De'),
(3, 3, 'YkK41oRk');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_card_purchase`
--

CREATE TABLE `shopping_card_purchase` (
  `ShoppingCardPurchaseID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ShoppingCardID` int(11) NOT NULL,
  `PurchasedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopping_card_purchase`
--

INSERT INTO `shopping_card_purchase` (`ShoppingCardPurchaseID`, `UserID`, `ShoppingCardID`, `PurchasedAt`) VALUES
(1, 45, 3, '2019-03-11 06:54:45'),
(2, 57, 2, '2019-03-11 07:09:41'),
(3, 41, 3, '2019-03-11 11:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_card_received`
--

CREATE TABLE `shopping_card_received` (
  `ShoppingCardReceivedID` int(11) NOT NULL,
  `ShoppingCardID` int(11) NOT NULL,
  `ShoppingCardSharedBy` int(11) NOT NULL,
  `ShoppingCardReceivedBy` int(11) NOT NULL,
  `ShoppingCardAddedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopping_card_received`
--

INSERT INTO `shopping_card_received` (`ShoppingCardReceivedID`, `ShoppingCardID`, `ShoppingCardSharedBy`, `ShoppingCardReceivedBy`, `ShoppingCardAddedAt`) VALUES
(1, 2, 57, 45, '2019-03-11 07:11:05'),
(2, 3, 45, 57, '2019-03-11 07:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_card_transactions`
--

CREATE TABLE `shopping_card_transactions` (
  `ShoppingCardTransactionID` int(11) NOT NULL,
  `ShoppingCardID` int(11) NOT NULL,
  `UsedBy` int(11) NOT NULL,
  `AmountUsed` decimal(10,2) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `UsedAt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `SiteSettingID` int(11) NOT NULL,
  `SiteName` varchar(255) NOT NULL,
  `SiteImage` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Whatsapp` varchar(255) NOT NULL,
  `Skype` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `FacebookUrl` varchar(255) NOT NULL,
  `GoogleUrl` varchar(255) NOT NULL,
  `LinkedInUrl` varchar(255) NOT NULL,
  `TwitterUrl` varchar(255) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`SiteSettingID`, `SiteName`, `SiteImage`, `Email`, `Whatsapp`, `Skype`, `PhoneNumber`, `Fax`, `FacebookUrl`, `GoogleUrl`, `LinkedInUrl`, `TwitterUrl`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'Glamorous', 'uploads/3377089706320180803072243glam.png', 'info@schopfen.com', '123456789', '', '12345678', '', 'www.facebook.com', 'www.google.com', 'www.linkedin.com', 'www.twitter.com', '2018-04-03 14:33:23', '2019-10-11 17:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `StoreID` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `store_services`
--

CREATE TABLE `store_services` (
  `StoreServiceID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL COMMENT 'User with role of store',
  `ServiceID` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `TimeForService` int(11) NOT NULL,
  `StoreServicePrice` float NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store_services`
--

INSERT INTO `store_services` (`StoreServiceID`, `UserID`, `ServiceID`, `IsActive`, `TimeForService`, `StoreServicePrice`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(1, 11, 1, 1, 120, 0, '2018-07-06 11:05:36', '2018-07-06 11:47:04', 1, 11),
(2, 11, 3, 1, 30, 0, '2018-07-06 11:07:11', '2018-07-06 11:47:04', 1, 11),
(3, 15, 1, 1, 60, 0, '2018-07-11 07:55:57', '2018-07-11 07:55:57', 1, 1),
(4, 15, 3, 1, 60, 0, '2018-07-11 07:55:57', '2018-07-11 07:55:57', 1, 1),
(5, 16, 1, 1, 60, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(6, 16, 3, 1, 60, 0, '2018-07-18 09:00:21', '2018-07-18 09:00:21', 1, 1),
(7, 30, 1, 1, 60, 0, '2018-09-10 08:37:38', '2019-10-11 17:15:08', 1, 1),
(8, 30, 3, 1, 60, 0, '2018-09-10 08:37:38', '2018-09-13 11:51:31', 1, 1),
(9, 11, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-08 13:51:04', 1, 1),
(10, 12, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-08 13:51:04', 1, 1),
(11, 13, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-08 13:51:04', 1, 1),
(12, 14, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-08 13:51:04', 1, 1),
(13, 15, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-08 13:51:04', 1, 1),
(14, 30, 4, 1, 60, 0, '2019-10-08 13:51:04', '2019-10-11 17:15:08', 1, 1),
(15, 11, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(16, 12, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(17, 13, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(18, 14, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(19, 15, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 13:32:10', 1, 1),
(20, 30, 5, 1, 60, 0, '2019-10-11 13:32:10', '2019-10-11 17:15:08', 1, 1),
(21, 11, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(22, 12, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(23, 13, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(24, 14, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(25, 15, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 13:34:43', 1, 1),
(26, 30, 6, 1, 60, 0, '2019-10-11 13:34:43', '2019-10-11 17:15:08', 1, 1),
(27, 11, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(28, 12, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(29, 13, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(30, 14, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(31, 15, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 13:36:36', 1, 1),
(32, 30, 7, 1, 60, 0, '2019-10-11 13:36:36', '2019-10-11 17:15:08', 1, 1),
(33, 11, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(34, 12, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(35, 13, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(36, 14, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(37, 15, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 13:37:34', 1, 1),
(38, 30, 8, 1, 60, 0, '2019-10-11 13:37:34', '2019-10-11 17:15:08', 1, 1),
(39, 58, 1, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(40, 58, 4, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(41, 58, 5, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(42, 58, 6, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(43, 58, 7, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(44, 58, 8, 1, 60, 0, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_languages`
--

CREATE TABLE `system_languages` (
  `SystemLanguageID` int(11) NOT NULL,
  `SystemLanguageTitle` varchar(255) NOT NULL,
  `ShortCode` varchar(255) NOT NULL,
  `IsDefault` tinyint(4) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_languages`
--

INSERT INTO `system_languages` (`SystemLanguageID`, `SystemLanguageTitle`, `ShortCode`, `IsDefault`, `Hide`, `CreatedAt`, `UpdatedAt`) VALUES
(1, 'English', 'EN', 1, 0, '2018-04-09 00:00:00', '2018-04-09 00:00:00'),
(2, 'Arabic', 'AR', 0, 0, '2018-04-09 00:00:00', '2018-04-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `temp_bookings`
--

CREATE TABLE `temp_bookings` (
  `TempBookingID` int(11) NOT NULL,
  `UserID` varchar(255) NOT NULL COMMENT 'we can also use guest id as cookie',
  `ServiceID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_orders`
--

CREATE TABLE `temp_orders` (
  `TempOrderID` int(11) NOT NULL,
  `UserID` varchar(255) NOT NULL COMMENT 'this is actually aa cookie value not user id logic change',
  `ProductID` int(11) NOT NULL,
  `ProductQuantity` bigint(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_orders`
--

INSERT INTO `temp_orders` (`TempOrderID`, `UserID`, `ProductID`, `ProductQuantity`) VALUES
(1, '12', 1, 3),
(3, '12', 3, 2),
(5, '1536936027', 1, 21),
(10, '29', 1, 8),
(13, '558630291', 6, 1),
(15, '12', 4, 5),
(16, '562785099', 2, 2),
(17, '563179964', 6, 1),
(18, '30', 6, 1),
(19, '569257223', 3, 1),
(23, '569766062', 3, 8),
(24, '569766062', 4, 1),
(25, '569764675', 1, 2),
(28, '569766062', 1, 5),
(29, '1549279932', 1, 2),
(30, '571124162', 1, 1),
(31, '571297493', 6, 1),
(37, '572867884', 6, 1),
(38, '572867996', 6, 6),
(45, '1551193454', 5, 2),
(46, '1551193454', 1, 3),
(47, '49', 6, 6),
(48, '1551193714', 6, 1),
(49, '1551193787', 3, 2),
(51, '572860313', 1, 1),
(52, '1551193999', 3, 2),
(53, '1551194067', 1, 3),
(54, '1551194067', 6, 2),
(55, '1551194192', 1, 1),
(56, '49', 5, 14),
(57, '572869633', 5, 1),
(70, '55', 6, 1),
(71, '51', 6, 3),
(78, '45', 6, 0),
(79, '45', 1, 0),
(80, '572877829', 6, 6),
(82, '1551159866', 1, 1),
(83, '1551270202', 1, 1),
(84, '1551270288', 1, 1),
(85, '1551270446', 1, 1),
(93, '-1', 1, 1),
(97, '43', 2, 1),
(98, '43', 1, 1),
(100, '-1', 3, 1),
(101, '37', 9, 2),
(102, '569257223', 8, 1),
(103, '-1', 8, 1),
(104, '592836270', 8, 1),
(105, '59', 8, 4),
(106, '59', 10, 1),
(110, '41', 8, 1),
(113, '60', 8, 1),
(114, '41', 10, 1),
(117, '41', 1, 1),
(118, '61', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `StoreID` int(11) NOT NULL DEFAULT '0' COMMENT 'if user is child of store',
  `CategoryID` int(11) NOT NULL,
  `UserType` varchar(255) DEFAULT NULL COMMENT 'only for store sub user',
  `Gender` enum('','Male','Female') NOT NULL,
  `Mobile` varchar(255) NOT NULL,
  `Phone` varchar(255) NOT NULL,
  `UName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `CityID` int(11) NOT NULL,
  `Country` varchar(255) NOT NULL,
  `Image` varchar(500) NOT NULL,
  `BannerImage` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `OnlineStatus` enum('','Online','Offline') DEFAULT NULL,
  `Rating` int(11) NOT NULL,
  `Hide` tinyint(4) NOT NULL DEFAULT '0',
  `BranchID` varchar(50) NOT NULL DEFAULT '0',
  `OpenTime` varchar(255) NOT NULL,
  `CloseTime` varchar(255) NOT NULL,
  `IsActive` tinyint(4) NOT NULL DEFAULT '1',
  `SortOrder` int(11) NOT NULL,
  `CreatedAt` date NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `RoleID`, `StoreID`, `CategoryID`, `UserType`, `Gender`, `Mobile`, `Phone`, `UName`, `Email`, `Password`, `DateOfBirth`, `CityID`, `Country`, `Image`, `BannerImage`, `Address`, `OnlineStatus`, `Rating`, `Hide`, `BranchID`, `OpenTime`, `CloseTime`, `IsActive`, `SortOrder`, `CreatedAt`, `CreatedBy`, `UpdatedAt`, `UpdatedBy`) VALUES
(1, 1, 0, 0, NULL, '', '', '', 'admin', 'admin@glamorous.com', '0ed8b69c0b1f5a77f7f9e37936a4aad3', '0000-00-00', 0, '', '', '', '', '', 0, 0, '0', '', '', 1, 0, '0000-00-00', 0, '2018-07-04 10:05:01', 1),
(9, 1, 0, 0, NULL, 'Male', '', '', '', 'admin2@gmail.com', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', 0, '', '', '', '', '', 0, 0, '0', '', '', 1, 3, '2018-07-04', 1, '2018-07-04 09:43:13', 1),
(10, 1, 0, 0, NULL, 'Male', '', '', '', 'admin3@test.com', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', 0, '', '', '', '', '', 0, 0, '0', '', '', 1, 4, '2018-07-04', 1, '2018-07-04 09:49:26', 1),
(13, 3, 0, 4, NULL, 'Male', '123456789', '123456789', '', 'second_store2@gmail.com', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', 151, '', 'uploads/store/9862215551320191011054608images.png', 'uploads/store/612053197482019101105460820151022_181759_fin.jpg', 'This is a test address.', 'Online', 1, 0, '0', '9:00', '21:00', 1, 7, '2018-07-11', 1, '2019-10-11 17:11:47', 1),
(14, 3, 0, 4, NULL, 'Male', '03008094941', '3008094941', '', 'second_store3@gmail.com', '25d55ad283aa400af464c76d713c07ad', '0000-00-00', 151, '', 'uploads/store/3637914078320191011052213c6372403fcf1aaea6b56fd32f141abcd.jpg', 'uploads/store/8983017056420191011052213retreat-at-Telford-College.png', 'Seven Stars Salon for people in Saudi Arabia', 'Online', 5, 0, '0', '7:15', '20:15', 1, 8, '2018-07-11', 1, '2019-10-23 08:20:03', 1),
(16, 3, 12, 1, 'delivery', 'Male', '03008094941', '3008094941', '', 'moduleuser@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 0, '', '', '', 'This is testing by sarfraz, This is testing by sarfraz, This is testing by sarfraz', 'Online', 0, 0, '3', '', '', 1, 10, '2018-07-18', 1, '2018-09-10 08:02:54', 1),
(24, 4, 0, 0, NULL, '', '', '0987654321', 'test', 'test@test.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-08-08', 0, '2018-08-08 07:20:41', 0),
(25, 4, 0, 0, NULL, '', '', '09865432q', 'test1', 'test1@test.com', '25d55ad283aa400af464c76d713c07ad', NULL, 5, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-08-08', 0, '2018-08-08 07:22:30', 0),
(26, 4, 0, 0, NULL, '', '', '989379848', 'test2', 'test2@test.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-08-08', 0, '2018-08-08 07:31:31', 0),
(27, 4, 0, 0, NULL, '', '', '342343', 'Test3', 'test3@test.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-08-09', 0, '2018-08-09 05:52:21', 0),
(28, 4, 0, 0, NULL, 'Male', '', '', 'waqar', 'waqar', '202cb962ac59075b964b07152d234b70', NULL, 6, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-06', 0, '2018-09-06 06:33:02', 0),
(29, 4, 0, 0, NULL, 'Male', '', '123123123123', 'waqarr', 'waqar@zynq.net', 'd828725179d622a56f951e527a966ed7', NULL, 0, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-06', 0, '2018-09-06 06:42:53', 0),
(31, 4, 0, 0, NULL, 'Male', '', '0512345678', 'salmancs', 'salman@gmail.com', '32e05616c8ed659463f9af00b142dd6f', NULL, 3, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-10', 0, '2018-09-10 12:15:08', 0),
(32, 4, 0, 0, NULL, 'Male', '', '', 'waqaf', 'waqar.rocketalpha@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-13', 0, '2018-09-13 13:18:37', 0),
(33, 4, 0, 0, NULL, 'Male', '', '0512345678', 'salmancs43', 'muhammadsalman@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 3, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-14', 0, '2018-09-14 14:01:54', 0),
(34, 4, 0, 0, NULL, '', '', '0512345678', 'what', 'waqarcs@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 5, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-14', 0, '2018-09-14 14:04:45', 0),
(35, 4, 0, 0, NULL, '', '', '0512345678', 'knaskd', 'na@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 3, 'also clans', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-14', 0, '2018-09-14 14:05:52', 0),
(36, 4, 0, 0, NULL, '', '', '0512345678', 'cascasc', 'cs@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 3, 'kasnckans', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-14', 0, '2018-09-14 14:10:16', 0),
(37, 4, 0, 0, NULL, 'Male', '', '1234567890', 'basit', 'basit@basit.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'saudiarabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-18', 0, '2018-09-18 10:14:59', 0),
(39, 4, 0, 0, NULL, '', '', '12335467', 'hamza_faroooq', 'hamza@zync.net', 'd62129ba57e53adea5189ce35e676842', NULL, 0, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-28', 0, '2018-09-28 07:59:53', 0),
(40, 4, 0, 0, NULL, '', '', '33', '66', '22', 'b53b3a3d6ab90ce0268229151c9bde11', NULL, 0, '44', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2018-09-28', 0, '2018-09-28 12:46:28', 0),
(41, 4, 0, 0, NULL, '', '', '0581057444', '12345678', 'me@zynq.net', '25d55ad283aa400af464c76d713c07ad', NULL, 23, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-01-15', 0, '2019-01-15 15:15:22', 0),
(42, 4, 0, 0, NULL, 'Male', '', '', 'rizwan', 'rizwan@zynq.net', '202cb962ac59075b964b07152d234b70', NULL, 2, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-01-24', 0, '2019-01-24 08:55:32', 0),
(43, 4, 0, 0, NULL, 'Male', '', '+923312342966', 'riz', 'r@r.com', '202cb962ac59075b964b07152d234b70', NULL, 0, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-01', 0, '2019-02-01 11:08:59', 0),
(44, 4, 0, 0, NULL, 'Male', '', '009234534512', 'itzasifmirza', 'itzasifmirza@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 09:20:04', 0),
(45, 4, 0, 0, NULL, 'Male', '', '36363636', 'Asifmirza', 'asif.m@zynq.net', 'e10adc3949ba59abbe56e057f20f883e', NULL, 0, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 09:22:23', 0),
(46, 4, 0, 0, NULL, 'Male', '', '123456789', 'hamza', 'hamza@zynq.net', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 09:34:29', 0),
(47, 4, 0, 0, NULL, 'Male', '', '123456789', 'ham', 'hamza@zynq.ne', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 09:36:11', 0),
(48, 4, 0, 0, NULL, 'Male', '', '+923323911622', 'hanz', 'hanz@zynq.net', '25d55ad283aa400af464c76d713c07ad', NULL, 3, 'Pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 09:38:05', 0),
(49, 4, 0, 0, NULL, 'Male', '', '+923323911622', 'testing ', 'salman12@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:01:15', 0),
(50, 4, 0, 0, NULL, 'Male', '', '+8858858', 'asif', 'asif@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:17:50', 0),
(51, 4, 0, 0, NULL, 'Male', '', '321321', 'asifs', 'asif1@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:18:28', 0),
(52, 4, 0, 0, NULL, 'Male', '', '+923323011622', 'salmananwaar', 'salman123@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:23:34', 0),
(53, 4, 0, 0, NULL, '', '', '+923323911622', 'testing teams dad', 'testing@y.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:24:45', 0),
(54, 4, 0, 0, NULL, '', '', '+923323911622', 'test123', 'test1@test1.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:27:03', 0),
(55, 4, 0, 0, NULL, '', '', '+923323911622', 'testasdc', 'teas@sad.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'pakistan', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:27:49', 0),
(56, 4, 0, 0, NULL, 'Male', '', '', 'Asifmirza1', 'asif2@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-02-26', 0, '2019-02-26 10:59:08', 0),
(57, 4, 0, 0, NULL, 'Male', '', '+923323911622', 'salmancs123', 'salman@salman.salman', '25d55ad283aa400af464c76d713c07ad', NULL, 2, 'saudi arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-03-07', 0, '2019-03-07 12:59:42', 0),
(58, 3, 0, 5, NULL, '', '0555555555', '055555555', '', 'finesalon@schopfen.com', '25d55ad283aa400af464c76d713c07ad', NULL, 2, '', '', '', 'THQ Jeddah', 'Online', 0, 0, '0', '15:30', '15:30', 1, 1, '2019-10-12', 1, '2019-10-12 12:26:04', 1),
(59, 4, 0, 0, NULL, 'Male', '', '+96123123123', 'pinky', 'asd@add.asd', 'a3dcb4d229de6fde0db5686dee47145d', NULL, 74, 'Pakistan ', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-10-15', 0, '2019-10-15 12:46:00', 0),
(60, 4, 0, 0, NULL, 'Male', '', '', 'test1234', 'test123@gmail.com', '02c75fb22c75b23dc963c7eb91a062cc', NULL, 74, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-10-22', 0, '2019-10-22 10:56:08', 0),
(61, 4, 0, 0, NULL, 'Male', '', '', 'rizwann', 'rizwan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 150, 'Saudi Arabia', '', '', '', NULL, 0, 0, '0', '', '', 1, 0, '2019-10-24', 0, '2019-10-24 07:07:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_text`
--

CREATE TABLE `users_text` (
  `UserTextID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `AboutMe` varchar(5000) NOT NULL,
  `SystemLanguageID` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `UpdatedAt` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `UpdatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_text`
--

INSERT INTO `users_text` (`UserTextID`, `UserID`, `Title`, `AboutMe`, `SystemLanguageID`, `CreatedAt`, `UpdatedAt`, `CreatedBy`, `UpdatedBy`) VALUES
(6, 1, 'Admin', '', 1, '2018-05-03 00:00:00', '2018-07-04 10:05:01', 1, 1),
(13, 9, 'admin2', '', 1, '2018-07-04 09:43:13', '2018-07-04 09:43:13', 1, 1),
(14, 10, 'Admin3', '', 1, '2018-07-04 09:49:26', '2018-07-04 09:49:26', 1, 1),
(17, 13, 'Eves Avenue', 'High class 5 star salon for Jeddawis their hometown in Jeddah Saudi Arabia.', 1, '2018-07-11 07:52:43', '2019-10-11 17:11:47', 1, 1),
(18, 14, 'Seven Stars Salon', '', 1, '2018-07-11 07:54:03', '2019-10-23 08:20:03', 1, 1),
(20, 16, 'Module', '', 1, '2018-07-18 09:00:21', '2018-09-10 08:02:54', 1, 1),
(28, 24, 'Test', '', 1, '2018-08-08 07:20:41', '2018-08-08 07:20:41', 0, 0),
(29, 25, 'Test1', '', 1, '2018-08-08 07:22:30', '2018-08-08 07:22:30', 0, 0),
(30, 26, 'test2', '', 1, '2018-08-08 07:31:31', '2018-08-08 07:31:31', 0, 0),
(31, 27, 'Test3', '', 1, '2018-08-09 05:52:21', '2018-08-09 05:52:21', 0, 0),
(32, 28, 'waqar', '', 1, '2018-09-06 06:33:02', '2018-09-06 06:33:02', 0, 0),
(33, 29, 'Waqar Ahmedd', '', 1, '2018-09-06 06:42:53', '2018-09-06 06:42:53', 0, 0),
(35, 31, 'Muhammad Salman', '', 1, '2018-09-10 12:15:08', '2018-09-10 12:15:08', 0, 0),
(37, 13, 'test', 'test', 2, '2018-09-13 11:52:51', '2018-09-13 11:52:51', 1, 1),
(38, 32, 'waqar ', '', 1, '2018-09-13 13:18:37', '2018-09-13 13:18:37', 0, 0),
(39, 33, 'Muhammad Salman', '', 1, '2018-09-14 14:01:54', '2018-09-14 14:01:54', 0, 0),
(40, 34, 'Muhammad Salman', '', 1, '2018-09-14 14:04:45', '2018-09-14 14:04:45', 0, 0),
(41, 35, 'kasnmckl', '', 1, '2018-09-14 14:05:52', '2018-09-14 14:05:52', 0, 0),
(42, 36, 'akscnka', '', 1, '2018-09-14 14:10:16', '2018-09-14 14:10:16', 0, 0),
(43, 37, 'basit', '', 1, '2018-09-18 10:14:59', '2018-09-18 10:14:59', 0, 0),
(44, 39, 'Hamza Farooq', '', 1, '2018-09-28 07:59:53', '2018-09-28 07:59:53', 0, 0),
(45, 40, '11', '', 1, '2018-09-28 12:46:28', '2018-09-28 12:46:28', 0, 0),
(46, 41, 'Basit Chughtai', '', 1, '2019-01-15 15:15:22', '2019-01-15 15:15:22', 0, 0),
(47, 42, 'Rizwan', '', 1, '2019-01-24 08:55:32', '2019-01-24 08:55:32', 0, 0),
(48, 43, 'Rizwan ', '', 1, '2019-02-01 11:08:59', '2019-02-01 11:08:59', 0, 0),
(49, 44, 'asif', '', 1, '2019-02-26 09:20:04', '2019-02-26 09:20:04', 0, 0),
(50, 45, 'Asif', '', 1, '2019-02-26 09:22:23', '2019-02-26 09:22:23', 0, 0),
(51, 46, 'Hamza', '', 1, '2019-02-26 09:34:29', '2019-02-26 09:34:29', 0, 0),
(52, 47, 'Hamza', '', 1, '2019-02-26 09:36:11', '2019-02-26 09:36:11', 0, 0),
(53, 48, 'Hamza Farooq', '', 1, '2019-02-26 09:38:05', '2019-02-26 09:38:05', 0, 0),
(54, 49, 'Muhammad salman', '', 1, '2019-02-26 10:01:15', '2019-02-26 10:01:15', 0, 0),
(55, 50, 'asif mirza', '', 1, '2019-02-26 10:17:50', '2019-02-26 10:17:50', 0, 0),
(56, 51, 'asif', '', 1, '2019-02-26 10:18:28', '2019-02-26 10:18:28', 0, 0),
(57, 52, 'muhamamdsalman', '', 1, '2019-02-26 10:23:34', '2019-02-26 10:23:34', 0, 0),
(58, 53, 'testing testing', '', 1, '2019-02-26 10:24:45', '2019-02-26 10:24:45', 0, 0),
(59, 54, 'testing test', '', 1, '2019-02-26 10:27:03', '2019-02-26 10:27:03', 0, 0),
(60, 55, 'testsad', '', 1, '2019-02-26 10:27:49', '2019-02-26 10:27:49', 0, 0),
(61, 56, 'Asif', '', 1, '2019-02-26 10:59:08', '2019-02-26 10:59:08', 0, 0),
(62, 57, 'Muhammad salman', '', 1, '2019-03-07 12:59:42', '2019-03-07 12:59:42', 0, 0),
(63, 58, 'Fine Salon', 'Lorem Ipsum dolor sit amet.', 1, '2019-10-12 12:26:04', '2019-10-12 12:26:04', 1, 1),
(64, 59, 'funky', '', 1, '2019-10-15 12:46:00', '2019-10-15 12:46:00', 0, 0),
(65, 60, 'test', '', 1, '2019-10-22 10:56:08', '2019-10-22 10:56:08', 0, 0),
(66, 61, 'rizwan', '', 1, '2019-10-24 07:07:53', '2019-10-24 07:07:53', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`AddressID`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`AdID`);

--
-- Indexes for table `ads_text`
--
ALTER TABLE `ads_text`
  ADD PRIMARY KEY (`AdTextID`),
  ADD KEY `ads_text_AdID_fk` (`AdID`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`BookingID`);

--
-- Indexes for table `booking_services`
--
ALTER TABLE `booking_services`
  ADD PRIMARY KEY (`BookingServiceID`);

--
-- Indexes for table `branchs`
--
ALTER TABLE `branchs`
  ADD PRIMARY KEY (`BranchID`);

--
-- Indexes for table `branchs_text`
--
ALTER TABLE `branchs_text`
  ADD PRIMARY KEY (`BranchTextID`),
  ADD KEY `branchs_text_BranchID_fk` (`BranchID`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryID`);

--
-- Indexes for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD PRIMARY KEY (`CategoryTextID`),
  ADD KEY `categories_text_CategoryID_fk` (`CategoryID`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`CityID`),
  ADD KEY `DistrictID` (`DistrictID`);

--
-- Indexes for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD PRIMARY KEY (`CityTextID`),
  ADD KEY `FK_cities_text` (`CityID`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`DistrictID`);

--
-- Indexes for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD PRIMARY KEY (`DistrictTextID`),
  ADD KEY `DistrictID` (`DistrictID`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`Email_templateID`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`ImageID`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`ModuleID`);

--
-- Indexes for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD PRIMARY KEY (`ModuleTextID`),
  ADD KEY `ModuleID` (`ModuleID`);

--
-- Indexes for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  ADD PRIMARY KEY (`ModuleRightID`),
  ADD KEY `fk_ModuleID` (`ModuleID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`OrderItemID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `products_text`
--
ALTER TABLE `products_text`
  ADD PRIMARY KEY (`ProductTextID`),
  ADD KEY `products_text_ProductID_fk` (`ProductID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indexes for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD PRIMARY KEY (`RoleTextID`),
  ADD KEY `RoleID` (`RoleID`);

--
-- Indexes for table `screening_camps`
--
ALTER TABLE `screening_camps`
  ADD PRIMARY KEY (`ScreeningCampID`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`ServiceID`);

--
-- Indexes for table `services_text`
--
ALTER TABLE `services_text`
  ADD PRIMARY KEY (`ServiceTextID`),
  ADD KEY `services_text_ServiceID_fk` (`ServiceID`);

--
-- Indexes for table `shopping_cards`
--
ALTER TABLE `shopping_cards`
  ADD PRIMARY KEY (`ShoppingCardID`);

--
-- Indexes for table `shopping_cards_text`
--
ALTER TABLE `shopping_cards_text`
  ADD PRIMARY KEY (`ShoppingCardTextID`),
  ADD KEY `shoppingCards_text_ShoppingCardID_fk` (`ShoppingCardID`);

--
-- Indexes for table `shopping_card_numbers`
--
ALTER TABLE `shopping_card_numbers`
  ADD PRIMARY KEY (`ShoppingCardNumberID`);

--
-- Indexes for table `shopping_card_purchase`
--
ALTER TABLE `shopping_card_purchase`
  ADD PRIMARY KEY (`ShoppingCardPurchaseID`);

--
-- Indexes for table `shopping_card_received`
--
ALTER TABLE `shopping_card_received`
  ADD PRIMARY KEY (`ShoppingCardReceivedID`);

--
-- Indexes for table `shopping_card_transactions`
--
ALTER TABLE `shopping_card_transactions`
  ADD PRIMARY KEY (`ShoppingCardTransactionID`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`SiteSettingID`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`StoreID`);

--
-- Indexes for table `store_services`
--
ALTER TABLE `store_services`
  ADD PRIMARY KEY (`StoreServiceID`);

--
-- Indexes for table `system_languages`
--
ALTER TABLE `system_languages`
  ADD PRIMARY KEY (`SystemLanguageID`);

--
-- Indexes for table `temp_bookings`
--
ALTER TABLE `temp_bookings`
  ADD PRIMARY KEY (`TempBookingID`);

--
-- Indexes for table `temp_orders`
--
ALTER TABLE `temp_orders`
  ADD PRIMARY KEY (`TempOrderID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `users_ibfk_1` (`RoleID`);

--
-- Indexes for table `users_text`
--
ALTER TABLE `users_text`
  ADD PRIMARY KEY (`UserTextID`),
  ADD KEY `RoleID` (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `AddressID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `AdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ads_text`
--
ALTER TABLE `ads_text`
  MODIFY `AdTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `BookingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking_services`
--
ALTER TABLE `booking_services`
  MODIFY `BookingServiceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branchs`
--
ALTER TABLE `branchs`
  MODIFY `BranchID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branchs_text`
--
ALTER TABLE `branchs_text`
  MODIFY `BranchTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories_text`
--
ALTER TABLE `categories_text`
  MODIFY `CategoryTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `cities_text`
--
ALTER TABLE `cities_text`
  MODIFY `CityTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `DistrictID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `districts_text`
--
ALTER TABLE `districts_text`
  MODIFY `DistrictTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `Email_templateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `ImageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `ModuleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `modules_rights`
--
ALTER TABLE `modules_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `modules_text`
--
ALTER TABLE `modules_text`
  MODIFY `ModuleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `modules_users_rights`
--
ALTER TABLE `modules_users_rights`
  MODIFY `ModuleRightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `OrderItemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products_text`
--
ALTER TABLE `products_text`
  MODIFY `ProductTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `RoleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles_text`
--
ALTER TABLE `roles_text`
  MODIFY `RoleTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `screening_camps`
--
ALTER TABLE `screening_camps`
  MODIFY `ScreeningCampID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `ServiceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `services_text`
--
ALTER TABLE `services_text`
  MODIFY `ServiceTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `shopping_cards`
--
ALTER TABLE `shopping_cards`
  MODIFY `ShoppingCardID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shopping_cards_text`
--
ALTER TABLE `shopping_cards_text`
  MODIFY `ShoppingCardTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `shopping_card_numbers`
--
ALTER TABLE `shopping_card_numbers`
  MODIFY `ShoppingCardNumberID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shopping_card_purchase`
--
ALTER TABLE `shopping_card_purchase`
  MODIFY `ShoppingCardPurchaseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shopping_card_received`
--
ALTER TABLE `shopping_card_received`
  MODIFY `ShoppingCardReceivedID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shopping_card_transactions`
--
ALTER TABLE `shopping_card_transactions`
  MODIFY `ShoppingCardTransactionID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `SiteSettingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `StoreID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_services`
--
ALTER TABLE `store_services`
  MODIFY `StoreServiceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `system_languages`
--
ALTER TABLE `system_languages`
  MODIFY `SystemLanguageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `temp_bookings`
--
ALTER TABLE `temp_bookings`
  MODIFY `TempBookingID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_orders`
--
ALTER TABLE `temp_orders`
  MODIFY `TempOrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users_text`
--
ALTER TABLE `users_text`
  MODIFY `UserTextID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `branchs_text`
--
ALTER TABLE `branchs_text`
  ADD CONSTRAINT `branchs_text_BranchID_fk` FOREIGN KEY (`BranchID`) REFERENCES `branchs` (`BranchID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories_text`
--
ALTER TABLE `categories_text`
  ADD CONSTRAINT `categories_text_CategoryID_fk` FOREIGN KEY (`CategoryID`) REFERENCES `categories` (`CategoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`DistrictID`) REFERENCES `districts` (`DistrictID`);

--
-- Constraints for table `cities_text`
--
ALTER TABLE `cities_text`
  ADD CONSTRAINT `FK_cities_text` FOREIGN KEY (`CityID`) REFERENCES `cities` (`CityID`);

--
-- Constraints for table `districts_text`
--
ALTER TABLE `districts_text`
  ADD CONSTRAINT `districts_text_ibfk_1` FOREIGN KEY (`DistrictID`) REFERENCES `districts` (`DistrictID`);

--
-- Constraints for table `modules_rights`
--
ALTER TABLE `modules_rights`
  ADD CONSTRAINT `fk_ModuleID` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `modules_text`
--
ALTER TABLE `modules_text`
  ADD CONSTRAINT `modules_text_ibfk_1` FOREIGN KEY (`ModuleID`) REFERENCES `modules` (`ModuleID`);

--
-- Constraints for table `products_text`
--
ALTER TABLE `products_text`
  ADD CONSTRAINT `products_text_ProductID_fk` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles_text`
--
ALTER TABLE `roles_text`
  ADD CONSTRAINT `roles_text_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`);

--
-- Constraints for table `services_text`
--
ALTER TABLE `services_text`
  ADD CONSTRAINT `services_text_ServiceID_fk` FOREIGN KEY (`ServiceID`) REFERENCES `services` (`ServiceID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `roles` (`RoleID`) ON UPDATE CASCADE;

--
-- Constraints for table `users_text`
--
ALTER TABLE `users_text`
  ADD CONSTRAINT `users_text_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
