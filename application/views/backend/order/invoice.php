<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Order Detail </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h3> <?php echo $order_items[0]['FullName'];?></h3>
                                            </div>
                                            <div class="pull-right">
                                                <h4>Track ID # <br>
                                                    <strong><?php echo $order_items[0]['OrderTrackID'];?></strong>
                                                </h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong><?php echo $order_items[0]['CityTitle'];?></strong><br>
                                                      <b>Zip</b> : <?php echo $order_items[0]['Zip'];?>, <b>Po Box</b> : <?php echo $order_items[0]['PoBox'];?><br>
                                                      <b>Block no:</b> <?php echo $order_items[0]['BlockNo'];?>, <b>Flat no:</b><?php echo $order_items[0]['FlatNo'];?><br>
                                                     <b>Address:</b><?php echo $order_items[0]['Location'];?><br>
                                                      <abbr title="Phone"><b>Phone:</b></abbr> <?php echo $order_items[0]['Phone'];?>
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Order Date: </strong> <?php echo date('d-m-Y',strtotime($order_items[0]['OrderDate']));?></p>
                                                    <p><strong>Order Status: </strong> <span class="label label-danger order_status"><?php echo $order_items[0]['Status'];?></span></p>
                                                    <p><strong>Order ID: </strong> #<?php echo $order_items[0]['OrderID'];?></p>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->

                                        <div class="m-h-50"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table m-t-30">
                                                        <thead>
                                                            <tr><th>#</th>
                                                            <th>Item</th>
                                                            
                                                            <th>Quantity</th>
                                                            <th>Unit Cost</th>
                                                            <th>Total</th>
                                                        </tr></thead>
                                                        <tbody>
                                            <?php 
                                                            $total = 0;
                                                            
                                                            foreach($order_items as $key => $value){
                                                                $total = $total + ($value['PlaceOrderPrice']  * $value['Quantity']);?>
                                                            <tr>
                                                                <td><?php echo $key +1 ; ?></td>
                                                                <td><a href="<?php echo base_url('cms/product/edit/'.$value['ProductID']);?>" target="_blank"><?php echo $value['ProductTitle'];?></a></td>
                                                                
                                                                <td><?php echo $value['Quantity']; ?></td>
                                                                <td>SAR <?php echo $value['PlaceOrderPrice'];?></td>
                                                                <td>SAR <?php echo ($value['PlaceOrderPrice']  * $value['Quantity']);?></td>
                                                            </tr>
                                            <?php } ?>                    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="clearfix m-t-40">
                                                    <h5 class="small text-inverse font-600">PAYMENT TERMS AND POLICIES</h5>

                                                    <small>
                                                        All accounts are to be paid within 7 days from receipt of
                                                        invoice. To be paid by cheque or credit card or direct payment
                                                        online. If account is not paid within 7 days the credits details
                                                        supplied as confirmation of work undertaken will be charged the
                                                        agreed quoted fee noted above.
                                                    </small>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                                <p class="text-right"><b>Sub Total:</b> <?php echo $total; ?> SAR</p>
                                                <!--<p class="text-right">Discout: 12.9%</p>
                                                <p class="text-right">VAT: 12.9%</p>
                                                <hr>
                                                <h3 class="text-right">USD 2930.00</h3> -->
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="hidden-print">
                                            <div class="pull-right">
                                                
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                <!--<a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->