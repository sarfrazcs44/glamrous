<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delivered</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&nbsp;</span>
        </button>
    </div>
    <div class="modal-body">
        <input type="hidden" id="OrderID" value="<?php echo $OrderID; ?>">
        <input type="hidden" id="form_type" value="otp">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group label-floating">
                    <label class="control-label" for="OTP">Delivery OTP*</label>
                    <input type="text" value="" name="OTP" id="OTP">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary odom-submit" id="update_otp">Delivered</button>
    </div>

</div>

<script>
    $("#update_otp").click(function () {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        var form_type = $('#form_type').val();
        var OrderID = $('#OrderID').val();
        var OTP = $('#OTP').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>cms/order/update_status_with_otp",
            data: {"form_type": form_type, "OrderID": OrderID, "OTP": OTP},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {

                    $('#validation-msg').addClass('alert-danger show');
                    $("#validation-msg").html(result.error);
                    //$('#validatio-msg').show();

                } else {
                    $('#validation-msg').addClass('alert-success show');
                    $("#validation-msg").html(result.success);
                   
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });
</script>