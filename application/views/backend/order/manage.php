<link href="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                                                    <h4 class="page-title"><?php echo $OrderStatus; ?> <?php echo lang($ControllerName.'s'); ?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                         
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                          
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th><?php echo lang('track_id');?></th>
                                            
                                            <th><?php echo lang('username');?></th>
                                            <th><?php echo lang('store_title');?></th>
                                            <th><?php echo lang('status');?></th>
                                            <th><?php echo lang('order_date');?></th>
                                            <th>Driver Name</th>
                                            
                                             <?php if(checkUserRightAccess(44,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(44,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                            <?php } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value['OrderID'];?>">
                                                    
                                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                                     <td><?php echo $value['OrderUserTitle']; ?></td>
                                                    <td><?php echo $value['StoreTitle']; ?></td>
                                                   
                                                    <td id="status_<?php echo $value['OrderID'];?>"><?php echo $value['Status']; ?></td>
                                                    <td><?php echo date('d-m-Y H:i',strtotime($value['OrderCreatedAt'])); ?></td>
                                                    <td><?php echo ($value['DriverName'] == '' ? 'N/A' : $value['DriverName']); ?></td>
                                                    
                                                     <?php if(checkUserRightAccess(44,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(44,$this->session->userdata['admin']['UserID'],'CanDelete')){?>     
                                                    <td>
                                                         
                                                        <?php if(checkUserRightAccess(44,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.$value['OrderID']);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                            <?php
                                                        if ($this->session->userdata['admin']['RoleID'] == 1 || $this->session->userdata['admin']['RoleID'] == 3) { ?>
                                                            <a href="javascript:void(0);" data-toggle="modal"
                                                               data-target="#exampleModal"
                                                               data-order-id="<?php echo $value['OrderID']; ?>"
                                                               class="btn btn-simple btn-warning btn-icon edit assign_order"><i
                                                                        class="material-icons"
                                                                        title="Assign To Driver">Assign To Driver</i>
                                                            </a>
                                                        <?php }
                                                        ?>
                                                        <?php if($value['Status'] != 'Delivered'){ ?>
                                                        <select onchange="changeStatus(this.value,'<?php echo $value['OrderID'];?>')">
                                                        <option >Change Stauts</option>
                                                        <?php if($value['Status'] == 'Received'){?>    
                                                        <option value="Dispatched">Dispatched</option>
                                                            <?php }else if($value['Status'] == 'Dispatched'){ ?>
                                                        <option value="Delivered">Delivered</option>
                                                        </select>
                                                        <?php } ?>
                                                        <?php } } ?>

                                                        
                                                        </td> 
                                                        <?php } ?>
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" id="response_data">

    </div>
</div>
                <script src="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
               
                $('#datatable-responsive').DataTable();
                $('.assign_order').on('click', function () {

            var OrderID = $(this).attr('data-order-id');


            $('#response_data').html('');

            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });


            $.ajax({
                type: "POST",
                url: base_url + 'cms/order/get_drivers',
                data: {
                    'OrderID': OrderID
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');

                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });
               
    
            });
    
    function changeStatus(value, order_id) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to change status?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        if(value != 'Delivered'){
            $.ajax({
            type: "POST",
            url: base_url + 'cms/order/change_status',
            data: {'order_id': order_id, 'status': value},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != false) {
                    alert('There is something went wrong');

                } else {
                    $('#status_' + order_id).html(value);

                }


            }, complete: function () {
                $.unblockUI();
            }
        });
        }else{
            $.ajax({
            type: "POST",
            url: base_url + 'cms/order/get_otp',
            data: {'order_id': order_id, 'status': value},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


               
                
                    $('#response_data').html(result.html);
                    $('#exampleModal').modal('show');

            }, complete: function () {
                $.unblockUI();
            }
        });
        }
        
        
    } else {
        return false;
    }

}
          

</script> 
