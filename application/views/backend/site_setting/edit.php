<div class="content-page" style="margin-top:10px;">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"><?php echo lang('EditSiteSettings'); ?></h4>

                        <form action="<?php echo base_url(); ?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SiteSettingID" value="<?php echo $SiteSettingID; ?>">
                           

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="SiteName"><?php echo lang('SiteName'); ?> <span class="text-danger">*</span> :</label>
                                        <input type="text" class="form-control" name="SiteName" id="SiteName" required value="<?php echo $result->SiteName; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="PhoneNumber"><?php echo lang('PhoneNumber'); ?> :</label>
                                        <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $result->PhoneNumber; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="Email"><?php echo lang('email'); ?> :</label>
                                        <input type="email" class="form-control" name="Email" id="Email" value="<?php echo $result->Email; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="Whatsapp"><?php echo lang('Whatsapp'); ?> :</label>
                                        <input type="text" class="form-control" name="Whatsapp" id="Whatsapp" value="<?php echo $result->Whatsapp; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="Skype"><?php echo lang('Skype'); ?> :</label>
                                        <input type="text" class="form-control" name="Skype" id="Skype" value="<?php echo $result->Skype; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="Fax"><?php echo lang('Fax'); ?> :</label>
                                        <input type="text" class="form-control" name="Fax" id="Fax" value="<?php echo $result->Fax; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="Image"><?php echo lang('SiteLogo'); ?> :</label>
                                        <input type="file" class="filestyle" id="Image" name="Image[]" data-placeholder="No Image">
                                    </div>
                                    <?php if ($result->SiteImage != '') { ?>
                                        <img src="<?php echo base_url($result->SiteImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            <hr/>
                            
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="FacebookUrl"><?php echo lang('FacebookUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="FacebookUrl" id="FacebookUrl" value="<?php echo $result->FacebookUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="GoogleUrl"><?php echo lang('GoogleUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="GoogleUrl" id="GoogleUrl" value="<?php echo $result->GoogleUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="LinkedInUrl"><?php echo lang('LinkedInUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="LinkedInUrl" id="LinkedInUrl" value="<?php echo $result->LinkedInUrl; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="TwitterUrl"><?php echo lang('TwitterUrl'); ?> :</label>
                                        <input type="text" class="form-control" name="TwitterUrl" id="TwitterUrl" value="<?php echo $result->TwitterUrl; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>



                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- end col -->


            </div>
            <!-- end row -->



        </div> <!-- container -->

    </div> <!-- content -->

</div>