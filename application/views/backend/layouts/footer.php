<!-- Footer -->
<footer class="footer text-center">
   <?php echo $site_setting->SiteName;?> Admin Panel 2018
</footer>
<!-- Footer -->

<!-- Right Sidebar -->
<div class="side-bar right-bar">
    <a href="javascript:void(0);" class="right-bar-toggle">
        <i class="mdi mdi-close-circle-outline"></i>
    </a>
    <h4 class="">Settings</h4>
    <div class="setting-list nicescroll">
        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Notifications</h5>
                <p class="text-muted m-b-0"><small>Do you need them?</small></p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">API Access</h5>
                <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Auto Updates</h5>
                <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Online Status</h5>
                <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>
    </div>
</div>
<!-- /Right-bar -->

</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/backend/js/script.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/detect.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/fastclick.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/switchery/switchery.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

<!-- google maps api 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZTXzGXKj5gqIHyxvH3P22UV874J3C80g"></script>
-->
<!-- main file 
<script src="<?php echo base_url();?>assets/backend/plugins/gmaps/gmaps.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/pages/jquery.gmaps.js"></script>
-->
<!-- Counter js  -->
<script src="<?php echo base_url();?>assets/backend/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/counterup/jquery.counterup.min.js"></script>

<!--Morris Chart
<script src="<?php echo base_url();?>assets/backend/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/raphael/raphael-min.js"></script>
-->

<!-- Flot chart js -->
<!--
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.selection.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.crosshair.js"></script>
-->

<script src="<?php echo base_url();?>assets/backend/plugins/moment/moment.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Dashboard init 
<script src="<?php echo base_url();?>assets/backend/pages/jquery.dashboard.js"></script>
-->
<!-- Dashboard init
<script src="<?php echo base_url();?>assets/backend/pages/jquery.dashboard_2.js"></script>
 -->
<!-- App js -->
<script src="<?php echo base_url();?>assets/backend/js/jquery.core.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.app.js"></script>
<!--<script src="<?php /*echo base_url();*/?>assets/backend/pages/jquery.form-pickers.init.js"></script>-->
<script>
    $(document).ready(function () {
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        jQuery('.time_picker').timepicker({
            showMeridian: false
        });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        });
    });
</script>
</body>
</html>