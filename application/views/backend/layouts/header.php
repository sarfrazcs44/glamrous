<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/backend/images/favicon.png" type="image/x-icon">
    
    <!-- App title -->
    <title><?php echo $site_setting->SiteName; ?></title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/backend/plugins/morris/morris.css">

    <!-- App css -->
    <link href="<?php echo base_url();?>assets/backend/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/datepicker/css/datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/pages.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/menu.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/cbd-style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/backend/plugins/switchery/switchery.min.css" rel="stylesheet">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>assets/backend/js/modernizr.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/timepicker/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>assets/backend/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
</head>

<script>
var base_url = '<?php echo base_url(); ?>';
var delete_msg = '<?php echo lang('are_you_sure');?>';
</script>
<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="front-page.php" class="logo" title="<?php echo $site_setting->SiteName; ?>"><span><?php echo $site_setting->SiteName; ?></span></a>
            
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Navbar-left -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left waves-effect">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                    
                </ul>

                <!-- Right(Notification) -->
                <ul class="nav navbar-nav navbar-right">
                    

                    <li class="dropdown user-box">
                            <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                    <img src="<?php echo base_url();?>assets/backend/images/users/avatar-1.jpg" alt="user-img" class="img-circle user-img">
                            </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li>
                                <h5>Hi, John</h5>
                            </li>
                            <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                            <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                            
                            <li><a href="<?php echo base_url('cms/account/logout');?> "><i class="ti-power-off m-r-5"></i> Logout</a></li>
                        </ul>
                    </li>

                </ul> <!-- end navbar-right -->

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->

    <!-- Top Bar End -->
    <div class="alert <?php if($this->session->flashdata('message')){ echo 'alert-danger show'; } ?>" id="validation-msg">
                        <?php if($this->session->flashdata('message')){ 

                            echo $this->session->flashdata('message');


                        } 
                        ?>
     <?php if($this->session->flashdata('message')){ ?>
        <script>
        
        $( document ).ready(function() {
                 setTimeout(function() {
                            $('#validation-msg').removeClass('alert-danger show alert-success');
                        }, 4000);
        });
        
        </script>
        <?php } ?>   
    </div>
    