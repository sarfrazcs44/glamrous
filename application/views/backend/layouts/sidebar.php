<!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul>
                    <li class="menu-title">Navigation</li>
                    <li>
                        <a href="<?php echo base_url('cms/dashboard');?>" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span>Dashboard</span></a>
                    </li>
                    <?php 

                    //$modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1');
                    $modules = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1');
                    
                    if(!empty($modules)){
                        foreach($modules as $value){ 
                           // $getParentUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1 AND modules.ModuleID = '.$value->ModuleID);

                            if($value->CanView == 1 || $value->CanAdd == 1 || $value->CanEdit == 1 || $value->CanDelete == 1){   

                            //$child_modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1');

                            $child_modules = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1');
                            if(!empty($child_modules)){

                                $getParent = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,"modules.Slug = '".$this->uri->segment(2)."' AND modules.IsActive = 1");
                        ?>   
                            
                                <li class="has_sub">
                                        <a href="javascript:void(0);" class="waves-effect <?php echo ($getParent && $value->ModuleID == $getParent[0]->ParentID ? 'active' : '');?>"><i class="<?php echo $value->IconClass; ?>"></i><span><?php echo $value->ModuleTitle; ?></span></a>
                                
                                        <?php 
                                          
                                         
                                          if(!empty($child_modules)){ ?>
                                             <ul class="list-unstyled">
                                               <?php
                                            foreach($child_modules as $child_value){ 
                                                //$getChildUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1 AND modules.ModuleID = '.$child_value->ModuleID);
                                                if(($child_value->CanView == 1 || $child_value->CanAdd == 1 || $child_value->CanEdit == 1 || $child_value->CanDelete == 1)){    ?>
                                    
                                       
                                             
                                             <li <?php echo ($this->uri->segment(2) == $child_value->Slug ? 'class="active"' : '');?>>
                                                <a href="<?php echo base_url('cms/'.$child_value->Slug); ?>" class="waves-effect"><i class="<?php echo $child_value->IconClass; ?>"></i><span><?php echo $child_value->ModuleTitle; ?> </span></a>
                                             </li>
                                                <?php } } ?>
                                           
                                        </ul>
                                            <?php } ?>
                                </li>  
                            <?php }else{ ?>
                                <li>
                                    <a href="<?php echo base_url('cms/'.$value->Slug); ?>" class="waves-effect"><i class="<?php echo $value->IconClass; ?>"></i><span><?php echo $value->ModuleTitle; ?></span></a>                      
                                </li>  
                           <?php }  ?>  
                            
                    <?php        
                        }
                        }
                    }
                    ?>
                    <li>
                        <a href="<?php echo base_url('cms/account/logout');?>" class="waves-effect"><i class="mdi mdi-logout"></i><span><?php echo lang('logout');?></span></a>
                    </li>
                </ul>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

            <div class="help-box">
                <h5 class="text-muted m-t-0">For Help ?</h5>
                <p class=""><span class="text-custom">Email:</span> <br/> <?php echo $site_setting->Email; ?></p>
                <p class="m-b-0"><span class="text-custom">Call:</span> <br/> <?php echo $site_setting->PhoneNumber; ?></p>
            </div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->