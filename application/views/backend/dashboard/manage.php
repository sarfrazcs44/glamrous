<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                                                   
                                    <ol class="breadcrumb p-0 m-0">
                                         <?php if($this->session->userdata['admin']['RoleID'] == 3){ ?>
                                        <li>
                                            <a href="<?php echo base_url('cms/product/index/'.base64_encode($this->session->userdata['admin']['UserID']).'/shopping');?>">
                                               <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Start Shopping</button>
                                            </a>
                                        </li>
                                       <?php } ?>
                                       <?php if($this->session->userdata['admin']['RoleID'] == 3){ ?>
                                        <li>
                                            <a href="#">
                                               <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Book Service</button>
                                            </a>
                                        </li>
                                       <?php } ?>
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <div class="row m-t-30 m-b-10">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-primary">
                                <i class="mdi mdi-chart-areaspline widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Users</p>
                                    <h2>34578 <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 30.4k</p>
                                </div>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-warning">
                                <i class="mdi mdi-account-convert widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Active Users</p>
                                    <h2>895 <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 1250</p>
                                </div>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-danger">
                                <i class="mdi mdi-account-convert widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Categories</p>
                                    <h2>895 <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 1250</p>
                                </div>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-success">
                                <i class="mdi mdi-account-convert widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Products</p>
                                    <h2>895 <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 1250</p>
                                </div>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-primary">
                                <i class="mdi mdi-account-convert widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Summary Reports</p>
                                    <h2>895 <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 1250</p>
                                </div>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="card-box widget-box-one widget-two-warning">
                                <i class="mdi mdi-account-convert widget-one-icon"></i>
                                <div class="wigdet-one-content">
                                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Income</p>
                                    <h2>895 <small><i class="mdi mdi-arrow-down text-danger"></i></small></h2>
                                    <p class="text-muted m-0"><b>Last:</b> 1250</p>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                    
                    <!-- end row -->


                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0 m-b-30">Number of users per year</h4>

                                <div id="website-stats" style="height: 320px;" class="flot-chart"></div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Users</h4>

                                <div class="pull-right m-b-30">
                                    <div id="reportrange" class="form-control">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div id="donut-chart">
                                    <div id="donut-chart-container" class="flot-chart" style="height: 240px;">
                                    </div>
                                </div>

                                <p class="text-muted m-b-0 m-t-15 font-13 text-overflow">Pie chart is used to see the proprotion of each data groups, making Flot pie chart is pretty simple, in order to make pie chart you have to incldue jquery.flot.pie.js plugin.</p>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->
                </div>
                <div class="col-md-3 col-xs-12 m-b-20">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Daily Reports</h3>
                            <ul class="list-unstyled m-b-20">
                                <li>FSO</li>
                                <li>TMD</li>
                                <li><a href="#">TG</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Statistics</h4>

                                <div class="widget-chart text-center">
                                    <div id="morris-donut-example"style="height: 245px;"></div>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Statistics</h4>
                                <div id="morris-bar-example" style="height: 280px;"></div>
                            </div>
                        </div><!-- end col -->
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="card-box">
                                <h4 class="header-title m-t-0">Total Revenue</h4>
                                <div id="morris-line-example" style="height: 280px;"></div>
                            </div>
                        </div><!-- end col -->
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div> <!-- container -->

    </div> <!-- content -->
    
  <!-- google maps api -->
    <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZTXzGXKj5gqIHyxvH3P22UV874J3C80g"></script>-->

    <!-- main file -->
    <!--<script src="<?php echo base_url();?>assets/backend/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/pages/jquery.gmaps.js" type="text/javascript"></script>-->

    <!-- Counter js  -->
    <script src="<?php echo base_url();?>assets/backend/plugins/waypoints/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

    <!--Morris Chart-->
    <script src="<?php echo base_url();?>assets/backend/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/raphael/raphael-min.js" type="text/javascript"></script>


    <!-- Flot chart js -->
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.time.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.tooltip.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.selection.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/flot-chart/jquery.flot.crosshair.js" type="text/javascript"></script>

    <!-- Dashboard init -->
    <script src="<?php echo base_url();?>assets/backend/pages/jquery.dashboard.js" type="text/javascript"></script>
    <!-- Dashboard init -->
    <script src="<?php echo base_url();?>assets/backend/pages/jquery.dashboard_2.js" type="text/javascript"></script>   