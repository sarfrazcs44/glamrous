<link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />
<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $spec = '';
        if(isset($result[$key]->Specifications) && $result[$key]->Specifications != ''){
        $specifications  = explode(',',$result[$key]->Specifications);
         if($specifications){
                                                foreach($specifications as $k => $specific){
                                                $spec .= '<div class="form-group fieldGroup" id="spec-'.$k.'">
                                                    <div class="input-group">
                                                        <input type="text" name="Specifications[]" class="form-control" placeholder="Specification Title" value="'.$specific.'"/>
                                                        
                                                        <div class="input-group-addon"> 
                                                           <a href="javascript:void(0);"> <span class="glyphicon glyphicon glyphicon-remove remove_spe" aria-hidden="true"></span></a>
                                                        </div>
                                                    </div>
                                                    </div>';
                                            
                                                }
                                        }
        }
                                    
        
        $common_fields = '';
        $common_fields1 = '';
        $common_fields2 = '';
        $common_fields4 = '';
        if($key == 0){
        $common_fields1 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Price">'.lang('price').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Price" parsley-trigger="change" value = "'.$result[0]->Price.'" required  class="form-control" id="Price">
                                                            </div>
                                                        </div>';  
        $common_fields2 = '';    
           
        $common_fields = '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive">'.lang('is_active').'</label><br>
                                                                 <input type="checkbox" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').' data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div>';
            $common_fields4 = '<div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Images</label>
                                            <input type="file" name="image[]" id="filer_input2" multiple="multiple">
                                        </div>
                                    </div>
                                </div>';
        }
        
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'"><a href="#'.$language->SystemLanguageID.'" data-toggle="tab" aria-expanded="false">'.$language->SystemLanguageTitle.'</a> </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageID.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('title').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                        '.$common_fields1.'
                                                        
                                                    </div>
                                                    '.$common_fields4.'
                                                    <div class="row"><div class="col-md-12">
                                                             <div class="form-group">
                                                                <label>'.lang('description').'</label>
                                                                <textarea class="form-control" rows="5" name="Description">'.((isset($result[$key]->Description)) ? $result[$key]->Description : '').'</textarea>

                                                                </div>
                                                        </div></div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12"> 
                                                        <label>Product Specifications :</label>
                                                        '.$spec.'
                                                    <div class="form-group fieldGroup">
                                        
                                        
                                       
                                        
                                        
                                        
                                        
                                        
                                        <input type="text" name="Specifications[]" class="form-control" placeholder="Specification Title"/>
                                           
                                            <div class="input-group-addon"> 
                                                <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                            </div>
                                                        </div>
                                                            </div>
                                    </div>    
                                                    
                                                    
                                                    <div class="row">
                                                    '.$common_fields.'
                                                    </div>
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>





<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                                      <?php if(count($languages) > 1){ ?>
                                        <p><?php echo lang('you_should_update_data_seperately_for_each_language'); ?></p>
                                        <div class="tabs-vertical-env row vTable">
                                      
                                            <ul class="nav tabs-vertical vTableLeft p-20">
                                                <?php echo $lang_tabs; ?>
                                            </ul>

                                            <div class="tab-content vTableRight p-20">
                                                <?php echo $lang_data; ?>
                                            </div>
                                       </div>
                                      <?php }else{  ?>
                                       <div class="tab-content">
                                           <div class="p-20">
                                               
                                                <?php echo $lang_data; ?>
                                               
                                           </div>
                                          
                                       </div>   
                                      <?php } ?>
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
    
</div>
<div class="form-group fieldGroupCopy" style="display: none;">
    <input type="text" name="Specifications[]" class="form-control" placeholder="Specification Title"/>
        
        <div class="input-group-addon"> 
            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
        </div>
</div>
<?php
$uploadFiles_data = array();
if ($images) {
    $i = 0;
    foreach ($images as $key => $row) {
        if(@file_get_contents(base_url() . $row['FileName']) != false) {
            $ext = pathinfo($row['FileName'], PATHINFO_EXTENSION);
            $file_name = explode('/', $row['FileName']);
            $contents = file_get_contents(base_url() . $row['FileName']);
            file_put_contents($row['FileName'], $contents);
            $size = filesize($row['FileName']);
            $uploadFiles_data[$i] = array('id' => $row['ImageID'], 'name' => $file_name[2], 'size' => $size, 'type' => $ext, 'file' => base_url() . $row['FileName']);
            $i++;
        }
    }
}

?>
 
   <?php
$Image = array();

$Image['com_url'] = 'cms/product/UploadImages/'.$result[0]->$TableKey."/products"
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
                
                 //group add limit
                var maxGroup = 100;

                //add more fields group
                $(".addMore").click(function(){
                    if($('body').find('.fieldGroup').length < maxGroup){
                        var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                        
                        $(this).parents().find('.fieldGroup:last').after(fieldHTML);
                    }else{
                        alert('Maximum '+maxGroup+' groups are allowed.');
                    }
                });

                //remove fields group
                $("body").on("click",".remove",function(){ 
                    $(this).parents(".fieldGroup").remove();
                });
                
                
                $("body").on("click",".remove_spe",function(){ 
                    $(this).parents(".fieldGroup").remove();
                });
                
                
                
                
                
                
                Image = <?php echo json_encode($Image); ?>;
                uploadFiles = <?php echo json_encode($uploadFiles_data); ?>;
                
                
			});
           
</script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/pages/jquery.fileuploads.init.js"></script> 
<script type="text/javascript">
			$(document).ready(function() {
				
                $(function () {
                                    //Remove button click
                                    $(document).on(
                                            'click',
                                            '[data-role="dynamic-fields"] > .row [data-role="remove"]',
                                            function (e) {
                                                e.preventDefault();
                                                $(this).closest('.row').remove();
                                            }
                                    );
                                    // Add button click
                                    $(document).on(
                                            'click',
                                            '[data-role="dynamic-fields"] > .row [data-role="add"]',
                                            function (e) {
                                                e.preventDefault();
                                                var container = $(this).closest('[data-role="dynamic-fields"]');
                                                new_field_group = container.children().filter('.row:first-child').clone();
                                                new_field_group.find('input').each(function () {
                                                    $(this).val('');
                                                });
                                                container.append(new_field_group);
                                            }
                                    );
                                });
                
                
			});
           
</script>