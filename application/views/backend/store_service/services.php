<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                                                    <h4 class="page-title"><?php echo lang($ControllerName); ?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                        
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
			</div>
                        <!-- end row -->


                         <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                            <input type="hidden" name="form_type" value="save_services">
                                        
                        <div class="row">
                            <?php if($this->session->userdata['admin']['RoleID'] == 3){ ?>
                             <input type="hidden" name="StoreID" value="<?php echo $UserID;?>">
                            
                            <?php }else{ ?>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="StoreID"><?php echo lang('choose_store');?></label>
                                        <select id="StoreID" class="form-control roles" required="" name="StoreID">
                                            <?php foreach($stores as $value){ ?>
                                                    <option value="<?php echo $value->UserID; ?>" <?php echo ($value->UserID == $UserID ? 'selected' : ''); ?>><?php echo $value->Title;?></option>
                                           <?php  } ?>
                                             
                                            
                                        </select>
                                    </div>
                            </div>
                            <?php } ?>
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                     <h4 class="m-t-0 header-title"><?php echo lang('services');?></h4>
                                   
                                    
                                    
                                        <table id="custom-table"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th><div class="checkbox">
                                                        <input id="all_check" type="checkbox"  data-parsley-multiple="all_check">
                                                        <label for="all_check"> &nbsp; </label>
                                                </div>
                                            </th>
                                            <th><?php echo lang('title');?></th>
                                            <th><?php echo lang('is_active');?></th>
                                            <th><?php echo lang('set_service_time');?> (min)</th>
                                            <th><?php echo lang('price');?></th>
                                            
                                        </tr>
                                        
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value->StoreServiceID;?>" >
                                                        <td><div class="checkbox">
                                                                    <input id="all-<?php echo $value->StoreServiceID;?>" class= "all horizontal" data-id = "<?php echo $value->StoreServiceID;?>" type="checkbox" data-parsley-multiple="all-<?php echo $value->StoreServiceID;?>">
                                                                    <label for="all-<?php echo $value->StoreServiceID;?>"> &nbsp; </label>
                                                        </div></td>    
                                                        <td><?php echo $value->ServiceTitle; ?></td>
                                                        <td><div class="checkbox">
                                                                <input id="view-<?php echo $value->StoreServiceID;?>" class= "all horizontal-<?php echo $value->StoreServiceID;?>" <?php echo ($value->IsActive == 1 ? 'checked' : '' );?> type="checkbox" data-parsley-multiple="view-<?php echo $value->StoreServiceID;?>" name="IsActive[<?php echo $value->StoreServiceID;?>]">
                                                                    <label for="view-<?php echo $value->StoreServiceID;?>"> &nbsp; </label>
                                                                </div></td>
                                                        <td><div>
                                                                    <select name="TimeForService[<?php echo $value->StoreServiceID;?>]">
                                                                        <option value="30" <?php echo ($value->TimeForService == 30 ? 'selected' : '' );?>>30
                                                                        </option>
                                                                        <option value="60" <?php echo ($value->TimeForService == 60 ? 'selected' : '' );?>>60
                                                                        </option><option value="90" <?php echo ($value->TimeForService == 90 ? 'selected' : '' );?>>90
                                                                        </option><option value="120" <?php echo ($value->TimeForService == 120 ? 'selected' : '' );?>>120
                                                                        </option></select>
                                                                </div></td>
                                                        <td>
                                                        
                                                        <input type="text" name="StoreServicePrice[<?php echo $value->StoreServiceID;?>]" value="<?php echo $value->StoreServicePrice;?>">
                                                        </td>

                                                        


                                                        
                                                    </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                        <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            <?php echo lang('submit');?>
                                                        </button>
                                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                          <?php echo lang('back');?>
                                                        </button>
                                                        </a>
                                        </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </form>
                                         

                    </div> <!-- container -->

                </div> <!-- content -->
<script src="<?php echo base_url();?>assets/backend/js/module.js"></script>            