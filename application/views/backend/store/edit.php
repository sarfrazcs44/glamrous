<?php
$c_option = '';
if(!empty($cities)){
    foreach($cities as $city){
        $c_option .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } }


?>


<?php

$option2 = '';
if(!empty($categories)){ 
    foreach($categories as $category){ 
        $option2 .= '<option value="'.$category->CategoryID.'" '.((isset($result[0]->CategoryID) && $result[0]->CategoryID == $category->CategoryID) ? 'selected' : '').'>'.$category->Title.' </option>';
    } }  
    
$option3 = '';
if(!empty($branches)){ 
    foreach($branches as $branch){ 
        $option3 .= '<option value="'.$branch->BranchID.'" '.((isset($result[0]->BranchID) && $result[0]->BranchID == $branch->BranchID) ? 'selected' : '').'>'.$branch->Title.' </option>';
    } }      
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields1 = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        $common_fields5 = '';
        $common_fields6 = '';
        if($key == 0){
         
            $common_fields1 = '<div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Mobile">'.lang('mobile').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Mobile" parsley-trigger="change"  class="form-control" id="Mobile" value = "'.$result[0]->Mobile.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Phone">'.lang('PhoneNumber').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Phone" parsley-trigger="change"  class="form-control" id="Phone" value = "'.$result[0]->Phone.'">
                                                            </div>
                                                        </div>
                                                        
                                                    
                                                    </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="Address">'.lang('address').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Address" parsley-trigger="change"  class="form-control" id="Address" value = "'.$result[0]->Address.'">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="OnlineStatus">'.lang('choose_online_status').' *</label>
                                                                    <select id="OnlineStatus" class="form-control"  name="OnlineStatus">

                                                                        <option value="Online" '.($result[0]->OnlineStatus == 'Online' ? 'selected' : '').'>Online</option>
                                                                        <option value="Offline" '.($result[0]->OnlineStatus == 'Offline' ? 'selected' : '').'>Offline</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="CategoryID">'.lang('choose_category').' *</label>
                                                                    <select id="CategoryID" class="form-control" name="CategoryID">
                                                                        '.$option2.'
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Email">'.lang('email').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row">
        '.($result[0]->StoreID != 0 ? '<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="OnlineStatus">'.lang('user_type').'*</label>
                                                                    <select id="UserType" class="form-control"  name="UserType">

                                                                        <option value="warehouse" '.($result[0]->UserType == 'warehouse' ? 'selected' : '').'>Ware House User</option>
                                                                        <option value="delivery" '.($result[0]->UserType == 'delivery' ? 'selected' : '').'>Delivery User</option>
                                                                    </select>
                                                                </div>
                                                            </div><div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="BranchID">'.lang('choose_branch').' *</label>
                                                                    <select id="BranchID" class="form-control" name="BranchID">
                                                                        '.$option3.'
                                                                    </select>
                                                                </div>
                                                            </div>' :'').'
                                                                </div>';
         $common_fields5 = '<div class="row">
        '.($result[0]->StoreID == 0 ? '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="OpenTime">'.lang('open_time').'<span class="text-danger"></span></label>
                                                                <input type="text" name="OpenTime" parsley-trigger="change"  class="form-control time_picker" id="OpenTime" value="'.$result[0]->OpenTime.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6"> 
                                                            <div class="form-group">
                                                                    <label for="CloseTime">'.lang('close_time').'<span class="text-danger"></span></label>
                                                                    <input type="text" name="CloseTime" parsley-trigger="change"  class="form-control time_picker" id="CloseTime" value="'.$result[0]->CloseTime.'">
                                                            </div>
                                                        </div>' :'').'
                                                                </div>';
             $common_fields4 = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Image">'.lang('Image').' :</label>
                                        <input type="file" class="filestyle" id="Image" name="Image[]" data-placeholder="No Image">
                                    </div>
                                    '.(file_exists($result[0]->Image) ? '<img src="'.base_url($result[0]->Image).'" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>' : '').'
                                </div><div class="col-md-6">
                                    <div class="form-group">
                                        <label for="BannerImage">'.lang('banner_image').' :</label>
                                        <input type="file" class="filestyle" id="Image" name="BannerImage[]" data-placeholder="No Image">
                                    </div>
                                    '.(file_exists($result[0]->BannerImage) ? '<img src="'.base_url($result[0]->BannerImage).'" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>' : '').'
                                </div></div>
                                                        <div class="row">
                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive">'.lang('is_active').'</label><br>
                                                                 <input type="checkbox" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').' data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div></div>
                           ';

            $common_fields6 = '<div class="row">
                                
                                                        '.($result[0]->StoreID == 0 ? '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="CityID">'.lang("choose_city").'</label>
                                                                <select id="CityID" class="form-control" required name="CityID">
                                                                   '.$c_option.'
                                                                </select>
                                                            </div>
                                                        </div>' :'').'
                                                        </div>';

        }
        
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'"><a href="#'.$language->SystemLanguageID.'" data-toggle="tab" aria-expanded="false">'.$language->SystemLanguageTitle.'</a> </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageID.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="StoreID" value="'.$result[0]->StoreID.'">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                    
                                                     <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('name').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        '.$common_fields2.'
                                                    </div>
                                                      
                                                    '.$common_fields1.'
                                                   '.$common_fields3.'
                                                    '.$common_fields5.'
                                                    '.$common_fields6.'
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12"> 
                                                            <div class="form-group">
                                                                    <label for="AboutMe">'.lang('about_me').'<span class="text-danger"></span></label>
                                                                    <textarea  name="AboutMe"  class="form-control" id="AboutMe">'.((isset($result[$key]->AboutMe)) ? $result[$key]->AboutMe : '').'</textarea>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                   '.$common_fields4.'
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>





<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                                      <?php if(count($languages) > 1){ ?>
                                        <p><?php echo lang('you_should_update_data_seperately_for_each_language'); ?></p>
                                        <div class="tabs-vertical-env row vTable">
                                      
                                            <ul class="nav tabs-vertical vTableLeft p-20">
                                                <?php echo $lang_tabs; ?>
                                            </ul>

                                            <div class="tab-content vTableRight p-20">
                                                <?php echo $lang_data; ?>
                                            </div>
                                       </div>
                                      <?php }else{  ?>
                                       <div class="tab-content">
                                           <div class="p-20">
                                               
                                                <?php echo $lang_data; ?>
                                               
                                           </div>
                                          
                                       </div>   
                                      <?php } ?>
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
    
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
			});
           
</script>