<link href="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                                                    <h4 class="page-title"><?php echo lang($ControllerName.'s'); ?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                         <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanAdd')){ ?>
                                        <li>
                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                                        </a>
                                        </li>
                                       <?php } ?>
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                          
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th><?php echo lang('title');?></th>
                                            
                                            <th><?php echo lang('is_active');?></th>
                                            
                                             <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                            <?php } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value->AdID;?>">
                                                    
                                                    <th><?php echo $value->Title; ?></th>
                                                    
                                                     
                                                    <th><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></th>
                                                    
                                                     <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>     
                                                    <th>
                                                        <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->AdID);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <?php } ?>
                                                        <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->AdID;?>','cms/<?php echo $ControllerName; ?>/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        <?php } ?>
                                                        </th> 
                                                        <?php } ?>
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <script src="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
               
                $('#datatable-responsive').DataTable();
                
               
               
            });
          

</script> 
