<!DOCTYPE html>
<html lang="en">
<head>
    <?php $site_setting = getSiteSetting();?>
	<title><?php echo $site_setting->SiteName;?> Admin Panel</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/backend/images/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/login_page/css/main.css">
<!--===============================================================================================-->
    
</head>
    <script>
    var base_url = '<?php echo base_url();?>';
    
    </script>
    <style>
        .show{
            display: block !important;
        }
        body {

            background-image: url(<?php echo base_url();?>assets/backend/images/wallpaper.jpg);

            

        }
        
        .login100-form-btn {
display: -webkit-box;
display: -webkit-flex;
display: -moz-box;
display: -ms-flexbox;
display: flex;
justify-content: center;
align-items: center;
padding: 0px 52px;
min-width: 160px;
height: 42px;
background-color: #FF7367;
border-radius: 67px;
font-family: SourceSansPro-SemiBold;
font-size: 14px;
color: #fff;
line-height: 1.2;
text-transform: uppercase;
-webkit-transition: all 0.4s;
-o-transition: all 0.4s;
-moz-transition: all 0.4s;
transition: all 0.4s;
box-shadow: 0 10px 30px 0px rgba(255, 115, 103, 0.5);
-moz-box-shadow: 0 10px 30px 0px rgba(189, 89, 212, 0.5);
-webkit-box-shadow: 0 10px 30px 0px rgba(255, 115, 103, 0.5);
-o-box-shadow: 0 10px 30px 0px rgba(189, 89, 212, 0.5);
-ms-box-shadow: 0 10px 30px 0px rgba(189, 89, 212, 0.5);
}


    </style>
<body>
	
	
	<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
			<form action="<?php echo base_url();?>cms/account/checkLogin" method="post" onsubmit="return false;"  class="login100-form loginForm validate-form form_data">
            
				<span class="login100-form-title p-b-37">
					<?php if(file_exists($site_setting->SiteImage)){
                        $image_url = base_url().$site_setting->SiteImage;
                    }else{
                        $image_url = base_url().'assets/backend/images/schop-01.png';
                    }
                    ?>
                    <img src="<?php echo $image_url;?>" alt="Logo" width="150" class="img-responsive inline-block">
				</span>
                <div class="alert" id="validation-msg" style="display: none;"></div>
				<div class="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
					<input class="input100" type="text" name="Email" placeholder="Username OR Email">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
					<input class="input100" type="password" name="Password" placeholder="password">
					<span class="focus-input100"></span>
				</div>

				<div class="container-login100-form-btn">
					<button type="submit" class="login100-form-btn">
						Sign In
					</button>
				</div>

			</form>

			
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/backend/login_page/js/main.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/script.js"></script>
    <script src="<?php echo base_url();?>assets/backend/js/jquery.blockUI.js"></script>

</body>
</html>
