<?php

$languages = getSystemLanguages();


?>
<?php
$option = '';
if(!empty($cities)){ 
    foreach($cities as $city){ 
        $option .= '<option value="'.$city->CityID.'">'.$city->Title.' </option>';
    } }


?>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                                      <div class="tab-content">
                                            <div class="p-20">
                                                <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="save">
                                                    <input type="hidden" name="UserID" value="<?php echo base64_encode($UserID);?>">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title"><?php echo lang('title'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="CityID"><?php echo lang('choose_city'); ?></label>
                                                                <select id="CityID" class="form-control" required="" name="CityID">
                                                                    <?php echo $option; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive"><?php echo lang('is_active'); ?></label><br>
                                                                 <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                    
                                                   
                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            <?php echo lang('submit');?>
                                                        </button>
                                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>/index/<?php echo base64_encode($UserID);?>">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                          <?php echo lang('back');?>
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                   
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
  </div>                
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
   
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
			});
           
</script>
