<link href="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
                <link href="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                                                    <h4 class="page-title"><?php echo $OrderStatus; ?> <?php echo lang($ControllerName.'s'); ?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                         
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                          
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th><?php echo lang('track_id');?></th>
                                            
                                            <th><?php echo lang('username');?></th>
                                            <th><?php echo lang('store_title');?></th>
                                            <th><?php echo lang('status');?></th>
                                            <th><?php echo lang('time_start');?></th>
                                            <th><?php echo lang('time_end');?></th>
                                            <th><?php echo lang('order_date');?></th>
                                            
                                             <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                            <?php } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value['BookingID'];?>">
                                                    
                                                    <td><?php echo $value['OrderTrackID']; ?></td>
                                                    <td><?php echo $value['StoreTitle']; ?></td>
                                                    <td><?php echo $value['OrderUserTitle']; ?></td>
                                                    <td id="status_<?php echo $value['BookingID'];?>"><?php echo $value['Status']; ?></td>
                                                    
                                                    <td><?php echo date('Y-m-d H:i',$value['TimeStart']); ?></td>
                                                    <td><?php echo date('Y-m-d H:i',$value['TimeEnd']); ?></td>
                                                    <td><?php echo date('d-m-Y H:i',strtotime($value['OrderCreatedAt'])); ?></td>
                                                     <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanDelete')){?>     
                                                    <td>
                                                        <?php if(checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.$value['BookingID']);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        <?php if($value['Status'] != 'Approved'){ ?>
                                                        <select onchange="changeStatus(this.value,'<?php echo $value['BookingID'];?>')">
                                                        <option >Change Stauts</option>
                                                        
                                                        <option value="Approved">Approved</option>
                                                        </select>
                                                        
                                                        <?php } } ?>
                                                        
                                                        </td> 
                                                        <?php } ?>
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <script src="<?php echo base_url();?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="<?php echo base_url();?>assets/backend/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
            $(document).ready(function () {
               
                $('#datatable-responsive').DataTable();
                
               
    
            });
    
    function changeStatus(value, booking_id) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to change status?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + 'cms/booking/change_status',
            data: {'booking_id': booking_id, 'status': value},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != false) {
                    alert('There is something went wrong');

                } else {
                    $('#status_' + booking_id).html(value);

                }


            }, complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}
          

</script> 
