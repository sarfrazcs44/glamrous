<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($activities)){ 
    foreach($activities as $activity){ 
        $option2 .= '<option value="'.$activity->ActivityID.'" '.((isset($result[0]->ActivityID) && $result[0]->ActivityID == $activity->ActivityID) ? 'selected' : '').'>'.$activity->Title.' </option>';
    } }     
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        if($key == 0){
         $common_fields = '<div class="row"><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="form-control" required="" disabled>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="ActivityID'.$key.'">'.lang('choose_activity').'</label>
                                                                <select id="ActivityID'.$key.'" class="form-control" required="" disabled>
                                                                    '.$option2.'
                                                                </select>
                                                            </div>
                                                        </div></div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Email">'.lang('email').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row"><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive">'.lang('is_active').'</label><br>
                                                                 <input type="checkbox" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').' data-plugin="switchery" data-color="#1bb99a" class="form-control" id="IsActive" disabled/>
                                                            </div>
                                                        </div></div>';
       
        }
        
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'"><a href="#'.$language->SystemLanguageID.'" data-toggle="tab" aria-expanded="false">'.$language->SystemLanguageTitle.'</a> </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageID.'">
                     

                                                   
                                                    
                                                     '.$common_fields.'
                                                      <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('name').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                         '.$common_fields2.'
                                                    </div>
                                                    
                                                   '.$common_fields3.'
                                                    

                                                 

                                                


                        </div>';
        
        
        
        
        
    }
}


?>





<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo 'View'.' '.lang($ControllerName);?></h4>
                                      <?php if(count($languages) > 1){ ?>
                                        <p><?php echo lang('you_should_update_data_seperately_for_each_language'); ?></p>
                                        <div class="tabs-vertical-env row vTable">
                                      
                                            <ul class="nav tabs-vertical vTableLeft p-20">
                                                <?php echo $lang_tabs; ?>
                                            </ul>

                                            <div class="tab-content vTableRight p-20">
                                                <?php echo $lang_data; ?>
                                            </div>
                                       </div>
                                      <?php }else{  ?>
                                       <div class="tab-content">
                                           <div class="p-20">
                                               
                                                <?php echo $lang_data; ?>
                                               
                                           </div>
                                          
                                       </div>   
                                      <?php } ?>
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
    
</div>
