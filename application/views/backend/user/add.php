<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                                      <div class="tab-content">
                                            <div class="p-20">
                                                <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="save">

                                                 
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID"><?php echo lang('choose_user_role'); ?> *</label>
                                                                <select id="RoleID" class="form-control" required="" name="RoleID">
                                                                    
                                                                    <?php if(!empty($roles)){ 
                                                                            foreach($roles as $role){ ?>
                                                                                <option value="<?php echo $role->RoleID; ?>"><?php echo $role->Title; ?> </option>
                                                                    <?php } } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title"><?php echo lang('name'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                    
                                                    <div class="row">
                                                       
                                                       
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="Email"><?php echo lang('email'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Email" parsley-trigger="change" required  class="form-control" id="Email">
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                        
                                                    </div>
                                                    
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Password"><?php echo lang('password'); ?><?php echo lang('min_length'); ?><span class="text-danger">*</span></label>
                                                                <input type="password" name="Password" parsley-trigger="change" required  class="form-control" id="Password">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="ConfirmPassword"><?php echo lang('confirm_password'); ?><span class="text-danger">*</span></label>
                                                                <input type="password" name="ConfirmPassword" parsley-trigger="change" required  class="form-control" id="ConfirmPassword">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="store_div" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Mobile"><?php echo lang('mobile'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Mobile" parsley-trigger="change"  class="form-control" id="Mobile">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Phone"><?php echo lang('PhoneNumber'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Phone" parsley-trigger="change"  class="form-control" id="Phone">
                                                            </div>
                                                        </div>
                                                        
                                                    
                                                    </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="Address"><?php echo lang('address'); ?><span class="text-danger">*</span></label>
                                                                <input type="text" name="Address" parsley-trigger="change"  class="form-control" id="Address">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="OnlineStatus"><?php echo lang('choose_online_status'); ?> *</label>
                                                                    <select id="OnlineStatus" class="form-control"  name="OnlineStatus">

                                                                        <option value="Online">Online</option>
                                                                        <option value="Offline">Offline</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="CategoryID"><?php echo lang('choose_category'); ?> *</label>
                                                                    <select id="CategoryID" class="form-control" name="CategoryID">

                                                                        <?php if(!empty($categories)){ 
                                                                                foreach($categories as $category){ ?>
                                                                                    <option value="<?php echo $category->CategoryID; ?>"><?php echo $category->Title; ?> </option>
                                                                        <?php } } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                        
                                                    </div>
                                                     <div class="row">
                                                       
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive"><?php echo lang('is_active'); ?></label><br>
                                                                 <input type="checkbox" checked data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    
                                                   
                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            <?php echo lang('submit');?>
                                                        </button>
                                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                          <?php echo lang('back');?>
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                   
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
  </div>                
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
   
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
                
                $('#RoleID').on('change',function(){
                    if($('#RoleID').val() == 3){
                        $('.store_div').show();
                    }else{
                        $('.store_div').hide();
                    }
                });
			});
    
    
           
</script>
