<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($categories)){ 
    foreach($categories as $category){ 
        $option2 .= '<option value="'.$category->CategoryID.'" '.((isset($result[0]->CategoryID) && $result[0]->CategoryID == $category->CategoryID) ? 'selected' : '').'>'.$category->Title.' </option>';
    } }     
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields1 = '';
        $common_fields2 = '';
        $common_fields3 = '';
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="form-control" required="" name="RoleID" readonly>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
            $common_fields1 = '<div class="store_div" style="'.($result[0]->RoleID == 3 ? 'display:block;' : 'display:none;').'">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Mobile">'.lang('mobile').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Mobile" parsley-trigger="change"  class="form-control" id="Mobile" value = "'.$result[0]->Mobile.'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Phone">'.lang('PhoneNumber').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Phone" parsley-trigger="change"  class="form-control" id="Phone" value = "'.$result[0]->Phone.'">
                                                            </div>
                                                        </div>
                                                        
                                                    
                                                    </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="Address">'.lang('address').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Address" parsley-trigger="change"  class="form-control" id="Address" value = "'.$result[0]->Address.'">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="OnlineStatus">'.lang('choose_online_status').' *</label>
                                                                    <select id="OnlineStatus" class="form-control"  name="OnlineStatus">

                                                                        <option value="Online" '.($result[0]->OnlineStatus == 'Online' ? 'selected' : '').'>Online</option>
                                                                        <option value="Offline" '.($result[0]->OnlineStatus == 'Offline' ? 'selected' : '').'>Offline</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="CategoryID">'.lang('choose_category').' *</label>
                                                                    <select id="CategoryID" class="form-control" name="CategoryID">
                                                                        '.$option2.'
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        
                                                        </div>
                                                        
                                                    </div>';
        $common_fields2 = ' <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="Email">'.lang('email').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row"><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive">'.lang('is_active').'</label><br>
                                                                 <input type="checkbox" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').' data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div></div>';
       
        }
        
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'"><a href="#'.$language->SystemLanguageID.'" data-toggle="tab" aria-expanded="false">'.$language->SystemLanguageTitle.'</a> </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageID.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('name').'<span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="row">
                                                        
                                                        
                                                         '.$common_fields2.'
                                                    </div>
                                                    
                                                   '.$common_fields3.'
                                                    '.$common_fields1.'

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>





<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                                      <?php if(count($languages) > 1){ ?>
                                        <p><?php echo lang('you_should_update_data_seperately_for_each_language'); ?></p>
                                        <div class="tabs-vertical-env row vTable">
                                      
                                            <ul class="nav tabs-vertical vTableLeft p-20">
                                                <?php echo $lang_tabs; ?>
                                            </ul>

                                            <div class="tab-content vTableRight p-20">
                                                <?php echo $lang_data; ?>
                                            </div>
                                       </div>
                                      <?php }else{  ?>
                                       <div class="tab-content">
                                           <div class="p-20">
                                               
                                                <?php echo $lang_data; ?>
                                               
                                           </div>
                                          
                                       </div>   
                                      <?php } ?>
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
    
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
			});
           
</script>