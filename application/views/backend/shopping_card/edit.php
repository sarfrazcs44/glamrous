<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        if($key == 0){
           
        $common_fields = '<div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="CardPrice">Card Price <span class="text-danger">*</span></label>
                                                                <input type="text" name="CardPrice" parsley-trigger="change" required  class="form-control" id="CardPrice" value="'.((isset($result[$key]->CardPrice)) ? $result[$key]->CardPrice : '').'">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="CardValidityDate">Card Valid Till <span class="text-danger">*</span></label>
                                                                <input type="text" name="CardValidityDate" parsley-trigger="change" required  class="form-control datepicker" id="CardValidityDate" value="'.((isset($result[$key]->CardValidityDate)) ? $result[$key]->CardValidityDate : '').'">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="CardCount">Card Purchases Allowed <span class="text-danger">*</span></label>
                                                                <input type="number" name="CardCount" parsley-trigger="change" required  class="form-control" id="CardCount" value="'.((isset($result[$key]->CardCount)) ? $result[$key]->CardCount : '').'">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="IsActive">'.lang('is_active').'</label><br>
                                                                 <input type="checkbox" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').' data-plugin="switchery" data-color="#1bb99a" class="form-control" name ="IsActive" id="IsActive"/>
                                                            </div>
                                                        </div>';
            $common_fields2 = '<div class="row">
                                        <img src="' .base_url(). $result[$key]->Image . '" width="100" height="100">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Image</label>
                                                    <input type="file" name="Image" accept="image/jpeg,image/png">
                                                </div>
                                            </div>
                                        </div>';
        }
        
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'"><a href="#'.$language->SystemLanguageID.'" data-toggle="tab" aria-expanded="false">'.$language->SystemLanguageTitle.'</a> </li>';
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageID.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('title').' <span class="text-danger">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        '.$common_fields.'
                                                        
                                                        
                                                    </div>
                                                    '.$common_fields2.'
                                                    
                                                    
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>





<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo lang('edit').' '.lang($ControllerName);?></h4>
                                      <?php if(count($languages) > 1){ ?>
                                        <p><?php echo lang('you_should_update_data_seperately_for_each_language'); ?></p>
                                        <div class="tabs-vertical-env row vTable">
                                      
                                            <ul class="nav tabs-vertical vTableLeft p-20">
                                                <?php echo $lang_tabs; ?>
                                            </ul>

                                            <div class="tab-content vTableRight p-20">
                                                <?php echo $lang_data; ?>
                                            </div>
                                       </div>
                                      <?php }else{  ?>
                                       <div class="tab-content">
                                           <div class="p-20">
                                               
                                                <?php echo $lang_data; ?>
                                               
                                           </div>
                                          
                                       </div>   
                                      <?php } ?>
                                </div>
                            </div> <!-- end col -->

                           
                        </div>
                        <!-- end row -->




                    </div> <!-- container -->

                </div> <!-- content -->
    
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
			$(document).ready(function() {
				$('form').parsley();
			});
           
</script>
