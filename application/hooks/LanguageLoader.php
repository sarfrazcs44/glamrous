<?php
class LanguageLoader
{
	function initialize() {
        
		$ci =& get_instance();
		$ci->load->helper('language');
		
		if($ci->session->userdata('lang'))
		{
                       $language = $ci->session->userdata('lang');
		} else
		{
			
                 
                    $result = getDefaultLanguage();
                    if($result){
                        $language = $result->ShortCode;
                    }else{
                        $language = 'EN';
                    }
                    
                    
		}
		$ci->lang->load('rest_controller', $language);
	}
}