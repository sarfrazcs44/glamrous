<?php
    Class Temp_order_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("temp_orders");

        }
        
        
        public function getCartData($user_id,$system_language = 'EN'){
        
        $this->db->select('temp_orders.TempOrderID,temp_orders.ProductQuantity,products.*,products_text.Title as ProductTitle,users_text.Title as SalonTitle');
        $this->db->from('temp_orders');
        $this->db->join('products','temp_orders.ProductID = products.ProductID','left');
        
        $this->db->join('products_text','products_text.ProductID = products.ProductID','left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
            
        $this->db->join('users','users.UserID = products.UserID');    
        $this->db->join('users_text','users.UserID = users_text.UserID');    
            
            
        if($system_language) {
                    $this->db->where('system_languages.ShortCode', $system_language);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }    
        
        $this->db->where('temp_orders.UserID',$user_id)->group_by('temp_orders.TempOrderID'); // made this group_by here on 13-09-2018
        return $this->db->get()->result_array();
        
        
    }


    }