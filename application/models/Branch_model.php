<?php
    Class Branch_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("branchs");

        }
        
        
        
        
        public function getAllStoreBranchs($language_code = 'EN',$where = false){
            
            $this->db->select('branchs.*,branchs_text.Title,users_text.Title as StoreTitle,cities_text.Title as CityTitle');
            $this->db->from('branchs');
            $this->db->join('branchs_text','branchs_text.BranchID = branchs.BranchID');
            $this->db->join('system_languages','system_languages.SystemLanguageID = branchs_text.SystemLanguageID');
            
            $this->db->join('cities','cities.CityID = branchs.CityID','left');
            $this->db->join('cities_text','cities_text.CityID = cities.CityID');
            
            $this->db->join('users','users.UserID = branchs.UserID');
            $this->db->join('users_text','users_text.UserID = users.UserID');
            
            $this->db->where('system_languages.ShortCode',$language_code);
            
            if($where){
                $this->db->where($where);
            }
            
            
            return $this->db->get()->result();
            
            
        }


    }