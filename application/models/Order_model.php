<?php


Class Order_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders");

    }
    
    
    
    public function getAllOrders($UserID = false,$system_language_code = 'EN',$order_status,$CityID = false,$DriverID = false){//$UserID is store id 
        
        $this->db->select('orders.OrderID,orders.OrderTrackID,orders.Status,orders.CreatedAt as OrderCreatedAt,orders.UpdatedAt as OrderUpdatedAt,s.UserID as StoreUserID,st.Title as StoreTitle,u.UserID as OrderUserID,ut.Title as OrderUserTitle,dt.Title as DriverName');
        $this->db->from('orders');
        
        $this->db->join('order_items','order_items.OrderID = orders.OrderID');
        $this->db->join('products','order_items.ProductID = products.ProductID');
        $this->db->join('users s','s.UserID = products.UserID');
        $this->db->join('users_text st','st.UserID = s.UserID');
        $this->db->join('system_languages sl','sl.SystemLanguageID = st.SystemLanguageID' );
        
        
        $this->db->join('users u','u.UserID = orders.UserID');
        $this->db->join('users_text ut','ut.UserID = u.UserID');
        $this->db->join('system_languages slu','slu.SystemLanguageID = ut.SystemLanguageID' );

        $this->db->join('users d','d.UserID = orders.DriverID','left');
        $this->db->join('users_text dt','dt.UserID = d.UserID AND dt.SystemLanguageID = 1','left');
        
        if($CityID){
           $this->db->join('address','orders.AddressID = address.AddressID');
           $this->db->join('cities','cities.CityID = address.CityID');
           $this->db->where('cities.CityID', $CityID);
        }

        if($DriverID){
            $this->db->where('orders.DriverID', $DriverID);
        }
        
        
        
        $this->db->where('sl.ShortCode', $system_language_code);
        $this->db->where('orders.Status', $order_status);
        
        
        if($UserID){
            $this->db->where('s.UserID', $UserID);
        }
        
        $this->db->group_by('orders.OrderTrackID');
        
        return $this->db->get()->result_array();
    }
    
    
    
    public function getOrderDetail($order_id,$system_language_code = 'EN',$UserID = false)// $UserID is store id
        
    {
        $this->db->select('users.UserID,users.Email,users_text.Title as FullName,orders.*,orders.CreatedAt as OrderDate,order_items.Quantity,order_items.Price as PlaceOrderPrice,products.*,products_text.Title as ProductTitle,address.*,cities_text.Title as CityTitle');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.OrderID = order_items.OrderID','left');
        $this->db->join('products', 'products.ProductID = order_items.ProductID','left');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID','left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        $this->db->join('users u', 'u.UserID = products.UserID');
        $this->db->join('users', 'users.UserID = orders.UserID','left');
        $this->db->join('users_text', 'users.UserID = users_text.UserID','left');
        $this->db->join('address', 'address.AddressID = orders.AddressID','left');
        $this->db->join('cities', 'cities.CityID = address.CityID','left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID','left');
        $this->db->where('orders.OrderID', $order_id);
        if($UserID){
            $this->db->where('u.UserID', $UserID);
        }
        
        $this->db->where('system_languages.ShortCode', $system_language_code);
        return $this->db->get()->result_array();
    }

    public function getAllOrdersWithDetailsForUser($user_id, $system_language_code = 'EN')
    {
        $this->db->select('users.UserID,users.Email,users_text.Title as FullName,orders.*,orders.CreatedAt as OrderDate,order_items.Quantity,order_items.Price as PlaceOrderPrice,products.*,products_text.Title as ProductTitle,address.*,cities_text.Title as CityTitle');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.OrderID = orders.OrderID');
        $this->db->join('products', 'products.ProductID = order_items.ProductID');
        $this->db->join('products_text', 'products.ProductID = products_text.ProductID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
        $this->db->join('users u', 'u.UserID = products.UserID');
        $this->db->join('users', 'users.UserID = orders.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('address', 'address.AddressID = orders.AddressID');
        $this->db->join('cities', 'cities.CityID = address.CityID');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID');
        $this->db->where('orders.UserID', $user_id);
        $this->db->where('system_languages.ShortCode', $system_language_code);
        return $this->db->get()->result_array();
    }

    public function getOrderItems($order_id)
    {
        $this->db->select('order_items.Quantity,order_items.Price as PlaceOrderPrice,products.*');
        $this->db->from('order_items');
        $this->db->join('products', 'products.ProductID = order_items.ProductID');
        $this->db->where('order_items.OrderID', $order_id);
        return $this->db->get()->result_array();
    }


}