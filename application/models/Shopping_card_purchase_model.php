<?php

Class Shopping_card_purchase_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shopping_card_purchase");
    }

    public function getPurchasedShoppingCards($UserID)
    {
        $this->db->select('shopping_cards.*,shopping_cards_text.*,shopping_card_purchase.*');
        $this->db->from('shopping_card_purchase');
        $this->db->join('shopping_cards', 'shopping_card_purchase.ShoppingCardID = shopping_cards.ShoppingCardID', 'LEFT');
        $this->db->join('shopping_cards_text', 'shopping_cards.ShoppingCardID = shopping_cards_text.ShoppingCardID AND shopping_cards_text.SystemLanguageID = 1', 'LEFT');
        $this->db->where("shopping_card_purchase.UserID", $UserID);
        return $this->db->get()->result_array();
    }

}