<?php

Class Store_service_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("store_services");

    }


    public function getStoreWithServices($store_id, $system_language_code, $where = false)
    {


        $this->db->select('users.UserID,users_text.Title,services.ServiceID,services_text.Title as ServiceTitle,store_services.*');
        $this->db->from('users');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        $this->db->join('store_services', 'users.UserID = store_services.UserID');
        $this->db->join('services', 'services.ServiceID = store_services.ServiceID');
        $this->db->join('services_text', 'services.ServiceID = services_text.ServiceID');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->where('services.Hide', '0');
        $this->db->where('services.IsActive', '1');
        $this->db->where('users.UserID', $store_id);


        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('services.ServiceID');

        $this->db->order_by('services.SortOrder', 'ASC');
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        return $result->result();
    }


    public function getStoresWithServiceID($system_language_code, $where = false)
    {
        // this function is called in api
        $this->db->select('users.UserID,users_text.Title,services.Image as ServiceImage,services.ServiceID,services_text.Title as ServiceTitle,store_services.*,users.Image as UserImage,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('cities_text', 'users.CityID = cities_text.CityID', 'left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        $this->db->join('store_services', 'users.UserID = store_services.UserID');
        $this->db->join('services', 'services.ServiceID = store_services.ServiceID');
        $this->db->join('services_text', 'services.ServiceID = services_text.ServiceID');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->where('services.Hide', '0');
        $this->db->where('services.IsActive', '1');


        if ($where) {
            $this->db->where($where);
        }

         $this->db->group_by('services.ServiceID');

        $this->db->order_by('services.SortOrder', 'ASC');
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        return $result->result_array();
    }


}