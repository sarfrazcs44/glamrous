<?php
Class Shopping_card_number_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shopping_card_numbers");
    }

    public function getPurchasedShoppingCards($where = false)
    {
        $this->db->select('shopping_cards.*,shopping_cards_text.*,shopping_card_purchase.*');
        $this->db->from('shopping_card_purchase');
        $this->db->join('shopping_cards', 'shopping_card_purchase.ShoppingCardID = shopping_cards.ShoppingCardID', 'LEFT');
        $this->db->join('shopping_cards_text', 'shopping_cards.ShoppingCardID = shopping_cards_text.ShoppingCardID AND shopping_cards_text.SystemLanguageID = 1', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        return $this->db->get()->result_array();
    }

    public function getCardNumber($ShoppingCardID, $UserID)
    {
        $this->db->select('*');
        $this->db->from('shopping_card_numbers');
        $this->db->where('ShoppingCardID', $ShoppingCardID);
        $this->db->like('CardNumber', $UserID, 'both');
        return $this->db->get()->row();
    }

}