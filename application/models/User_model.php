<?php

Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }


    /*
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.Title');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        $this->db->where('users.Email',$data['Email']);
        $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    */

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.Title,roles.IsActive as RoleActivation,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities_text', 'users.CityID = cities_text.CityID', 'left');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        $where = '(users.Email OR users.UName = "' . $data['Email'] . '") AND users.Password = "' . $data['Password'] . '"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        $this->db->or_where('users.UName', $data['Email']);
        $this->db->where('users.Password', $data['Password']);

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->row_array();


    }


    public function getUserDataWithID($UserID, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.Title,users_text.AboutMe,roles.IsActive as RoleActivation,cities_text.Title as CityTitle,categories_text.Title as CategoryTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities_text', 'users.CityID = cities_text.CityID', 'left');
        $this->db->join('categories_text', 'users.CategoryID = categories_text.CategoryID', 'left');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        // $this->db->where($where);
        $this->db->where('users.UserID', $UserID);


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        $result = $this->db->get();
        return $result->row_array();


    }


    public function getStoresWithTopRating($system_language_code, $where = false)
    {

        // this function is called in api
        $this->db->select('users.*,users_text.*,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('cities_text', 'users.CityID = cities_text.CityID', 'left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


        if ($where) {
            $this->db->where($where);
        }

        // $this->db->group_by('services.ServiceID');

        $this->db->order_by('users.Rating', 'Desc');
        $result = $this->db->get();
        // echo $this->db->last_query();exit;
        return $result->result_array();
    }

    public function getAllStores($system_language_code, $where)
    {
        $this->db->select('users.*,users_text.*,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('cities_text', 'users.CityID = cities_text.CityID', 'left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        $this->db->where($where);
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function getBackendUsers($where = false)
    {
        $this->db->select('users.*,users_text.Title as FullName,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1');
       // $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'LEFT');
        $this->db->join('roles_text', 'users.RoleID = roles_text.RoleID AND users_text.SystemLanguageID = 1');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->group_by('users.UserID');
        $result = $this->db->get();
        return $result->result();
    }
}

?>