<?php
    Class Product_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("products");

        }
        
        
        

   public function getProductDetail($system_language = "EN",$product_id){

       $this->db->select('products.ProductID,products.Price,
       products_text.Title,products_text.Description,products_text.Specifications,users.Email,users.UserID,users.Address,users_text.Title as StoreTitle');
       
       $this->db->from('products');
       
       
       $this->db->join('products_text','products.ProductID = products_text.ProductID');
       $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
       
       $this->db->join('users','users.UserID = products.UserID');
       $this->db->join('users_text','users.UserID = users_text.UserID');
       
       if($system_language) {
                    $this->db->where('system_languages.ShortCode', $system_language);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        $this->db->where('products.ProductID',$product_id);
       
       return $this->db->get()->row_array();
   }
        
        
    public function getAllProduct($system_language = "EN"){

       $this->db->select('products.ProductID,products.Price,
       products_text.Title,products_text.Description,products_text.Specifications,users.Email,users.UserID,users.Address,users_text.Title as StoreTitle');
       
       $this->db->from('products');
       
       
       $this->db->join('products_text','products.ProductID = products_text.ProductID');
       $this->db->join('system_languages','system_languages.SystemLanguageID = products_text.SystemLanguageID' );
       
       $this->db->join('users','users.UserID = products.UserID');
       $this->db->join('users_text','users.UserID = users_text.UserID');
       
       if($system_language) {
                    $this->db->where('system_languages.ShortCode', $system_language);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        //$this->db->where('products.ProductID',$product_id);
        $this->db->group_by('products.ProductID');
       return   $this->db->get()->result_array();
        
        
   }    


    }