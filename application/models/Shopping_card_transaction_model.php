<?php
Class Shopping_card_transaction_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("shopping_card_transactions");
    }

    public function getTransactionHistory($where = false)
    {
        $this->db->select('shopping_card_transactions.AmountUsed as Outgoing, users_text.Title as PurchasedBy, shopping_card_transactions.OrderID as UsedAtOrderNumber, shopping_card_transactions.UsedAt as Date');
        $this->db->from('shopping_card_transactions');
        $this->db->join('users', 'shopping_card_transactions.UsedBy = users.UserID', 'LEFT');
        $this->db->join('users_text', 'users.UserID = users_text.UserID AND users_text.SystemLanguageID = 1', 'LEFT');
        if ($where) {
            $this->db->where($where);
        }
        return $this->db->get()->result_array();
    }

}