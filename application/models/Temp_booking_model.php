<?php
    Class Temp_booking_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("temp_bookings");

        }
        
        
        public function getCartData($user_id,$system_language = 'EN'){
        
        $this->db->select('temp_bookings.TempBookingID,services.*,services_text.Title as ServiceTitle,users_text.Title as SalonTitle');
        $this->db->from('temp_bookings');
        $this->db->join('store_services','store_services.ServiceID = temp_bookings.ServiceID','left');
        $this->db->join('services','services.ServiceID = store_services.ServiceID','left');
        
        $this->db->join('services_text','services_text.ServiceID = services.ServiceID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = services_text.SystemLanguageID' );
            
        $this->db->join('users','users.UserID = store_services.UserID');    
        $this->db->join('users_text','users.UserID = users_text.UserID');    
            
            
        if($system_language) {
                    $this->db->where('system_languages.ShortCode', $system_language);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }    
        
        $this->db->where('temp_bookings.UserID',$user_id);
        $this->db->group_by('temp_bookings.TempBookingID');
        return $this->db->get()->result_array();
        
        
    }


    }