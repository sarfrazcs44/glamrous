<?php

Class Booking_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("bookings");

    }


    public function getAllBookingOrders($UserID = false, $system_language_code = 'EN', $order_status = false)
    {//$UserID is store id

        $this->db->select('bookings.BookingID,bookings.OrderTrackID,bookings.TimeStart,bookings.TimeEnd,bookings.Status,bookings.CreatedAt as OrderCreatedAt,bookings.UpdatedAt as OrderUpdatedAt,s.UserID as StoreUserID,st.Title as StoreTitle,u.UserID as OrderUserID,ut.Title as OrderUserTitle');
        $this->db->from('bookings');

        $this->db->join('booking_services', 'booking_services.BookingID = bookings.BookingID');
        $this->db->join('services', 'booking_services.ServiceID = services.ServiceID');
        $this->db->join('store_services', 'booking_services.ServiceID = store_services.ServiceID');
        $this->db->join('users s', 's.UserID = store_services.UserID');
        $this->db->join('users_text st', 'st.UserID = s.UserID');
        $this->db->join('system_languages sl', 'sl.SystemLanguageID = st.SystemLanguageID');


        $this->db->join('users u', 'u.UserID = bookings.UserID');
        $this->db->join('users_text ut', 'ut.UserID = u.UserID');
        $this->db->join('system_languages slu', 'slu.SystemLanguageID = ut.SystemLanguageID');

        $this->db->where('sl.ShortCode', $system_language_code);

        if ($order_status)
        {
            $this->db->where('bookings.Status', $order_status);
        }


        if ($UserID) {
            $this->db->where('s.UserID', $UserID);
        }

        $this->db->group_by('bookings.OrderTrackID');

        return $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
    }

    public function getBookingOrderDetail($booking_id, $system_language_code = 'EN', $UserID = false)// $UserID is store id

    {
        $this->db->select('b.UserID,b.Phone,b.Email,bt.Title as BookingUserFullName,s.UserID,s.Email,st.Title as StoreFullName,bookings.*,bookings.CreatedAt as OrderDate,booking_services.*,services.ServiceID,services_text.Title as ServiceTitle,store_services.TimeForService,store_services.StoreServicePrice');
        $this->db->from('bookings');
        $this->db->join('booking_services', 'booking_services.BookingID = bookings.BookingID', 'left');
        $this->db->join('store_services', 'booking_services.ServiceID = store_services.ServiceID', 'left');
        $this->db->join('services', 'services.ServiceID = store_services.ServiceID', 'left');
        $this->db->join('services_text', 'services.ServiceID = services_text.ServiceID', 'left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = services_text.SystemLanguageID');
        $this->db->join('users s', 's.UserID = store_services.UserID');
        $this->db->join('users_text st', 's.UserID = st.UserID', 'left');


        $this->db->join('users b', 'b.UserID = bookings.UserID');
        $this->db->join('users_text bt', 'b.UserID = bt.UserID', 'left');


        $this->db->where('bookings.BookingID', $booking_id);
        if ($UserID) {
            $this->db->where('s.UserID', $UserID);
        }

        $this->db->where('system_languages.ShortCode', $system_language_code);

        $this->db->group_by('services.ServiceID');
        return $this->db->get()->result_array();
    }

    public function checkIfBookingExistInTimeslot($user_id, $start_time, $end_time)
    {
        /*$sql = "SELECT b.* FROM bookings b LEFT JOIN booking_services bs ON b.BookingID = bs.BookingID JOIN store_services ss ON bs.ServiceID = ss.ServiceID AND ss.UserID = $user_id AND '$start_time' BETWEEN DATE_FORMAT(FROM_UNIXTIME(b.TimeStart), '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(FROM_UNIXTIME(b.TimeEnd), '%Y-%m-%d %H:%i:%s') AND '$end_time' BETWEEN DATE_FORMAT(FROM_UNIXTIME(b.TimeStart), '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(FROM_UNIXTIME(b.TimeEnd), '%Y-%m-%d %H:%i:%s')";*/
        $sql = "SELECT b.* FROM bookings b LEFT JOIN booking_services bs ON b.BookingID = bs.BookingID JOIN store_services ss ON bs.ServiceID = ss.ServiceID AND ss.UserID = $user_id AND '$start_time' BETWEEN b.TimeStart AND b.TimeEnd AND '$end_time' BETWEEN b.TimeStart AND b.TimeEnd";
        $query = $this->db->query($sql);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getOrderServices($booking_id)
    {
        $this->db->select('bookings.*,booking_services.*,services_text.ServiceID,services.Image as ServiceImage,services_text.Title as ServiceTitle');
        $this->db->from('booking_services');
        $this->db->join('bookings', 'booking_services.BookingID = bookings.BookingID');
        $this->db->join('services', 'booking_services.ServiceID = services.ServiceID');
        $this->db->join('services_text', 'services.ServiceID = services_text.ServiceID');
        $this->db->where('booking_services.BookingID', $booking_id);
        return $this->db->get()->result_array();
    }


}