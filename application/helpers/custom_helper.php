<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function currentDate()
{
    return date('Y-m-d H:i:s');
}


function print_rm($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}


function checkRightAccess($module_id, $role_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['RoleID'] = $role_id;
    $fetch_by[$can] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSiteSetting($site_id = 1)
{
    $CI = &get_Instance();
    $CI->load->Model('Site_setting_model');

    $result = $CI->Site_setting_model->get($site_id, false, 'SiteSettingID');
    return $result;
}

function checkUserRightAccess($module_id, $user_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['UserID'] = $user_id;
    $fetch_by[$can] = 1;

    $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSystemLanguages()
{
    $CI = &get_Instance();
    $CI->load->model('System_language_model');
    $languages = $CI->System_language_model->getAllLanguages();
    return $languages;
}


function checkStoreHasRights($module_id, $user_id)
{

    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');
    $row = $CI->Modules_users_rights_model->checkStoreHasRights($module_id, $user_id);
    if (!empty($row)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function getDefaultLanguage()
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $fetch_by = array();
    $fetch_by['IsDefault'] = 1;
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}


function getAllActiveModules($role_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Module_rights_model');
    $result = $CI->Module_rights_model->getModulesWithRights($role_id, $system_language_id, $where);
    return $result;
}


function getActiveUserModule($user_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Modules_users_rights_model');
    $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id, $system_language_id, $where);
    return $result;
}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url'));
    }
}

function getProductImages($product_id)
{
    $CI = &get_Instance();
    $CI->load->model('Image_model');
    $fetch_by = array();
    $fetch_by['OtherTableID'] = $product_id;
    $fetch_by['OtherTableName'] = 'products';
    $result = $CI->Image_model->getMultipleRows($fetch_by, true);
    return $result;
}

function RandomString()
{
    $characters = '0123456789012345678901234567890123456789012345678912';
    $randstring = '';
    for ($i = 0; $i < 4; $i++) {
        $randstring .= $characters[rand(0, 50)];
    }
    return $randstring;
}

function sendEmail($data = array())
{

    $CI = &get_Instance();
    $CI->load->library('email');
    $CI->email->from($data['from']);
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['body']);
    $CI->email->set_mailtype('html');

    if ($CI->email->send()) {
        return true;

    } else {
        return false;
    }
}

function getDecimalHours($time)
{
    $hms = explode(":", $time);
    //return ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));
    return ($hms[0] + ($hms[1] / 60));
}

function uploadImage($key, $path)
{
    $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];
    $file_size = $_FILES[$key]['size'];
    $file_tmp = $_FILES[$key]['tmp_name'];
    $file_type = $_FILES[$key]['type'];
    move_uploaded_file($file_tmp, $path . $file_name);
    return $path . $file_name;
}

function generate_string($id, $strength = 7)
{
    $a = rand(1, $strength - 1);
    $input = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $input_length = strlen($input);
    $random_string = '';
    for ($i = 0; $i < $strength; $i++) {
        if ($i == $a) {
            $random_character = $id;
        } else {
            $random_character = $input[mt_rand(0, $input_length - 1)];
        }
        $random_string .= $random_character;
    }

    return $random_string;
}

function getUserIDFromString($string)
{
    preg_match_all('!\d+!', $string, $matches);
    return implode('', $matches[0]);
}


