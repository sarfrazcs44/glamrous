<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            'Branch_model',
            'User_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        //$this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
        $this->data['TableKey'] = 'OrderID';
        $this->data['Table'] = 'orders';


    }


    public function index($order_status = 'Received')
    {

        if ($order_status == 'Received') {
            if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
                $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
                redirect(base_url('cms/dashboard'));
            }
        } elseif ($order_status == 'Dispatched') {
            if (!checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
                $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
                redirect(base_url('cms/dashboard'));
            }

        } elseif ($order_status == 'Delivered') {
            if (!checkUserRightAccess(47, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
                $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
                redirect(base_url('cms/dashboard'));
            }
        }


        $parent = $this->data['Parent_model'];
        //  $child                              = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $UserID = false;
        $CityID = false;
        $DriverID = false;
        if ($this->session->userdata['admin']['RoleID'] == 3) {

            if($this->session->userdata['admin']['UserType'] == 'delivery'){
                $DriverID = $this->session->userdata['admin']['UserID'];
            }
           
            if($this->session->userdata['admin']['StoreID'] != 0){
               $get_branch_data = $this->Branch_model->get($this->session->userdata['admin']['BranchID'],false,'BranchID');
               $CityID = $get_branch_data->CityID; 
               $UserID = $this->session->userdata['admin']['StoreID'];
            }else{
              $UserID = $this->session->userdata['admin']['UserID'];  
            }
            
           
        }

        $this->data['results'] = $this->$parent->getAllOrders($UserID, $this->language, $order_status,$CityID,$DriverID);
        //print_rm($this->data['results']);

        // $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language);
        $this->data['OrderStatus'] = $order_status;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function edit($id = '')
    {
        if (!checkUserRightAccess(44, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function get_drivers()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');

        $this->data['users'] = $this->User_model->getBackendUsers('users.RoleID = 3 AND users.UserType = "delivery" AND users.StoreID = '.$this->session->userdata['admin']['UserID']);

        $html = $this->load->view('backend/order/assign_driver_popup', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }


    public function get_otp()
    {

        $this->data['OrderID'] = $this->input->post('order_id');

        

        $html = $this->load->view('backend/order/otp_pop_up', $this->data, true);

        $response = array();
        $response['html'] = $html;
        echo json_encode($response);


    }

    public function update_status_with_otp()
    {

        $this->data['OrderID'] = $this->input->post('OrderID');
        $this->data['OTP'] = $this->input->post('OTP');


        $OrderData = $this->Order_model->get($this->data['OrderID'],true,'OrderID');

        if($OrderData['DeliveryOTP'] == $this->data['OTP']){
            $this->Order_model->update(array('Status' => 'Delivered'),array('OrderID' => $this->data['OrderID'] ));
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = TRUE;
            // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
            echo json_encode($success);
        }else{
            $success['error'] = 'Delivery OTP is not valid';
            $success['success'] = false;
           
           
            echo json_encode($success);

        }

        

        



    }

    public function assignDriver()
    {
        $update = array();
        $update_by = array();
        $update_by['OrderID'] = $this->input->post('OrderID');
        $update['DriverID'] = $this->input->post('UserID');
       
        
        $order_info = $this->Order_model->getOrderDetail($this->input->post('OrderID'), 'EN', false);
        $order_info = $order_info[0];
        if($order_info['DeliveryOTP'] == ''){
             $update['DeliveryOTP'] = RandomString();
            $email = array();
        $email['body'] = 'Your order is assign to delivery boy with delivery OTP ' . $update['DeliveryOTP'] . '. Please give this OTP to the delivery boy who give you parcel.';
        $email['to'] = $order_info['Email'];
        $email['from'] = 'no-reply@glamorous.com';
        $email['subject'] = 'Delivery OTP';
       
        $mail_sent = sendEmail($email);
    }
        $this->Order_model->update($update, $update_by);
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['reload'] = TRUE;
        // $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $OrderID;
        echo json_encode($success);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;
            case 'assign_driver':
                $this->assignDriver();
                break;
        }
    }

    public function change_status()
    {
        $post_data = $this->input->post();


        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');


        $update_by = array();
        $update = array();
        $update_by['OrderID'] = $post_data['order_id'];
        $update['Status'] = $post_data['status'];
        $this->Order_model->update($update, $update_by);

        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit;
    }


    public function view($order_id)
    {
        $UserID = false;
        if ($this->session->userdata['admin']['RoleID'] == 3) {
            $UserID = $this->session->userdata['admin']['UserID'];
        }

        $this->data['order_items'] = $this->Order_model->getOrderDetail($order_id, 'EN', $UserID);
//print_rm($this->data['order_items']);
        if (!$this->data['order_items']) {
            redirect(base_url('cms/order'));
        }
        $this->data['view'] = 'backend/order/invoice';


        $this->data['OrderID'] = $order_id;
        $this->load->view('backend/layouts/default', $this->data);

    }


}