<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store_service extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                $this->load->Model([
			ucfirst($this->router->fetch_class()).'_model',
                    'User_model',
                    'Service_model'
			
		]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                
                $this->data['TableKey'] = 'StoreServiceID';
                $this->data['Table'] = 'store_services';
       
		
	}
	 
    
    public function index($store_id = '')
	{
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
       
        $this->data['stores']   = $this->User_model->getAllJoinedData(false,'UserID',$this->language,'users.RoleID = 3 AND users.StoreID = 0');
        
        if(empty($this->data['stores'])){
            
            $this->session->set_flashdata('message',lang('please_add_store_first'));
            redirect(base_url('cms/dashboard'));
        }
        if($store_id == ''){
            if($this->session->userdata['admin']['RoleID'] == 3){
                $store_id = $this->session->userdata['admin']['UserID'];
            }else{
                $store_id = $this->data['stores'][0]->UserID;
            }
            
       }
        
        $parent                             = $this->data['Parent_model'];
        
        $this->data['UserID']               = $store_id;
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/services';
        $this->data['results'] = $this->Store_service_model->getStoreWithServices($store_id,$this->language);
        
      
        if(empty($this->data['results'])){
            
            $this->session->set_flashdata('message',lang('please_add_services_first'));
            redirect(base_url('cms/dashboard'));
        }
        
        
        
        
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save_services':
               
                $this->saveServices();
          break; 
           
        }
    }
    
    
    private function saveServices(){
        
        $parent                             = $this->data['Parent_model'];
        
        $store_id   = $this->input->post('StoreID');
      
        $services = $this->Store_service_model->getStoreWithServices($store_id,$this->language);
        
        $is_active   = $this->input->post('IsActive');      
        $time_for_service    = $this->input->post('TimeForService');
        $price_for_service    = $this->input->post('StoreServicePrice');
        
         
        if(!empty($services)){
            
            foreach($services as $value){
              
                $update_data[] = [
                                    'StoreServiceID'     => $value->StoreServiceID,
                                    'IsActive'		=> (isset($is_active[$value->StoreServiceID]) ? 1 : 0),
                                    'TimeForService'            => $time_for_service[$value->StoreServiceID],
                                    'StoreServicePrice'            => $price_for_service[$value->StoreServiceID],
                                    'UpdatedAt'         => date('Y-m-d H:i:s'),
                                    'UpdatedBy'         => $this->session->userdata['admin']['UserID']
                    
				];
            }
            
            $this->Store_service_model->update_batch($update_data,'StoreServiceID');
            $success['error']   = false;
            $success['success'] = lang('update_successfully');
            $success['reload']   = true;

            echo json_encode($success);
            exit;  


            
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] =   false;
            
            echo json_encode($errors);
            exit;
        }
        
        
        
        
        
        
    }
     
    
    
	

}