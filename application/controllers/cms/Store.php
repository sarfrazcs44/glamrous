<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                $this->load->Model([
			            'User_model',
			            'User_text_model',
                        'Role_model',
                        'User_model',
                        'Modules_users_rights_model',
                        'Module_model',
                        'Category_model',
                        'Service_model',
                        'Store_service_model',
                        'City_model',
                        'Branch_model'
			
		]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = 'User_model';
                $this->data['Child_model']    = 'User_text_model';
                $this->data['TableKey'] = 'UserID';
                $this->data['Table'] = 'users';
       
		
	}
	 
    
     public function index()
	{
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          
          $where = '';
          if($this->session->userdata['admin']['RoleID'] == 3){
            $where = ' AND users.UserID = '.$this->session->userdata['admin']['UserID'].'';  
          }
          $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,'users.RoleID = 3 AND users.StoreID = 0'.$where.'');

          
          
          $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function store_users($store_id = '')
	{
          if($store_id != ''){
              $parent                             = $this->data['Parent_model'];
              $child                              = $this->data['Child_model'];
              $store_id                           = base64_decode($store_id);
              $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/store_users';

              $where = '';
              if($this->session->userdata['admin']['RoleID'] == 3){
                 $store_id = $this->session->userdata['admin']['UserID'];
              }
              $this->data['UserID'] = $store_id;
              $where = ' AND users.StoreID = '.$store_id.''; 

              $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language,'users.RoleID = 3'.$where.'');
              $this->load->view('backend/layouts/default',$this->data);
          }else{
              $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
              redirect(base_url('cms/'.$this->router->fetch_class())); 
              
          }
          
	}
    public function add($store_id = 0)// this is actually a store id mean user id
    {
        
         if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        $this->data['StoreID'] = $store_id;

        $this->data['cities'] = $this->City_model->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1');
        if(empty($this->data['cities'] )){
            $this->session->set_flashdata('message',lang('please_add_city_first'));
            redirect(base_url('cms/store'));
        }
        
        $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.IsActive = 1');
        
        $where = 'branchs.UserID = '.base64_decode($store_id).'';
        $this->data['branches'] = $this->Branch_model->getAllStoreBranchs($this->language,$where);
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function add_branch()
    {
        
         if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add_branch';
        
        
        $this->data['cities'] = $this->City_model->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1');
        
        $this->load->view('backend/layouts/default',$this->data);
    }

        
     public function edit($id = '')
	{
         if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']		    = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');

        $this->data['cities'] = $this->City_model->getAllJoinedData(false,'CityID',$this->language,'cities.IsActive = 1');
        if(empty($this->data['cities'] )){
            $this->session->set_flashdata('message',lang('please_add_city_first'));
            redirect(base_url('cms/store'));
        }
        
        $where = 'branchs.UserID = '.$this->data['result'][0]->StoreID.'';
        $this->data['branches'] = $this->Branch_model->getAllStoreBranchs($this->language,$where);
        
        
        if(!$this->data['result']){
            $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
        
        
        
        
       $this->data['categories'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.IsActive = 1');
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
       
	$this->load->view('backend/layouts/default',$this->data);
		
	}    
    
    
    public function rights($user_id = ''){
        
        if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit') && !checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
       if($user_id == ''){
           $user_id = $this->session->userdata['admin']['UserID'];
       }
        $use_data = $this->User_model->get($user_id,false,'UserID');
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $this->data['UserID']               = $user_id;
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/rights';
        $this->data['results'] = $this->Modules_users_rights_model->getModulesWithRights($user_id,$this->language);
        
      
        if(empty($this->data['results'])){
            
            $this->session->set_flashdata('message',lang('please_add_module_first'));
            redirect(base_url('cms/'.$this->router->fetch_class()));
        }
        //print_rm($this->data['results']);
       // $this->data['users']   = $this->User_model->getAllJoinedData(false,'UserID',$this->language);
        
        if($this->session->userdata['admin']['RoleID'] == 3 ){
            if($this->session->userdata['admin']['StoreID'] == 0){
                $store_id = $this->session->userdata['admin']['UserID'];
            }else{
                $store_id = $this->session->userdata['admin']['StoreID'];
            }
        }else{
            if($use_data){
                if($use_data->StoreID == 0){
                    $store_id = $use_data->UserID;
                }else{
                    $store_id = $use_data->StoreID;
                }
            }
            //$store_id = $use_data->
        }
         
       
        $where = ' AND users.StoreID = '.$store_id.''; 
        $this->data['users'] = $this->User_model->getAllJoinedData(false,$this->data['TableKey'],$this->language,'users.RoleID = 3'.$where.'');
        
        if(empty($this->data['users'])){
            
            $this->session->set_flashdata('message',lang('please_add_user_first'));
            redirect(base_url('cms/store'));
        }
        
        
        
        $this->load->view('backend/layouts/default',$this->data);
        
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->update();
          break;
            case 'delete':
                //$this->validate();
                $this->delete();
          break; 

          case 'save_rights':
                $this->saveRights();
          break; 
            case 'save_branch':
                $this->saveBranch();
                 
        }
    }
    
    private function saveBranch(){
        if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        
        
        print_rm($this->input->post());
    }
    
    
    private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        
           // $this->form_validation->set_rules('RoleID', lang('role'), 'required');
            $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
            $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
            $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
             if($this->input->post('StoreID') != '0'){
               $this->form_validation->set_rules('BranchID', 'Branch', 'required'); 
             }
        
            $this->form_validation->set_rules('Phone', lang('PhoneNumber'), 'required'); 
           $this->form_validation->set_rules('Mobile', lang('mobile'), 'required');     
           $this->form_validation->set_rules('Address', lang('address'), 'required');     
           $this->form_validation->set_rules('CategoryID', lang('choose_category'), 'required');

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
                return true;
        }

    }	

    
    private function save()
    {
        
        if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        if($this->input->post('StoreID') != '0'){
        
            $save_parent_data['StoreID']  = base64_decode($post_data['StoreID']);
            $save_parent_data['UserType'] = $post_data['UserType'];
        }
        
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['RoleID']         = 3;
        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];
        
        $save_parent_data['Mobile']         = $post_data['Mobile'];
        $save_parent_data['Phone']          = $post_data['Phone'];
        $save_parent_data['Address']        = $post_data['Address'];
        $save_parent_data['OnlineStatus']   = $post_data['OnlineStatus'];
        $save_parent_data['CategoryID']     = $post_data['CategoryID'];
       
         if($this->input->post('StoreID') != '0'){
           $save_parent_data['BranchID']       = (isset($post_data['BranchID']) ? $post_data['BranchID'] : 0 );
        }
        
        if($this->input->post('StoreID') == '0'){
        
            $save_parent_data['OpenTime']       = $post_data['OpenTime'];
            $save_parent_data['CloseTime']     = $post_data['CloseTime'];
        }

        if($this->input->post('StoreID') == '0'){

            $save_parent_data['CityID']     = $post_data['CityID'];
        }
        
        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');		
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->uploadImageAdd("Image", "uploads/store/");
        }
        if (isset($_FILES['BannerImage']["name"][0]) && $_FILES['BannerImage']["name"][0] != '') {
                        $save_parent_data['BannerImage'] = $this->uploadImageAdd("BannerImage", "uploads/store/");
        } 
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
            
                $default_lang = getDefaultLanguage();
                
                
                
                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data['AboutMe']                       = (isset($post_data['AboutMe']) ? $post_data['AboutMe'] : '' );
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);


                $modules = $this->Module_model->getAll();
                foreach($modules as $key => $value)
                {
                    /*if($post_data['RoleID'] == 1 || $post_data['RoleID'] == 2){
                        $rights_all = 1;
                    }else{
                        $rights_all = 0;
                    }*/

                        $other_data[] = [
                                'ModuleID'  => $value->ModuleID,
                                'UserID'    => $insert_id,
                                'CanView'   => (checkUserRightAccess($value->ModuleID, 3, 'CanView') ? 1 : 0),
                                'CanAdd'    => (checkUserRightAccess($value->ModuleID, 3, 'CanAdd') ? 1 : 0),
                                'CanEdit'   => (checkUserRightAccess($value->ModuleID, 3, 'CanEdit') ? 1 : 0),
                                'CanDelete' => (checkUserRightAccess($value->ModuleID, 3, 'CanDelete') ? 1 : 0),
                                'CreatedAt' => date('Y-m-d H:i:s'),
                                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                                'UpdatedAt' => date('Y-m-d H:i:s'),
                                'UpdatedBy' => $this->session->userdata['admin']['UserID']
                        ];



                }
                
                
               
                $this->Modules_users_rights_model->insert_batch($other_data);
                
                
            
                $services = $this->Service_model->getAll();
                $services_data = array();
                foreach($services as $key => $value)
                {

                        $services_data[] = [
                                'ServiceID'	=> $value->ServiceID,
                                'UserID'	=> $insert_id,
                                'IsActive'	=> 1,
                                'TimeForService'	=> 60,
                                'CreatedAt'	=> date('Y-m-d H:i:s'),
                                'CreatedBy'	=> $this->session->userdata['admin']['UserID'],
                                'UpdatedAt'	=> date('Y-m-d H:i:s'),
                                'UpdatedBy'	=> $this->session->userdata['admin']['UserID']
                        ];



                }
                
                
               
                $this->Store_service_model->insert_batch($services_data);
            
            
            
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
	}
    
        private function update()
	{
		if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
                $post_data = $this->input->post();
                
                //print_r($post_data);exit;
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']		    = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
	
        
                if(!$this->data['result']){
                    
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $success['redirect'] = true;
                   $success['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }

            
                
	        unset($post_data['form_type']);
		        $save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                    
                   
                    
                    //$save_parent_data['Email']          = $post_data['Email'];
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                   // $save_parent_data['RoleID']         = $post_data['RoleID'];
                    //$save_parent_data['CenterID']     = $post_data['CenterID'];
                    
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');		
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                     if($this->input->post('StoreID') != '0'){
                        //$save_parent_data['StoreID']  = base64_decode($post_data['StoreID']);
                        $save_parent_data['UserType'] = $post_data['UserType'];
                    }
                    $save_parent_data['Mobile']         = $post_data['Mobile'];
                    $save_parent_data['Phone']          = $post_data['Phone'];
                    $save_parent_data['Address']        = $post_data['Address'];
                    $save_parent_data['OnlineStatus']   = $post_data['OnlineStatus'];
                    $save_parent_data['CategoryID']     = $post_data['CategoryID'];
                    if($this->input->post('StoreID') != '0' && isset($post_data['BranchID'])){

                        $save_parent_data['BranchID']       = $post_data['BranchID']; 
                     }

                     if($this->input->post('StoreID') == '0'){

                         $save_parent_data['OpenTime']       = $post_data['OpenTime'];
                         $save_parent_data['CloseTime']     = $post_data['CloseTime'];
                     }

                    if($this->input->post('StoreID') == '0'){

                        $save_parent_data['CityID']     = $post_data['CityID'];
                    }

                    if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                        $save_parent_data['Image'] = $this->uploadImageAdd("Image", "uploads/store/");
                    }
                    
                    if (isset($_FILES['BannerImage']["name"][0]) && $_FILES['BannerImage']["name"][0] != '') {
                        $save_parent_data['BannerImage'] = $this->uploadImageAdd("BannerImage", "uploads/store/");
                    }
                    
                    $this->$parent->update($save_parent_data,$update_by);
                    $save_child_data['Title']                        = $post_data['Title'];
                    if(isset($post_data['AboutMe'])){
                      $save_child_data['AboutMe']                        = $post_data['AboutMe'];
                    }
                    
                   
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data['AboutMe']                      = $post_data['AboutMe'];
                       
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];

  

                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                      
                        $save_child_data['Title']                        =  $post_data['Title'];
                        $save_child_data['AboutMe']                      = $post_data['AboutMe'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }
		
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
                
                
              
              
           
		
        


        
	}else{
           
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    
    
   private function saveRights(){
        
        
        
        
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $user_id   = $this->input->post('UserID');
      
        $modules = $this->Modules_users_rights_model->getModulesWithRights($user_id,$this->language);
        
        $can_view   = $this->input->post('CanView');
        
        $can_add    = $this->input->post('CanAdd');
        $can_edit   = $this->input->post('CanEdit');
        $can_delete = $this->input->post('CanDelete');
         
        if(!empty($modules)){
            
            foreach($modules as $module){
              
                $update_data[] = [
                                    'ModuleRightID'     => $module->ModuleRightID,
                                    'CanView'       => (isset($can_view[$module->ModuleRightID]) ? 1 : 0),
                                    'CanAdd'            => (isset($can_add[$module->ModuleRightID]) ? 1 : 0),
                                    'CanEdit'           => (isset($can_edit[$module->ModuleRightID]) ? 1 : 0),
                                    'CanDelete'         => (isset($can_delete[$module->ModuleRightID]) ? 1 : 0),
                                    'UpdatedAt'         => date('Y-m-d H:i:s'),
                                    'UpdatedBy'         => $this->session->userdata['admin']['UserID']
                    
                ];
            }
            
            $this->Modules_users_rights_model->update_batch($update_data,'ModuleRightID');
            $success['error']   = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = true;

            echo json_encode($success);
            exit;  


            
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] =   false;
            
            echo json_encode($errors);
            exit;
        }
        
        
        
        
        
        
    }
    
    
   
    
    private function delete(){
        
        if(!checkUserRightAccess(41,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

        $this->Modules_users_rights_model->delete($deleted_by);
        
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        
       
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    
     
    
    
    
	

}