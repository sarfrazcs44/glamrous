<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                $this->load->Model([
			ucfirst($this->router->fetch_class()).'_model',
			'Booking_service_model'
		]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                //$this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'BookingID';
                $this->data['Table'] = 'bookings';
       
		
	}
	 
    
    public function index($order_status = 'Pending')
	{
         
          
        
         $parent                             = $this->data['Parent_model'];
        //  $child                           = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          $UserID = false;
          if($this->session->userdata['admin']['RoleID'] == 3){
              $UserID = $this->session->userdata['admin']['UserID'];
          }
        
          $this->data['results'] = $this->$parent->getAllBookingOrders($UserID,$this->language,$order_status);
        
         // print_rm($this->data['results']);
          
          $this->data['OrderStatus'] = $order_status;
          $this->load->view('backend/layouts/default',$this->data);
        
        
	}
    
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
           
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
   
    public function change_status()
    {
        $post_data = $this->input->post();


        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');


        $update_by = array();
        $update = array();
        $update_by['BookingID'] = $post_data['booking_id'];
        $update['Status'] = $post_data['status'];
        $this->Booking_model->update($update, $update_by);

        $success['error']   = false;
        $success['success'] = lang('update_successfully');
        echo json_encode($success);
        exit; 
    }
    
    
   
    
    private function delete(){
        
         if(!checkUserRightAccess(48,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    public function view($booking_id)
    {
        $UserID = false;
        if($this->session->userdata['admin']['RoleID'] == 3){
              $UserID = $this->session->userdata['admin']['UserID'];
          }

        $this->data['order_items'] = $this->Booking_model->getBookingOrderDetail($booking_id,'EN',$UserID);
//print_rm($this->data['order_items']);
        if (!$this->data['order_items']) {
            redirect(base_url('cms/order'));
        }
        $this->data['view'] = 'backend/booking/invoice';


        $this->data['BookingID'] = $booking_id;
        $this->load->view('backend/layouts/default', $this->data);

    }
    
    
    
	

}