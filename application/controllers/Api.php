<?php


defined('BASEPATH') OR exit('No direct script access allowed');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller
{

    protected $language;
    public $post_data = array();

    function __construct()

    {

        // Construct the parent class

        parent::__construct();


        // Configure limits on our controller methods

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php

        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key

        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key

        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key

        $this->load->model('Ad_model');
        $this->load->model('Ad_text_model');
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('Service_model');
        $this->load->model('Store_service_model');
        $this->load->model('Product_model');
        $this->load->model('Image_model');
        $this->load->model('Address_model');
        $this->load->model('Order_model');
        $this->load->model('Booking_model');
        $this->load->model('Order_item_model');
        $this->load->model('Booking_service_model');
        $this->load->model('Temp_order_model');
        $this->load->model('Temp_booking_model');

        $this->load->model('City_model');
        $this->load->model('City_text_model');
        $this->load->model('Category_model');
        $this->load->model('Shopping_card_model');
        $this->load->model('Shopping_card_purchase_model');
        $this->load->model('Shopping_card_number_model');
        $this->load->model('Shopping_card_received_model');
        $this->load->model('Shopping_card_transaction_model');
         $this->load->helper('language');

        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $this->post_data = $this->input->get();
        } elseif ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->post_data = $this->input->post();
        }

        if (isset($this->post_data['Language']) && $this->post_data['Language'] !== '') {
            $this->language = $this->post_data['Language'];
            unset($this->post_data['Language']);
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }

        if (isset($this->post_data['Language']) && $this->post_data['Language'] !== '') {
            $this->language = $this->post_data['Language'];
            unset($this->post_data['Language']);
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'EN';
            }
        }



    }

    public function test_get()
    {
        $startTime = time();
        $cenvertedTime = strtotime('+1 hour', $startTime);
        echo 'Converted Time (added 1 hour): ' . $cenvertedTime;
    }


    public function userRegistration_post()

    {

        $data = array();

        $data_fetch = array();

        $post_data = $this->post_data;

        if (!empty($post_data)) {

            if (isset($post_data['temp_order_key'])) {
                $temp_order_key = $post_data['temp_order_key'];
                unset($post_data['temp_order_key']);
            }

            $user = false;
            $user_name = false;

            if (isset($post_data['Email'])) {
                $data_fetch['Email'] = $post_data['Email'];
                $user = $this->User_model->getMultipleRows($data_fetch, true);
            }

            if (isset($post_data['UName'])) {
                $fetch_username = array();
                $fetch_username['UName'] = $post_data['UName'];
                $user_name = $this->User_model->getMultipleRows($fetch_username, true);
            }

            if ($user) {


                $this->response([

                    'status' => FALSE,

                    'message' => 'Email already exist'

                ], REST_Controller::HTTP_OK);


            } elseif ($user_name) {
                $this->response([

                    'status' => FALSE,

                    'message' => 'Username already exist'

                ], REST_Controller::HTTP_OK);

            } else {


                $user_data = array();
                foreach ($post_data as $key => $value) {
                    if ($key != 'FullName') {
                        if ($key == 'Password') {
                            $user_data[$key] = md5($value);
                        } else {
                            $user_data[$key] = $value;
                        }

                    }


                }

                $user_data['CreatedAt'] = $user_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $user_data['CreatedBy'] = $user_data['UpdatedBy'] = 0; // 0 for user register from app
                $file_name = '';
                if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                    $file_name = $this->uploadImage("Image", "uploads/images/");
                    $file_name = "uploads/images/" . $file_name;
                }
                $user_data['Image'] = $file_name;

                $insert_id = $this->User_model->save($user_data);

                if ($insert_id > 0) {
                    $user_text_data = array();
                    $user_text_data['SystemLanguageID'] = 1;
                    $user_text_data['UserID'] = $insert_id;
                    $user_text_data['Title'] = $post_data['FullName'];
                    $user_text_data['CreatedAt'] = $user_text_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $user_text_data['CreatedBy'] = $user_text_data['UpdatedBy'] = 0; // 0 for user register from     
                    $this->User_text_model->save($user_text_data);
                    $user = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID = ' . $insert_id . '');

                    // updating temp user id to this user inserted id in temp orders
                    if (isset($temp_order_key) && $temp_order_key != '') {
                        $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('UserID' => $temp_order_key));
                        if ($temp_orders_for_user) {
                            $this->Temp_order_model->update(array('UserID' => $user[0]['UserID']), array('UserID' => $temp_order_key));
                        }
                    }

                    $this->response([

                        'status' => TRUE,

                        'user_info' => $user[0]

                    ], REST_Controller::HTTP_OK);


                } else {
                    $this->response([

                        'status' => FALSE,

                        'user_info' => 'Something went worng'

                    ], REST_Controller::HTTP_OK);
                }


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function checkUsername_post()

    {

        $data = array();

        $data_fetch = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            foreach ($post_data as $key => $value) {
                $data_fetch[$key] = $value;
            }

            $user = $this->User_model->getWithMultipleFields($data_fetch);

            if ($user) {


                $this->response([

                    'status' => FALSE,

                    'message' => 'User already exist'

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => TRUE,

                    'message' => ''

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function services_and_cities_get()
    {
        $services = $this->Service_model->getAllJoinedData(true, 'ServiceID', $this->language);
        $cities = $this->City_model->getAllJoinedData(true, 'CityID', $this->language);
        $this->response([
            'status' => TRUE,
            'services' => $services,
            'cities' => $cities
        ], REST_Controller::HTTP_OK);

    }


    public function search_store_post()
    {
        $post_data = $this->post_data; // ServiceID, CityID
        if (!empty($post_data)) {
            $where = 'store_services.ServiceID = ' . $post_data['ServiceID'] . ' AND users.CityID = ' . $post_data['CityID'] . ' AND users.StoreID = 0';
            $stores = $this->Store_service_model->getStoresWithServiceID('EN', $where, true);
            $this->response([
                'status' => TRUE,
                'stores' => $stores
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function get_top_rated_store_get()

    {
        $post_data = $this->post_data;
        $stores = array();

        $where = 'users.RoleID = 3 AND users.StoreID = 0';
        $stores = $this->User_model->getStoresWithTopRating('EN', $where, true);

        $this->response([
            'status' => TRUE,
            'stores' => $stores
        ], REST_Controller::HTTP_OK);


    }


    public function store_services_and_products_post()

    {
        $post_data = $this->post_data;
        $stores = array();
        $services = array();
        $products = array();

        if (!empty($post_data)) {
            $user_data = $this->User_model->getUserDataWithID($post_data['UserID'], $this->language);
            if ((isset($post_data['services']) && $post_data['services']) || (isset($post_data['products']) && $post_data['products'])) {
                if (isset($post_data['services']) && $post_data['services']) {
                    $where = 'store_services.UserID = ' . $post_data['UserID'] . '';
                    $services = $this->Store_service_model->getStoresWithServiceID($this->language, $where, true);
                }
                if (isset($post_data['products']) && $post_data['products']) {
                    $where = 'products.UserID = ' . $post_data['UserID'] . '';
                    $result = $this->Product_model->getAllJoinedData(true, 'ProductID', $this->language, $where);
                    //print_r($result);exit;
                    if (!empty($result)) {
                        $fetch_image = array();
                        $fetch_image['OtherTableName'] = 'products';

                        foreach ($result as $value) {
                            $product_images = array();
                            $fetch_image['OtherTableID'] = $value['ProductID'];
                            $images = $this->Image_model->getMultipleRows($fetch_image, true);
                            if ($images) {
                                $value['images'] = $images;
                            } else {
                                $value['images'] = $product_images;
                            }
                            $products[] = $value;
                        }
                    }

                }

                $this->response([
                    'status' => TRUE,
                    'store_detail' => $user_data,
                    'services' => $services,
                    'products' => $products
                ], REST_Controller::HTTP_OK);


            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Please send product or services as true"
                ], REST_Controller::HTTP_OK);
            }
            //$where = 'store_services.ServiceID = '.$post_data['ServiceID'].' AND users.StoreID = 0';
            //$stores = $this->Store_service_model->getStoresWithServiceID('EN',$where,true);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function addAddress_post()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $insert_id = $this->Address_model->save($post_data);


            $post_data['CreatedAt'] = $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $address = $this->Address_model->get($insert_id, true, 'AddressID');


            if ($insert_id > 0) {


                $this->response([

                    'status' => True,

                    'address_info' => $address

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function updateAddress_post()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $update_by = array();
            $update_by['AddressID'] = $post_data['AddressID'];
            $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $this->Address_model->update($post_data, $update_by);


            $address = $this->Address_model->get($post_data['AddressID'], true, 'AddressID');


            $this->response([

                'status' => True,

                'address_info' => $address

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getAddress_get()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $address = $this->Address_model->get($post_data['AddressID'], true, 'AddressID');
            $city = $this->City_text_model->get($address['CityID'], true, 'CityID');
            $address['CityName'] = $city['Title'];


            if ($address) {


                $this->response([

                    'status' => True,

                    'address_info' => $address

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getAllAddress_get()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $return_array = array();

            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];

            // $address = $this->Address_model->getMultipleRows($fetch_by, true);
            $address = $this->Address_model->getJoinedDataWithOtherTable(true, 'CityID', 'cities_text', 'address.UserID = ' . $fetch_by['UserID'] . '');

            if ($address) {
                $return_array = $address;
            }


            $this->response([

                'status' => True,

                'address_info' => $return_array

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function deleteAddress_post()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $return_array = array();

            $deleted_by = array();
            $deleted_by['AddressID'] = $post_data['AddressID'];

            $this->Address_model->delete($deleted_by);


            $this->response([

                'status' => True,

                'message' => 'Deleted Successfully'

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function addToCart_post()
    {
        $data = array();

        $post_data = $this->post_data;

        if (!empty($post_data)) {


            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];
            $fetch_by['ProductID'] = $this->input->post('ProductID');

            $already_added = $this->Temp_order_model->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update = array();
                $update_by = array();

                $update['ProductQuantity'] = $post_data['ProductQuantity'];
                $update_by['TempOrderID'] = $already_added->TempOrderID;

                $this->Temp_order_model->update($update, $update_by);


                $insert_id = $already_added->TempOrderID;

            } else {
                $insert_id = $this->Temp_order_model->save($post_data);
            }


            // $order_data = $this->Temp_order_model->getJoinedDataWithOtherTable(true, 'ProductID', 'products', 'temp_orders.TempOrderID = ' . $insert_id . '');
            $order_data = $this->Temp_order_model->getJoinedDataWithOtherTable(true, 'ProductID', 'products_text', 'temp_orders.TempOrderID = ' . $insert_id . '');

            if ($insert_id > 0) {


                $this->response([

                    'status' => True,

                    'order_item' => $order_data

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function updateCart_post()
    {
        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $update = array();
            $update_by = array();

            $update['ProductQuantity'] = $post_data['ProductQuantity'];
            $update_by['TempOrderID'] = $post_data['TempOrderID'];

            $this->Temp_order_model->update($update, $update_by);


            $order_data = $this->Temp_order_model->getJoinedDataWithOtherTable(true, 'ProductID', 'products', 'temp_orders.TempOrderID = ' . $post_data['TempOrderID'] . '');

            $this->response([

                'status' => True,

                'order_item' => $order_data

            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function deleteOrderItem_post()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $return_array = array();

            $deleted_by = array();
            $deleted_by['TempOrderID'] = $post_data['TempOrderID'];

            $this->Temp_order_model->delete($deleted_by);


            $this->response([

                'status' => True,

                'message' => 'Deleted Successfully'

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getAllCartItem_get()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $return_array = array();


            $products = $this->Temp_order_model->getCartData($post_data['UserID']);

            if ($products) {
                foreach ($products as $product) {
                    $images = array();
                    $res = getProductImages($product['ProductID']);
                    if ($res) {
                        $images = $res;
                    }
                    $product['images'] = $images;
                    $return_array[] = $product;
                }


            }


            $this->response([

                'status' => True,

                'cart_item' => $return_array

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function placeOrder_post()
    {
        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $data = array();


            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];

            $cart_products = $this->Temp_order_model->getJoinedDataWithOtherTable(false, 'ProductID', 'products', 'temp_orders.UserID = ' . $post_data['UserID'] . '');

            if ($cart_products) {

                $address_id = $this->input->post('AddressID');
                $order_data = array();

                $order_data['UserID'] = $post_data['UserID'];
                $order_data['AddressID'] = $address_id;
                $order_data['CreatedAt'] = $order_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $order_data['OrderTrackID'] = RandomString();
                $insert_id = $this->Order_model->save($order_data);
                if ($insert_id > 0) {
                    $update_order = array();
                    $update_order_by = array();
                    $update_order_by['OrderID'] = $insert_id;
                    $update_order['OrderTrackID'] = $order_data['OrderTrackID'] . $insert_id;
                    $this->Order_model->update($update_order, $update_order_by);
                    // $this->sendThankyouEmailToCustomer($insert_id);
                    $return_array = array();
                    $return_array = $this->Order_model->get($insert_id, true, 'OrderID');
                    $order_item_data = array();
                    foreach ($cart_products as $product) {

                        $order_item_data[] = ['OrderID' => $insert_id,
                            'ProductID' => $product->ProductID,
                            'Quantity' => $product->ProductQuantity,
                            'Price' => $product->Price
                        ];

                    }

                    $this->Order_item_model->insert_batch($order_item_data);
                    $deleted_by = array();
                    $deleted_by['UserID'] = $post_data['UserID'];
                    $this->Temp_order_model->delete($deleted_by);

                    $order_items = array();

                    // $items = $this->Order_model->getOrderItems($insert_id);
                    $items = $this->Order_model->getOrderDetail($insert_id);

                    if (!empty($items)) {
                        $order_items = $items;
                    }
                    $return_array['order_items'] = $order_items;
                    $this->response([

                        'status' => True,

                        'order_data' => $return_array

                    ], REST_Controller::HTTP_OK);

                } else {

                    $this->response([

                        'status' => false,

                        'message' => 'There is something went worng'

                    ], REST_Controller::HTTP_OK);
                }


            } else {
                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);
            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    // booking section 

    public function addToCartBooking_post()
    {
        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];
            $fetch_by['ServiceID'] = $this->input->post('ServiceID');

            $already_added = $this->Temp_booking_model->getWithMultipleFields($fetch_by);
            if (!$already_added) {
                $insert_id = $this->Temp_booking_model->save($post_data);

            } else {
                $insert_id = $already_added->TempBookingID;
            }


            $order_data = $this->Temp_booking_model->getJoinedDataWithOtherTable(true, 'ServiceID', 'services', 'temp_bookings.TempBookingID = ' . $insert_id . '');


            if ($insert_id > 0) {


                $this->response([

                    'status' => True,

                    'order_services' => $order_data

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function deleteBookingService_post()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {

            $return_array = array();

            $deleted_by = array();
            $deleted_by['TempBookingID'] = $post_data['TempBookingID'];

            $this->Temp_booking_model->delete($deleted_by);


            $this->response([

                'status' => True,

                'message' => 'Deleted Successfully'

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getAllCartServices_get()

    {

        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {


            $services = $this->Temp_booking_model->getCartData($post_data['UserID']);


            $this->response([

                'status' => True,

                'cart_services' => $services

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function check_date_get($d){
        echo date('y-m-d H:i:s',$d);
    }


    public function placeBooking_post()
    {
        $data = array();

        $post_data = $this->post_data;


        if (!empty($post_data)) {
            $data = array();
            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];

            $service_ids = explode(',', $post_data['ServiceID']);

            $cart_products = $this->Temp_booking_model->getCartData($post_data['UserID']);

            if (1 == 1 || $cart_products) { // this $cart_products functionality was already here so just bypassing this as I am not sure what is going on

                //$address_id = $this->input->post('AddressID');
                $order_data = array();

                $order_data['UserID'] = $post_data['UserID'];
                $order_data['TimeStart'] = $post_data['TimeStart'];

                $TotalServiceTime = $post_data['TotalServiceTime']; // This should be in minutes always
                $order_data['TimeEnd'] = strtotime("+$TotalServiceTime minutes", $order_data['TimeStart']);

                $order_data['CreatedAt'] = $order_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $order_data['OrderTrackID'] = RandomString();
                $insert_id = $this->Booking_model->save($order_data);
                if ($insert_id > 0) {
                    $update_order = array();
                    $update_order_by = array();
                    $update_order_by['BookingID'] = $insert_id;
                    $update_order['OrderTrackID'] = $order_data['OrderTrackID'] . $insert_id;
                    $this->Booking_model->update($update_order, $update_order_by);
                    // $this->sendThankyouEmailToCustomer($insert_id);
                    $return_array = array();
                    $return_array = $this->Booking_model->get($insert_id, true, 'BookingID');
                    $order_item_data = array();

                    // commented this as I am told that we are not saving any temp bookings on our database side, bilal on 14-09-2018
                    /*foreach ($cart_products as $product) {

                        $order_item_data[] = ['BookingID' => $insert_id,
                            'ServiceID' => $product['ServiceID']
                        ];

                    }*/

                    foreach ($service_ids as $id) {

                        $order_item_data[] = ['BookingID' => $insert_id,
                            'ServiceID' => $id
                        ];

                    }

                    $this->Booking_service_model->insert_batch($order_item_data);
                    $deleted_by = array();
                    $deleted_by['UserID'] = $post_data['UserID'];
                    $this->Temp_booking_model->delete($deleted_by);

                    $order_items = array();

                    $items = $this->Booking_model->getOrderServices($insert_id);

                    if (!empty($items)) {
                        $order_items = $items;
                    }
                    $return_array['order_services'] = $order_items;
                    $this->response([

                        'status' => True,

                        'order_data' => $return_array

                    ], REST_Controller::HTTP_OK);

                } else {

                    $this->response([

                        'status' => false,

                        'message' => 'Something went wrong when saving booking'

                    ], REST_Controller::HTTP_OK);
                }


            } else {

                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);
            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    // booking section end


    public function productDetail_get()
    {


        $post_data = $this->post_data;
        $product_images = array();


        if (!empty($post_data)) {

            $product_detail = $this->Product_model->getProductDetail('EN', $post_data['ProductID']);
            if (!empty($product_detail)) {
                $result = getProductImages($post_data['ProductID']);
                if ($result) {
                    $product_images = $result;
                }
                $product_detail['images'] = $product_images;

                $this->response([

                    'status' => true,

                    'product_detail' => $product_detail

                ], REST_Controller::HTTP_OK);
            } else {

                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);

            }


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }

    public function categories_get()
    {
        $where = 'system_languages.ShortCode = "' . $this->language . '"';
        $categories = $this->Category_model->getJoinedData(true, 'CategoryID',$where);
        $this->response([
            'status' => TRUE,
            'categories' => $categories
        ], REST_Controller::HTTP_OK);

    }


    public function allProducts_get()
    {
        $products = array();
        $product_images = array();
        $result = $this->Product_model->getAllProduct($this->language);


        if (!empty($result)) {
            $fetch_image = array();
            $fetch_image['OtherTableName'] = 'products';

            foreach ($result as $value) {
                $product_images = array();
                $fetch_image['OtherTableID'] = $value['ProductID'];
                $images = $this->Image_model->getMultipleRows($fetch_image, true);
                if ($images) {
                    $value['images'] = $images;
                } else {
                    $value['images'] = $product_images;
                }
                $products[] = $value;
            }
        }


        $this->response([
            'status' => TRUE,
            'products' => $products
        ], REST_Controller::HTTP_OK);

    }


    public function login_post()

    {

        $data = array();

        $data_fetch = array();

        $data = $this->post_data;

        if (!empty($data)) {

            if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                $temp_order_key = $data['temp_order_key'];
                unset($data['temp_order_key']);
            }

            if (isset($data['facebook_id'])) {
                $data_fetch_by_fb_id['facebook_id'] = $data['facebook_id'];
                $arr = $this->User_model->getMultipleRows($data_fetch_by_fb_id, true);
                $user = $arr[0];

                if ($user) {

                    // updating temp user id to this user inserted id in temp orders
                    if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                        $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('UserID' => $temp_order_key));
                        if ($temp_orders_for_user) {
                            $this->Temp_order_model->update(array('UserID' => $user['UserID']), array('UserID' => $temp_order_key));
                        }
                    }

                    $this->updateUserLoginStatus($user['id']);


                    $this->response([

                        'status' => TRUE,

                        'user_info' => $user

                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([

                        'status' => FALSE,

                        'message' => 'User does not exist with this facebook id.please sign up this user first.'

                    ], REST_Controller::HTTP_OK);
                }
            } elseif (isset($data['google_id'])) {
                $data_fetch_by_google_id['google_id'] = $data['google_id'];
                $arr = $this->User_model->getMultipleRows($data_fetch_by_google_id, true);
                $user = $arr[0];

                if ($user) {

                    // updating temp user id to this user inserted id in temp orders
                    if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                        $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('UserID' => $temp_order_key));
                        if ($temp_orders_for_user) {
                            $this->Temp_order_model->update(array('UserID' => $user['UserID']), array('UserID' => $temp_order_key));
                        }
                    }

                    $this->updateUserLoginStatus($user['id']);


                    $this->response([

                        'status' => TRUE,

                        'user_info' => $user

                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([

                        'status' => FALSE,

                        'message' => 'User does not exist with this google id.please sign up this user first.'

                    ], REST_Controller::HTTP_OK);
                }
            } else {


                //$data_fetch['Email'] = $data['Email'];
                //$data_fetch['Password'] = $data['Password'];

                $where = 'users.Email = "' . $data['Email'] . '" AND users.Password = "' . md5($data['Password']) . '"';
                $user = $this->User_model->getJoinedData(true, 'UserID', $where);

                if ($user) {

                    // updating temp user id to this user inserted id in temp orders
                    if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                        $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('UserID' => $temp_order_key));
                        if ($temp_orders_for_user) {
                            $this->Temp_order_model->update(array('UserID' => $user[0]['UserID']), array('UserID' => $temp_order_key));
                        }
                    }

                    // $this->updateUserLoginStatus($user['id']);


                    $this->response([

                        'status' => TRUE,

                        'user_info' => $user[0]

                    ], REST_Controller::HTTP_OK);


                } else {

                    $this->response([

                        'status' => FALSE,

                        'message' => 'Email or password incorrect'

                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
            }
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function uploadImage($key, $path)

    {


        $file_name = '';

        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];

        $file_size = $_FILES[$key]['size'];

        $file_tmp = $_FILES[$key]['tmp_name'];

        $file_type = $_FILES[$key]['type'];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $file_name;


    }


    public function uploadMultipleImages($key, $path, $i)

    {


        $file_name = '';

        $file_name = date('Ymdhsi') . $_FILES[$key]['name'][$i];

        $file_size = $_FILES[$key]['size'][$i];

        $file_tmp = $_FILES[$key]['tmp_name'][$i];

        $file_type = $_FILES[$key]['type'][$i];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $file_name;


    }


    public function updateUserLoginStatus($id)

    {

        $data = array();

        $arr_update = array();

        $data['last_login'] = date('Y-m-d H:i:s');

        //$user = $this->session->userdata('user');

        $arr_update['id'] = $id;

        $this->User_model->update($data, $id);

        return true;

        //redirect($this->config->item('base_url') . 'user');


    }


    public function forgotPassword_post()
    {
        $data = $this->post_data;
        if ($data) {
            $fetch_by['Email'] = $data['Email'];
            $user = $this->User_model->getWithMultipleFields($fetch_by);
            if ($user) {
                $new_password = RandomString();
                $new_password = md5($new_password);
                $email['body'] = 'Your new password is ' . $new_password . '. Please use this password to login then you can change your password.';
                $email['to'] = $user->Email;
                $email['from'] = 'no-reply@glamorous.com';
                $email['subject'] = 'Forgot Password';
                $mail_sent = sendEmail($email);
                if ($mail_sent) {
                    $this->User_model->update(array('Password' => $new_password), array('UserID' => $user->UserID));
                    $this->response([
                        'status' => TRUE,
                        'message' => 'your password is sent at your email address'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No user found with this email'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

    }


    public function updateUser_post()
    {
        $post_data = $this->post_data;
        // print_rm($post_data);
        if (!empty($post_data)) {
            $update_by['UserID'] = $post_data['UserID'];
            $FullName = $post_data['FullName'];
            unset($post_data['UserID']);
            unset($post_data['FullName']);
            if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
                $file_name = $this->uploadImage("image", "uploads/images/");
                $file_name = "uploads/images/" . $file_name;
                $post_data['image'] = $file_name;
            }
            $this->User_model->update($post_data, $update_by);
            $this->User_text_model->update(array('Title' => $FullName), $update_by);
            $user = $this->User_model->getJoinedData(true, 'UserID', 'users.UserID = ' . $update_by['UserID'] . '');
            $this->response([
                'status' => TRUE,
                'user_info' => $user[0]
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllAds_get()
    {
        $ads = $this->Ad_model->getAllJoinedData(false, 'AdID', $this->language, 'ads.IsActive = 1');
        if (!empty($ads)) {
            $this->response([
                'status' => TRUE,
                'ads' => $ads
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No Ads Found'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function getAllUserOrdersBK_get()
    {
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $orders = $this->Order_model->getAllOrdersWithDetailsForUser($post_data['UserID']);
            if ($orders) {
                $this->response([
                    'status' => true,
                    'orders' => $orders
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'There is no order for this user'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllUserOrders_get() // fetches all orders for a user
    {
        $post_data = $this->post_data;
        if (!empty($post_data)) {
            $return_array = array();
            $return_bookings = array();
            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];
            $orders = $this->Order_model->getMultipleRows($fetch_by, true);
            $bookings = $this->Booking_model->getMultipleRows($fetch_by, true);
            if ($orders || $bookings) {
                if ($orders) {
                    foreach ($orders as $key => $order) {
                        $return_array[$key] = $order;
                        $order_items = array();
                        $items = $this->Order_model->getOrderItems($order['OrderID']);
                        if (!empty($items)) {
                            $order_items = $items;
                        }
                        $return_array[$key]['order_items'] = $order_items;
                    }
                }

                if ($bookings) {
                    foreach ($bookings as $key => $booking) {
                        $return_bookings[$key] = $booking;
                        $booking_services = array();
                        $booking_items = $this->Booking_model->getAllBookingOrders($fetch_by['UserID']);
                        if (!empty($booking_items)) {
                            $booking_services = $booking_items;
                        }
                        $return_bookings[$key]['booking_services'] = $booking_services;
                    }
                }

                $this->response([
                    'status' => true,
                    'orders' => $return_array,
                    'bookings' => $return_bookings,
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'There are no orders or bookings for this user'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllSlotsBK_get() // fetches all booked timeslots for a store
    {
        $post_data = $this->post_data; // UserID, Date
        if (!empty($post_data)) {
            $return_array = array();
            $fetch_by = array();
            $fetch_by['UserID'] = $post_data['UserID'];
            $store_info = $this->User_model->getMultipleRows($fetch_by, true);
            if ($store_info) {
                $store_opening_time = date('Y-m-d H:i:s', strtotime($post_data['Date'] . ' ' . $store_info['OpenTime'])); // timestamp of date posted and store opening time
                $store_closing_time = date('Y-m-d H:i:s', strtotime($post_data['Date'] . ' ' . $store_info['CloseTime'])); // timestamp of date posted and store closing time
                //$store_opening_time = strtotime($post_data['Date'].' '.$store_info['OpenTime']); // timestamp of date posted and store opening time
                //$store_closing_time = strtotime($post_data['Date'].' '.$store_info['CloseTime']); // timestamp of date posted and store closing time
                // getting all the bookings made during these opening and closing time
                $bookings = $this->Booking_model->getAllBookingsFOrStoreWithinATimespan($post_data['UserID'], $store_opening_time, $store_closing_time);
                foreach ($bookings as $key => $booking) {
                    $return_array[$key] = $booking;
                    /*$order_items = array();
                    $items = $this->Order_model->getOrderItems($order['OrderID']);
                    if (!empty($items)) {
                        $order_items = $items;
                    }
                    $return_array[$key]['order_items'] = $order_items;*/
                }
                $this->response([
                    'status' => true,
                    'orders' => $return_array
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Store not found'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllSlots_get() // fetches all booked timeslots for a store
    {
        $post_data = $this->post_data; // UserID, ServiceTotalTime, Date
        if (!empty($post_data)) {
            $return_array = array();

            $UserID = $post_data['UserID']; // This is basically Store's ID
            $TimeForService = $post_data['ServiceTotalTime']; // This is total serving time for all service

            // getting store details
            $store_info = $this->User_model->get($UserID, true, 'UserID');

            if ($store_info) {
                $store_opening_time = date('Y-m-d H:i:s', strtotime($post_data['Date'] . ' ' . $store_info['OpenTime'])); // creating complete date from date posted and store opening time
                $store_closing_time = date('Y-m-d H:i:s', strtotime($post_data['Date'] . ' ' . $store_info['CloseTime'])); // creating complete date from date posted and store closing time
                //$store_opening_time = strtotime($post_data['Date'].' '.$store_info['OpenTime']); // timestamp of date posted and store opening time
                //$store_closing_time = strtotime($post_data['Date'].' '.$store_info['CloseTime']); // timestamp of date posted and store closing time

                // getting total hours the store is open (as decimal)
                $store_opening_time_dt = new DateTime($store_opening_time);
                $store_closing_time_dt = new DateTime($store_closing_time);
                $interval = $store_closing_time_dt->diff($store_opening_time_dt);
                $store_open_time = $interval->format('%H:%I');
                $total_store_open_time = $this->getDecimalHours($store_open_time);
                $total_store_open_time = $total_store_open_time * 60;

                $total_slots = $total_store_open_time / $TimeForService; // These are total number of slots present for thsi service in this store
                $total_slots = floor($total_slots);
                $start_slot_factor = 0;
                $end_slot_factor = $TimeForService;
                for ($i = 0; $i < $total_slots; $i++) {
                    //$start_slot = date("Y-m-d H:i:s", strtotime("+$start_slot_factor minutes", $store_opening_time));
                    //$end_slot = date("Y-m-d H:i:s", strtotime("+$end_slot_factor minutes", $store_opening_time));

                    $dateTime_start = new DateTime($store_opening_time);
                    $dateTime_start->modify("+$start_slot_factor minutes");
                    $start_slot = $dateTime_start->format('Y-m-d H:i:s');

                    $dateTime_end = new DateTime($store_opening_time);
                    $dateTime_end->modify("+$end_slot_factor minutes");
                    $end_slot = $dateTime_end->format('Y-m-d H:i:s');

                    //echo $start_slot;echo $end_slot;exit();

                    $return_array[$i]['slot_start'] = $start_slot;
                    $return_array[$i]['slot_end'] = $end_slot;

                    // getting all the bookings made during these opening and closing time

                    $booking = $this->Booking_model->checkIfBookingExistInTimeslot($post_data['UserID'], strtotime($start_slot), strtotime($end_slot));
                   // echo $this->db->last_query();exit();
                    if ($booking) {
                        $return_array[$i]['status'] = 'booked';
                    } else {
                        $return_array[$i]['status'] = 'free';
                    }
                    $start_slot_factor = $end_slot_factor;
                    $end_slot_factor = $end_slot_factor + $TimeForService;
                }

                $this->response([
                    'status' => true,
                    'slots' => $return_array
                ], REST_Controller::HTTP_OK);
            } else { // if store information not found
                $this->response([
                    'status' => false,
                    'message' => 'Store not found'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllSalonsForCategory_get() // fetches all salons of a category
    {
        $post_data = $this->post_data; // CategoryID
        if (!empty($post_data)) {
            $where = 'users.CategoryID = ' . $post_data['CategoryID'] . ' AND users.StoreID = 0';
            $stores = $this->User_model->getAllStores('EN', $where, true);
            if ($stores) {
                $this->response([
                    'status' => true,
                    'stores' => $stores
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Stores not found for this category'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    private function getDecimalHours($time)
    {
        $hms = explode(":", $time);
        //return ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));
        return ($hms[0] + ($hms[1] / 60));
    }

    public function changePasswordBK()
    {
        $UserID = $this->input->get('UserID');
        $old_password_posted = md5($this->input->get('OldPassword'));
        $new_password = $this->input->get('NewPassword');
        $confirm_password = $this->input->get('ConfirmNewPassword');
        $user_detail = $this->User_model->get($UserID, false, 'UserID');
        $old_password_from_db = $user_detail->Password;
        if ($old_password_posted != $old_password_from_db) {
            $response['status'] = 0;
            $response['message'] = "Wrong old password provided";
            $response['response'] = "";
        } elseif ($new_password != $confirm_password) {
            $response['status'] = 0;
            $response['message'] = "Password and confirm password didn't match";
            $response['response'] = "";
        } else {
            $update_data['Password'] = md5($new_password);
            $this->User_model->update($update_data, array('UserID' => $UserID));
            $response['status'] = 1;
            $response['message'] = "Password changed successfully. Please use the new password next time you login";
            $response['response'] = "";
        }
        echo json_encode($response);
        exit();
    }

    public function changePassword_get()
    {
        $post_data = $this->post_data; // UserID, OldPassword, NewPassword, ConfirmNewPassword
        if (!empty($post_data)) {
            $old_password_posted = md5($post_data['OldPassword']);
            $new_password = $post_data['NewPassword'];
            $confirm_password = $post_data['ConfirmNewPassword'];
            $user_detail = $this->User_model->get($post_data['UserID'], false, 'UserID');
            $old_password_from_db = $user_detail->Password;
            if ($old_password_posted != $old_password_from_db) {
                $this->response([
                    'status' => false,
                    'message' => 'Wrong old password provided'
                ], REST_Controller::HTTP_OK);
            } elseif ($new_password != $confirm_password) {
                $this->response([
                    'status' => false,
                    'message' => 'Password and confirm password didn\'t match'
                ], REST_Controller::HTTP_OK);
            } else {
                $update_data['Password'] = md5($new_password);
                $this->User_model->update($update_data, array('UserID' => $post_data['UserID']));
                $user_detail = $this->User_model->get($post_data['UserID'], true, 'UserID');
                $this->response([
                    'status' => true,
                    'user_info' => $user_detail
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getAllShoppingCards_get()
    {
        $current_date = date('Y-m-d'); // current date
        $where = "shopping_cards.CardValidityDate >= '" . $current_date . "' AND shopping_cards.CardCount > 0 AND shopping_cards.IsActive = 1";
        $shopping_cards = $this->Shopping_card_model->getAllJoinedData(true, 'ShoppingCardID', 'EN', $where, 'ASC', 'CardValidityDate');
        $this->response([
            'status' => TRUE,
            'shopping_cards' => $shopping_cards ? $shopping_cards : array(),
        ], REST_Controller::HTTP_OK);
    }

    public function purchaseShoppingCard_get()
    {
        $post_data = $this->post_data; // ShoppingCardID, UserID
        $already_purchased = $this->Shopping_card_purchase_model->getWithMultipleFields($post_data);
        if ($already_purchased) {
            $this->response([
                'status' => FALSE,
                'message' => "You have already purchased this shopping card.",
            ], REST_Controller::HTTP_OK);
        } else {
            $post_data['PurchasedAt'] = date('Y-m-d H:i:s');
            $inserted_id = $this->Shopping_card_purchase_model->save($post_data);
            if ($inserted_id > 0) {
                $shopping_card_data = $this->Shopping_card_model->getWithMultipleFields(array('ShoppingCardID' => $post_data['ShoppingCardID']));
                $card_data['CardCount'] = $shopping_card_data->CardCount - 1;
                $card_number_data['ShoppingCardID'] = $post_data['ShoppingCardID'];
                $card_number_data['CardNumber'] = generate_string($post_data['UserID']);
                $this->Shopping_card_model->update($card_data, array('ShoppingCardID' => $post_data['ShoppingCardID']));
                $this->Shopping_card_number_model->save($card_number_data);
                $this->response([
                    'status' => TRUE,
                    'message' => "You have successfully purchased this shopping card.",
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Something went wrong while purchasing this shopping card.",
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function getPurchasedShoppingCards_get()
    {
        $UserID = $this->input->get('UserID');
        $shopping_cards = $this->Shopping_card_purchase_model->getPurchasedShoppingCards($UserID);
        $i = 0;
        foreach ($shopping_cards as $shopping_card) {
            $card_number_data = $this->Shopping_card_number_model->getCardNumber($shopping_card['ShoppingCardID'], $UserID);
            $shopping_cards[$i]['CardNumber'] = $card_number_data->CardNumber;
            $i++;
        }
        $this->response([
            'status' => TRUE,
            'shopping_cards' => $shopping_cards ? $shopping_cards : array(),
        ], REST_Controller::HTTP_OK);
    }

    public function addShoppingCard_get()
    {
        $post_data = $this->post_data; // CardNumber, UserID
        $shopping_card_number_data = $this->Shopping_card_number_model->getWithMultipleFields(array('CardNumber' => $post_data['CardNumber']));
        if (!$shopping_card_number_data) {
            $this->response([
                'status' => FALSE,
                'message' => "Sorry! Wrong card number used.",
            ], REST_Controller::HTTP_OK);
        }
        $shopping_card_data = $this->Shopping_card_model->getWithMultipleFields(array('ShoppingCardID' => $shopping_card_number_data->ShoppingCardID));
        $card_purchased_data = $this->Shopping_card_purchase_model->getWithMultipleFields(array('ShoppingCardID' => $shopping_card_data->ShoppingCardID));
        if ($card_purchased_data->UserID == $post_data['UserID']) {
            $this->response([
                'status' => FALSE,
                'message' => "Sorry! You can't add this shopping card as you have already purchased this card.",
            ], REST_Controller::HTTP_OK);
        } else {
            $already_added = $this->Shopping_card_received_model->getWithMultipleFields(array('ShoppingCardID' => $shopping_card_data->ShoppingCardID));
            if ($already_added) {
                $this->response([
                    'status' => FALSE,
                    'message' => "Sorry! This shopping card is already added.",
                ], REST_Controller::HTTP_OK);
            } else {
                $added_card_data['ShoppingCardID'] = $shopping_card_data->ShoppingCardID;
                $added_card_data['ShoppingCardSharedBy'] = getUserIDFromString($post_data['CardNumber']);
                $added_card_data['ShoppingCardReceivedBy'] = $post_data['UserID'];
                $added_card_data['ShoppingCardAddedAt'] = date('Y-m-d H:i:s');
                $inserted_id = $this->Shopping_card_received_model->save($added_card_data);
                if ($inserted_id > 0) {
                    $this->response([
                        'status' => TRUE,
                        'message' => "You have successfully added this shopping card.",
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Something went wrong while adding this shopping card.",
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
    }

    public function getReceivedShoppingCards_get()
    {
        $UserID = $this->input->get('UserID');
        $shopping_cards = $this->Shopping_card_received_model->getReceivedShoppingCards($UserID);
        $i = 0;
        foreach ($shopping_cards as $shopping_card) {
            $card_number_data = $this->Shopping_card_number_model->getCardNumber($shopping_card['ShoppingCardID'], $shopping_card['ShoppingCardSharedBy']);
            $shopping_cards[$i]['CardNumber'] = $card_number_data->CardNumber;
            $i++;
        }
        $this->response([
            'status' => TRUE,
            'shopping_cards' => $shopping_cards ? $shopping_cards : array(),
        ], REST_Controller::HTTP_OK);
    }

    public function getShoppingCardHistory_get()
    {
        $post_data = $this->post_data; // CardNumber
        $shopping_card_number_data = $this->Shopping_card_number_model->getWithMultipleFields(array('CardNumber' => $post_data['CardNumber']));
        if (!$shopping_card_number_data) {
            $this->response([
                'status' => FALSE,
                'message' => "Sorry! Wrong card number used.",
            ], REST_Controller::HTTP_OK);
        }
        $card_data = $this->Shopping_card_model->getJoinedData(false, 'ShoppingCardID', "shopping_cards.ShoppingCardID = " . $shopping_card_number_data->ShoppingCardID)[0];
        $Price = $card_data->CardPrice;
        $Remaining = $card_data->CardPrice;
        $CardPurchasedBy = getUserIDFromString($post_data['CardNumber']);
        $user = $this->User_model->getJoinedData(false, 'UserID', "users.UserID = " . $CardPurchasedBy)[0];
        $TransactionHistory = $this->Shopping_card_transaction_model->getTransactionHistory("shopping_card_transactions.ShoppingCardID =" . $shopping_card_number_data->ShoppingCardID);
        if ($TransactionHistory && count($TransactionHistory) > 0) {
            foreach ($TransactionHistory as $item) {
                $Remaining -= $item['Outgoing'];
            }
        }
        $data['Remaining'] = (string)number_format($Remaining, 2);
        $data['CardTitle'] = $card_data->Title;
        $data['CardValidTill'] = $card_data->CardValidityDate;
        $data['Incoming'] = $Price;
        $data['PurchasedBy'] = $user->Title;
        $data['TransactionHistory'] = $TransactionHistory;
        $this->response([
            'status' => TRUE,
            'data' => $data,
        ], REST_Controller::HTTP_OK);
    }


}

