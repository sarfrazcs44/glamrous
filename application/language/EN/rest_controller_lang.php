<?php

/*
 * English language
 */

//General 

$lang['some_thing_went_wrong'] = 'There is something went wrong';

$lang['save_successfully']     = 'Save Successfully';
$lang['update_successfully']   = 'Updated Successfully';
$lang['deleted_successfully']  = 'Deleted Successfully';
$lang['you_should_update_data_seperately_for_each_language']  = 'You sould need to update data separately for each language';
$lang['yes']  = 'Yes';
$lang['no']  = 'No';
$lang['actions']  = 'Action';
$lang['submit']  = 'Submit';
$lang['back']  = 'Back';
$lang['add']  = 'Add';
$lang['edit']  = 'Edit';
$lang['are_you_sure']  = 'Are you sure you want to delete this?';
$lang['view']     = 'View';
$lang['add']     = 'Add';
$lang['edit']     = 'Edit';
$lang['delete']     = 'Delete';
$lang['mobile']                 = 'Mobile';
$lang['is_active']                 = 'Is Active';
$lang['logout']                 = 'Logout';

$lang['please_add_role_first'] = 'Please add role first';
$lang['please_add_store_first'] = 'Please add store first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';

$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module']        = 'Parent Module';
$lang['slug']                 = 'Slug';
$lang['icon_class']            = 'Icon Class';

$lang['title']                 = 'Title';
$lang['add_module']            = 'Add Module';
$lang['modules']                = 'Modules';
$lang['module']                = 'Module';
$lang['module_rights']                = 'Module Rights';

$lang['please_add_module_first']                = 'Please add module first';
$lang['rights']            = 'Rights';


// Roles Section

$lang['select_role']            = 'Select Role';
$lang['add_role']               = 'Add Role';
$lang['roles']                  = 'Roles';
$lang['role']                   = 'Role';
$lang['please_add_role_first']                = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';


// activity section 

$lang['add_activity']            = 'Add Activity';
$lang['activities']              = 'Activities';
$lang['activity']                = 'Activity';
$lang['choose_activity']                = 'Choose Activity';

// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name']                    = 'Name';
$lang['user']                    = 'User';
$lang['users']                    = 'Users';
$lang['add_user']                    = 'Add User';
$lang['select_user']                    = 'Select User';

$lang['email']                    = 'Email';
$lang['password']                 = 'Password';
$lang['min_length']               = '(Min char 8)';
$lang['confirm_password']         = 'Confirm Password';





$lang['add_city']            = 'Add City';
$lang['citys']            = 'Citys';
$lang['city']            = 'City';
$lang['add_district']            = 'Add District';
$lang['districts']            = 'Districts';
$lang['district']            = 'District';
$lang['please_add_distric_first'] = 'Please add distric first';
$lang['choose_district'] = 'Choose District';
$lang['add_patient']            = 'Add Patient';
$lang['patients']            = 'Patients';
$lang['patient']            = 'Patient';
$lang['add_center']            = 'Add center';
$lang['centers']            = 'Centers';
$lang['center']            = 'Center';
$lang['add_screening_camp']            = 'Add Screening camp';
$lang['screening_camps']            = 'Screening camps';
$lang['screening_camp']            = 'Screening camp';
$lang['add_center_type']            = 'Add Center type';
$lang['center_types']            = 'Center Types';
$lang['center_type']            = 'Center Type';

$lang['add_email_template']            = 'Add Email_template';
$lang['email_templates']            = 'Email Templates';
$lang['email_template']            = 'Email Template';
$lang['Heading']            = 'Heading';
$lang['Description']            = 'Description';
$lang['Image']            = 'Image';
$lang['add_category']            = 'Add Category';
$lang['categorys']            = 'Categories';
$lang['category']            = 'Category';

$lang['choose_category']            = 'Choose Category';
$lang['address']  = 'Address';
$lang['choose_online_status']  = 'Online Status';
$lang['add_service']            = 'Add Service';
$lang['services']            = 'Services';
$lang['service']            = 'Service';
$lang['add_store_service']            = 'Add Store_service';
$lang['store_services']            = 'Store_services';
$lang['store_service']            = 'Store_service';
$lang['choose_store']  = 'Select Store';
$lang['set_service_time']  = 'Set Service Time';
$lang['add_store']            = 'Add Store';
$lang['stores']            = 'Stores';
$lang['store']            = 'Store';
$lang['add_branch']            = 'Add Branch';
$lang['branchs']            = 'Branchs';
$lang['branch']            = 'Branch';
$lang['store_does_not_exist']  = 'Store does not exist';
$lang['please_add_city_first'] = 'Please add city first';
$lang['add_view'] = 'Add / View';
$lang['choose_city'] = 'Choose City';
$lang['user_type'] = 'User Type';



$lang['add_product']            = 'Add Product';
$lang['products']            = 'Products';
$lang['product']            = 'Product';
$lang['description']            = 'Description';
$lang['price']            = 'Price';
$lang['add_order']            = 'Add Order';
$lang['orders']            = 'Orders';
$lang['order']            = 'Order';

$lang['track_id']          = 'Track ID';
$lang['username']          = 'User Name';
$lang['store_title']       = 'Store Title';
$lang['status']            = 'Status';
$lang['order_date']        = 'Order Date';
$lang['add_received_orders']            = 'Add Received_orders';
$lang['received_orderss']            = 'Received_orderss';
$lang['received_orders']            = 'Received_orders';
$lang['add_dispatched_order']            = 'Add Dispatched_order';
$lang['dispatched_orders']            = 'Dispatched_orders';
$lang['dispatched_order']            = 'Dispatched_order';
$lang['add_delivered_order']            = 'Add Delivered_order';
$lang['delivered_orders']            = 'Delivered_orders';
$lang['delivered_order']            = 'Delivered_order';
$lang['add_booking']            = 'Add Booking';
$lang['bookings']            = 'Bookings';
$lang['booking']            = 'Booking';
$lang['time_start']            = 'Start Time';
$lang['time_end']            = 'End Time';
$lang['choose_branch']            = 'Choose Branch';

$lang['open_time']            = 'Open Time';
$lang['close_time']            = 'Close Time';
$lang['add_ad']            = 'Add Ad';
$lang['ads']            = 'Ads';
$lang['ad']            = 'Ad';
$lang['add_ad']            = 'Add Ad';
$lang['ads']            = 'Ads';
$lang['ad']            = 'Ad';
$lang['about_me']            = 'About Store';
$lang['banner_image']            = 'Banner Image';
$lang['add_shoppingCard']            = 'Add Shopping Card';
$lang['shoppingCards']            = 'Shopping Cards';
$lang['shoppingCard']            = 'Shopping Card';